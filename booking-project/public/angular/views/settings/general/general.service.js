var app = angular.module('booking');
app.service('GeneralSettingsService', function($q,$http) {
	return {
        getGeneralSettings: function(url) {
            url='api/general/settings';
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },
        editGeneralSettings: function(settings) {
            console.log(settings);
            url='api/general/settings';
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'menu_position': settings.menu_position,
                    'template_src' : settings.template_src
                },
                url: url
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});