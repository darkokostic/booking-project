var app = angular.module('booking');
app.controller('GeneralSettingsController', ['$scope','GeneralSettingsService','ThemeService', function($scope,GeneralSettingsService,ThemeService) {
    $scope.builder = [];
    $scope.builder.theme = [];
    $scope.builder.colorId = [];
    $scope.app = [];
    $scope.app.layout = [];
    $scope.app.layout.theme = [];

    $scope.changeMenuPosition = function(check) {
        console.log(check)
        if (check) {
            console.log(check)
            $scope.generalSettings.menu_position = 'right';
            ThemeService.foo = '/css/pages.rtl.css';
            ThemeService.notifyObservers();
        }
        else {
            console.log(check)
            $scope.generalSettings.menu_position = 'left';
            ThemeService.foo = '/css/pages.css';
            ThemeService.notifyObservers();
        }
    }

    $scope.setTheme = function(themePath, theme, id) {
        ThemeService.foo = themePath;
        ThemeService.notifyObservers();
        $scope.builder.theme = theme;
        $scope.builder.colorId = id;

        if (theme == 'default') {
            $scope.app.layout.theme = 'pages/css/pages.css'
        } else {
            $scope.app.layout.theme = 'pages/css/themes/' + theme + '.css'
        }
    };

    var responseSettings = GeneralSettingsService.getGeneralSettings(null);
    responseSettings.then(function(response) {
        console.log(response);
        $scope.generalSettings = response.data.entity[0];
        if ($scope.generalSettings.template_src == '/css/pages.css') {
            $scope.builder.theme = 'default';
        }
        if ($scope.generalSettings.template_src == '/css/themes/corporate.css') {
            $scope.builder.theme = 'corporate';
        }
        if ($scope.generalSettings.template_src == '/css/themes/retro.css') {
            $scope.builder.theme = 'retro';
        }
        if ($scope.generalSettings.template_src == '/css/themes/unlax.css') {
            $scope.builder.theme = 'unlax';
        }
        if ($scope.generalSettings.template_src == '/css/themes/vibes.css') {
            $scope.builder.theme = 'vibes';
        }
        if ($scope.generalSettings.template_src == '/css/themes/abstract.css') {
            $scope.builder.theme = 'abstract';
        }
        if($scope.generalSettings.menu_position=='right') {
            $scope.generalSettings.menu_position = 'right';
            ThemeService.foo = '/css/pages.rtl.css';
            ThemeService.notifyObservers();
        }
        else {
            $scope.generalSettings.menu_position = 'left';
            ThemeService.foo = '/css/pages.css';
            ThemeService.notifyObservers();
        }

    }).catch(function(reject) {
        console.log(reject);
    });

    $scope.editGeneralSettings = function(generalSettings){
        $scope.settings = generalSettings;
        $scope.settings.template_src = ThemeService.foo;

        console.log($scope.settings);
        GeneralSettingsService.editGeneralSettings($scope.settings).then(function(response) {
            console.log(response.data.message);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: "An error occurs",
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
        });
    };
}]);