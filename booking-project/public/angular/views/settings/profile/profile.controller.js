var app = angular.module('booking');
app.controller('ProfileController', ['$scope','ProfileService','ThemeService', function($scope,ProfileService,ThemeService) {
    $scope.builder = [];
    $scope.builder.theme = [];
    $scope.builder.colorId = [];
    $scope.app = [];
    $scope.app.layout = [];
    $scope.app.layout.theme = [];
    $scope.menuPosition = 'left';
    $scope.switchButton=false;

    var isChecked=function() {
        if(!angular.isUndefined(ThemeService.foo)){
            if(ThemeService.foo.includes('rtl')) {
                $scope.switchButton=true;
            }
            else {
                $scope.switchButton=false;
            }    
        }else {
            $scope.switchButton=false;
        }
    }
    isChecked();

    var setSideOfTheme=function(side,theme) {
        ThemeService.foo = theme;
        ThemeService.notifyObservers();
        $scope.menuPosition = side;
    }

    $scope.changeMenuPosition = function(switchButton) {
        console.log(switchButton);
        if (switchButton) {
            $scope.profileSettings.menu_position = 'right';
            if(!angular.isUndefined(ThemeService.foo)){
                var position = ThemeService.foo.length-4;
                var theme=[ThemeService.foo.slice(0, position), '.rtl', ThemeService.foo.slice(position)].join('');
                setSideOfTheme('right',theme);
            }else {
                setSideOfTheme('right','/css/pages.rtl.css');
            }
        }
        else {
            $scope.profileSettings.menu_position = 'left';
            var themeWithoutRtl=ThemeService.foo.substring(0,ThemeService.foo.length-8)+'.css';
            console.log(themeWithoutRtl)
            setSideOfTheme('left',themeWithoutRtl);
        }
    }

    var setThemesVariables=function(themePath,theme,id){
        console.log("Proslo jednom")
        ThemeService.foo = themePath;
        ThemeService.notifyObservers();
        $scope.builder.theme = theme;
        $scope.builder.colorId = id;
    } 

    $scope.setTheme = function(themePath, theme, id) {
        if(!angular.isUndefined(ThemeService.foo)){
            if(ThemeService.foo.includes('rtl')) {
                var position = themePath.length-4;
                var themePath = [themePath.slice(0, position), '.rtl', themePath.slice(position)].join('');
                setThemesVariables(themePath,theme,id);

            }else {
                setThemesVariables(themePath,theme,id);
            }
        }else {
            setThemesVariables(themePath,theme,id);
        }
    };

    var responseSettings = ProfileService.getProfileSettings(null);
    responseSettings.then(function(response) {
        console.log(response);
        $scope.profileSettings = response.data.entity[0];
        if ($scope.profileSettings.template_src == '/css/pages.css') {
            $scope.builder.theme = 'default';
        }
        if ($scope.profileSettings.template_src == '/css/themes/corporate.css') {
            $scope.builder.theme = 'corporate';
        }
        if ($scope.profileSettings.template_src == '/css/themes/retro.css') {
            $scope.builder.theme = 'retro';
        }
        if ($scope.profileSettings.template_src == '/css/themes/unlax.css') {
            $scope.builder.theme = 'unlax';
        }
        if ($scope.profileSettings.template_src == '/css/themes/vibes.css') {
            $scope.builder.theme = 'vibes';
        }
        if ($scope.profileSettings.template_src == '/css/themes/abstract.css') {
            $scope.builder.theme = 'abstract';
        }
        if($scope.profileSettings.menu_position=='right') {
            $scope.switchButton = true;
            if(!angular.isUndefined(ThemeService.foo)){
                var position = ThemeService.foo.length-4;
                var theme=[ThemeService.foo.slice(0, position), '.rtl', ThemeService.foo.slice(position)].join('');
                setSideOfTheme('right',theme);
            }else {
                setSideOfTheme('right','/css/pages.rtl.css');
            }
        } else {
            $scope.switchButton = false;
            $scope.profileSettings.menu_position = 'left';
            var themeWithoutRtl=ThemeService.foo.substring(0,ThemeService.foo.length-8)+'.css';
            console.log(themeWithoutRtl)
            setSideOfTheme('left',themeWithoutRtl);
        }

    }).catch(function(reject) {
        console.log(reject);
    });

    $scope.editProfileSettings = function(profileSettings){
        $scope.settings = profileSettings;
        $scope.settings.template_src = ThemeService.foo;
        if ($scope.settings.ribbon_color_name == 'Purple') {
            $scope.settings.ribbon_color = "#6d5cae";
        }
        if ($scope.settings.ribbon_color_name == 'Blue') {
            $scope.settings.ribbon_color = "#42aefa";
        }
        if ($scope.settings.ribbon_color_name == 'Yellow') {
            $scope.settings.ribbon_color = "#f9d146";
        }
        if ($scope.settings.ribbon_color_name == 'Green') {
            $scope.settings.ribbon_color = "#00cfbd";
        }
        if ($scope.settings.ribbon_color_name == 'Red') {
            $scope.settings.ribbon_color = "#f8564f";
        }

        console.log($scope.settings);
        ProfileService.editProfileSettings($scope.settings).then(function(response) {
            console.log(response.data.message);
        $('body').pgNotification({
            style: 'simple',
            message: response.data.message,
            position: 'top-right',
            timeout: 4000,
            type: 'success'
        }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: "An error occurs",
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
        });
    };
    
    $scope.changeRibbonColorName = function (color) {
        $scope.profileSettings.ribbon_color_name = color;
    }

}]);