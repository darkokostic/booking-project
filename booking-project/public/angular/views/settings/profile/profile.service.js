var app = angular.module('booking');
app.service('ProfileService', function($q,$http) {
	return {
        getProfileSettings: function(url) {
                url='api/user/settings';
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },
        editProfileSettings: function(settings) {
            console.log(settings);
            url='api/user/settings';
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'menu_position': settings.menu_position,
                    'ribbon_color' : settings.ribbon_color,
                    'ribbon_color_name': settings.ribbon_color_name,
                    'ribbon_name' : settings.ribbon_name,
                    'template_src' : settings.template_src,
                    'password' : settings.password
                },
                url: url
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});