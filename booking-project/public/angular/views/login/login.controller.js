var app = angular.module('booking');
app.controller('LoginController', ['$scope','LoginService', '$cookies', '$state', '$http', function($scope,LoginService, $cookies, $state, $http) {
    
    $scope.invalidCredentials=false;

    if($cookies.get('access_token')) {
        $state.go('side_menu.dashboard');
    }

	$scope.LoginService = LoginService;
	$scope.user = {
		username: null,
		password: null
	}
	$scope.onSubmit = function() {
        $scope.invalidCredentials=false;
		$scope.LoginService.login($scope.user).then(function(response) {
            //Token for accessing data
            console.log(response.data.entity);
            $cookies.put('firstname', response.data.entity.user.firstname);
            $cookies.put('lastname', response.data.entity.user.lastname);
            // $cookies.put('avatar', response.data.entity.user.images.path);
            $cookies.put('access_token', response.data.entity.access_token);
            $cookies.put('avatar_url', response.data.entity.user.images.path);
            // Use moment.js to add expiration date
            // $cookies.put('access_token', response.data.access_token, {
             //    expires: date
            // });
            
            // var today = new Date();
            // var expiresValue = new Date(today);
            // //Set 'expires' option in 4 hours
            // expiresValue.setMinutes(today.getMinutes() + 240);  
            // $cookies.putObject('userCookie', userCookie,{expires:expiresValue});


            //Use refresh token to get new one
            // $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('access_token');
            $cookies.put('refresh_token', response.data.entity.refresh_token);
            $scope.LoginService.getLocations().then(function(responseLocation) {
            console.log(responseLocation)
            if(responseLocation.data.entity.locations.length>0) {
                $cookies.put('location_id',responseLocation.data.entity.locations[0].id);
            }
            $state.go('side_menu.dashboard');
            }).catch(function(error) {
                console.log(error);
            })

		}).catch(function(error) {
            // $cookies.remove('token');
            console.error(error);
		});
		return false;
	}
}]);