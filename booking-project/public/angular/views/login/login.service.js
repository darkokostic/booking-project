var app = angular.module('booking');
app.service('LoginService', function($q, $http) {
	return {
		login: function(user) {
			var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'username' :  user.username,
                    'password' : user.password
                },
                url: '/api/login/'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
		},

        getLocations: function(user) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/locations/'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});