var app = angular.module('booking');
app.controller('DashboardCtrl', ['$scope','DashboardService', function($scope,DashboardService) {
	$scope.test= function() {
		console.log('Test uspesan');	
	};
    (function() {

        d3.json('http://revox.io/json/charts.json', function(data) {

            var container = '.widget-15-chart';

            var seriesData = [
                [],
                []
            ];
            var random = new Rickshaw.Fixtures.RandomData(40);
            for (var i = 0; i < 40; i++) {
                random.addData(seriesData);
            }

            var graph = new Rickshaw.Graph({
                renderer: 'bar',
                element: document.querySelector(container),
                height: 200,
                padding: {
                    top: 0.5
                },
                series: [{
                    data: seriesData[0],
                    color: $.Pages.getColor('complete-light'),
                    name: "New users"
                }, {
                    data: seriesData[1],
                    color: $.Pages.getColor('master-lighter'),
                    name: "Returning users"

                }]

            });

            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                formatter: function(series, x, y) {
                    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
                    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
                    return content;
                }
            });

            graph.render();

            $(window).resize(function() {
                graph.configure({
                    width: $(container).width(),
                    height: 200
                });

                graph.render()
            });

            $(container).data('chart', graph);
        });
    })();

    (function() {

        d3.json('http://revox.io/json/charts.json', function(data) {

            var container = '.widget-14-chart';

            var seriesData = [
                [],
                []
            ];
            var random = new Rickshaw.Fixtures.RandomData(40);
            for (var i = 0; i < 40; i++) {
                random.addData(seriesData);
            }

            var graph = new Rickshaw.Graph({
                renderer: 'bar',
                element: document.querySelector(container),
                height: 170,
                padding: {
                    top: 0.5
                },
                series: [{
                    data: seriesData[0],
                    color: $.Pages.getColor('complete-light'),
                    name: "New users"
                }, {
                    data: seriesData[1],
                    color: $.Pages.getColor('master-lighter'),
                    name: "Returning users"

                }]

            });

            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                formatter: function(series, x, y) {
                    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
                    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
                    return content;
                }
            });

            graph.render();

            $(window).resize(function() {
                graph.configure({
                    width: $(container).width(),
                    height: 170
                });

                graph.render()
            });

            $(container).data('chart', graph);
        });
    })();

    (function() {

        d3.json('http://revox.io/json/charts.json', function(data) {

            var container = '.widget-13-chart';

            var seriesData = [
                [],
                []
            ];
            var random = new Rickshaw.Fixtures.RandomData(40);
            for (var i = 0; i < 40; i++) {
                random.addData(seriesData);
            }

            var graph = new Rickshaw.Graph({
                renderer: 'bar',
                element: document.querySelector(container),
                height: 170,
                padding: {
                    top: 0.5
                },
                series: [{
                    data: seriesData[0],
                    color: $.Pages.getColor('complete-light'),
                    name: "New users"
                }, {
                    data: seriesData[1],
                    color: $.Pages.getColor('master-lighter'),
                    name: "Returning users"

                }]

            });

            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                formatter: function(series, x, y) {
                    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
                    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
                    return content;
                }
            });

            graph.render();

            $(window).resize(function() {
                graph.configure({
                    width: $(container).width(),
                    height: 170
                });

                graph.render()
            });

            $(container).data('chart', graph);
        });
    })();

    (function() {

        d3.json('http://revox.io/json/charts.json', function(data) {

            var container = '.widget-12-chart';

            var seriesData = [
                [],
                []
            ];
            var random = new Rickshaw.Fixtures.RandomData(40);
            for (var i = 0; i < 40; i++) {
                random.addData(seriesData);
            }

            var graph = new Rickshaw.Graph({
                renderer: 'bar',
                element: document.querySelector(container),
                height: 170,
                padding: {
                    top: 0.5
                },
                series: [{
                    data: seriesData[0],
                    color: $.Pages.getColor('complete-light'),
                    name: "New users"
                }, {
                    data: seriesData[1],
                    color: $.Pages.getColor('master-lighter'),
                    name: "Returning users"

                }]

            });

            var hoverDetail = new Rickshaw.Graph.HoverDetail({
                graph: graph,
                formatter: function(series, x, y) {
                    var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';
                    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
                    var content = swatch + series.name + ": " + parseInt(y) + '<br>' + date;
                    return content;
                }
            });

            graph.render();

            $(window).resize(function() {
                graph.configure({
                    width: $(container).width(),
                    height: 170
                });

                graph.render()
            });

            $(container).data('chart', graph);
        });
    })();

    // line chart
    (function() {
        //NVD3 Charts
        d3.json('http://revox.io/json/charts.json', function(data) {
            nv.addGraph(function() {
                var chart = nv.models.lineChart()
                    .x(function(d) {
                        return d[0]
                    })
                    .y(function(d) {
                        return d[1]
                    })
                    .color([
                        $.Pages.getColor('success'),
                        $.Pages.getColor('danger'),
                        $.Pages.getColor('primary'),
                        $.Pages.getColor('complete'),

                    ])
                    .showLegend(false)
                    .margin({
                        left: 30,
                        bottom: 35
                    })
                    .useInteractiveGuideline(true);

                chart.xAxis
                    .tickFormat(function(d) {
                        return d3.time.format('%a')(new Date(d))
                    });

                chart.yAxis.tickFormat(d3.format('d'));

                d3.select('.nvd3-line svg')
                    .datum(data.nvd3.line)
                    .transition().duration(500)
                    .call(chart);

                nv.utils.windowResize(chart.update);

                $('.nvd3-line').data('chart', chart);

                return chart;
            });
        });
    })();

    d3.json('http://revox.io/json/min_sales_chart.json', function(data) {
        nv.addGraph(function() {
            var chart = nv.models.lineChart()
                .x(function(d) {
                    return d[0]
                })
                .y(function(d) {
                    return d[1]
                })
                .color(['#000'])
                .margin({
                    top: 10,
                    right: -10,
                    bottom: -13,
                    left: -10
                })
                .showXAxis(false)
                .showYAxis(false)
                .showLegend(false)
                .interactive(false);

            d3.select('.widget-8-chart svg')
                .datum(data.siteVisits)
                .call(chart);



            nv.utils.windowResize(chart.update);

            nv.utils.windowResize(function() {
                setTimeout(function() {
                    $('.widget-8-chart .nvd3 circle.nv-point').attr("r", "3").css({
                        'stroke-width': '2px',
                        ' stroke-opacity': 0.4
                    });
                }, 500);
            });

            return chart;
        }, function() {
            setTimeout(function() {
                $('.widget-8-chart .nvd3 circle.nv-point').attr("r", "3").css({
                    'stroke-width': '2px',
                    ' stroke-opacity': 0.4
                });
            }, 500);
        });
    });

    d3.json('http://revox.io/json/min_sales_chart.json', function(data) {
        nv.addGraph(function() {
            var chart = nv.models.lineChart()
                .x(function(d) {
                    return d[0]
                })
                .y(function(d) {
                    return d[1]
                })
                .color(['#000'])
                .margin({
                    top: 10,
                    right: -10,
                    bottom: -13,
                    left: -10
                })
                .showXAxis(false)
                .showYAxis(false)
                .showLegend(false)
                .interactive(false);

            d3.select('.widget-8-chart svg')
                .datum(data.siteVisits)
                .call(chart);



            nv.utils.windowResize(chart.update);

            nv.utils.windowResize(function() {
                setTimeout(function() {
                    $('.widget-8-chart .nvd3 circle.nv-point').attr("r", "3").css({
                        'stroke-width': '2px',
                        ' stroke-opacity': 0.4
                    });
                }, 500);
            });

            return chart;
        }, function() {
            setTimeout(function() {
                $('.widget-8-chart .nvd3 circle.nv-point').attr("r", "3").css({
                    'stroke-width': '2px',
                    ' stroke-opacity': 0.4
                });
            }, 500);
        });
    });
}]);