var app = angular.module('booking');
app.service('BookingEventService', function($q,$http) {
    return {
        getPackage: function($packageId) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/packages/' + $packageId
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    }
});