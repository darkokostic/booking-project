var app = angular.module('booking');
app.controller('bookingEventController', ['$scope','BookingEventService', '$sce', '$window', '$interval', '$stateParams', '$timeout', function($scope,BookingEventService, $sce, $window, $interval, $stateParams, $timeout) {
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        yearRange: '1900:-0',
        onSelect: function(date){
            console.log(date);
            $scope.eventDate.date = date;
            $scope.$apply();
        }
    };
    BookingEventService.getPackage($stateParams.packageId).then(function (response) {
        $scope.package = response.data.entity;
    });
    $scope.activeStepId = 1;
    $scope.activeSubStepId = 1;
    $scope.steps = [1, 2, 3, 4, 5];
    for(var i = 0; i < 5; i++) {
        if($scope.steps[i] == 3) {
            $scope.step3 = i + 1;
        }
    }

    $timeout(function () {
        console.log($scope.eventDate);
    },5000);

    $scope.nextStep = function() {
        if($scope.activeStepId < 6) {
            console.log($scope.step3);
            if($scope.activeStepId == $scope.step3 && $scope.activeSubStepId < 3) {
                $scope.activeSubStepId++;
                $window.scrollTo(0, 0);
            } else {
                $scope.activeStepId++;
                $window.scrollTo(0, 0);
            }
        }
    }
    $scope.previousStep = function() {
        if($scope.activeStepId == $scope.step3 && $scope.activeSubStepId > 1) {
            $scope.activeSubStepId--;
            $window.scrollTo(0, 0);
        } else {
            $scope.activeStepId--;
            $window.scrollTo(0, 0);
        }
    }

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.people = [{
        name: 'Option 1',
        age: 12,
        country: 'Group 1'
    }, {
        name: 'Option 2',
        age: 13,
        country: 'Group 2'
    }];

    $scope.numOfSliders=2;
    $scope.numbersOfOptions=8;//Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBoxOption=[];
    for (var i = 0; i < $scope.numbersOfOptions; i++) {
        var box={title:'title'+i, desc:'desc'+i,active:false};
        $scope.activeCheckBoxOption.push(box);
    }
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckboxActive=function(index) {
        if($scope.activeCheckBoxOption[index].active==true) {
            $scope.activeCheckBoxOption[index].active=false;
        }
        else {
            $scope.activeCheckBoxOption[index].active=true;
        }
    }

    $scope.activeCheckBoxOption2=[];
    for (var i = 0; i < $scope.numbersOfOptions; i++) {
        var box={title:'title'+i, desc:'desc'+i,active:false};
        $scope.activeCheckBoxOption2.push(box);
    }
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckboxActive2=function(index) {
        if($scope.activeCheckBoxOption2[index].active==true) {
            $scope.activeCheckBoxOption2[index].active=false;
        }
        else {
            $scope.activeCheckBoxOption2[index].active=true;
        }
    }

    $scope.startCompletedAnimation = function() {
        $scope.current = 0;
        var fillCircle = function () {
            $scope.current += 100;
            if ($scope.current >= 100) {
                $interval.cancel($scope.runAnimation);
            }
        }
        $scope.runAnimation = $interval(fillCircle, 500);
    }
}]);