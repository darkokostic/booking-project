var app = angular.module('booking');
app.service('AddOnsService', function($q,$http,$rootScope, $cookies) {
	return {
		getAddOns: function(url, id) {
            var url=url;
            if(url == null) {
                url='api/location/addons/'+id;
            }
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },
        deleteAddOn: function(AddOnId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/addons/'+AddOnId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        searchService: function(url, querry) {
            if(url==null) {
                url='api/search/addons';
            }
            console.log(querry);
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'search': querry,
                    'location_id': $cookies.get('location_id')
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
        // ,loadMoreAddOns: function(url){
        //     var defer = $q.defer();
        //     $http({
        //         method: 'GET',
        //         url: $rootScope.urlLocation
        //     }).then(function successCallback(response) {
        //             response.data.next_page_url=$rootScope.urlLocation;
        //             defer.resolve(response);
        //         return response;
        //     }, function errorCallback(response) {
        //             defer.reject(response);
        //     });
        //     return defer.promise;
        // }
	}
});