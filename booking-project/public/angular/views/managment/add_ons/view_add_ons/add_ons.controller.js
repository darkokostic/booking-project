var app = angular.module('booking');
app.controller('AddOnsCtrl', ['$scope','AddOnsService', '$location', '$sce','$rootScope','$interval','$timeout','$cookies', function($scope,AddOnsService, $location, $sce,$rootScope,$interval,$timeout,$cookies) {
    
    $scope.addOns=[];
    $scope.addOns.data=[];
    $scope.baseURL=$scope.baseUrlPagination + '/api/location/addons/'+$cookies.get('location_id')+'?page=';
    $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/addons?page='; //default url za paginaciju sa searchom
    $scope.maxSize = 0;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 0;
    $scope.prevURLNumber='';
    $scope.querry='';

    AddOnsService.getAddOns(null, $cookies.get('location_id')).then(function(response) {
        $scope.addOns = response.data.entity;
        $scope.maxSize = response.data.entity.per_page;
        $scope.bigTotalItems = response.data.entity.total;
        $scope.bigCurrentPage = response.data.entity.current_page;
        console.log($scope.addOns);
    }).catch(function(error) {
        console.error(error);
    });

    //delete
    $scope.deleteAddOn = function(AddOnId,index) {
        console.log(AddOnId);
        AddOnsService.deleteAddOn(AddOnId).then(function(response) {
            $scope.addOns.data.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    }

    //funkcija koja se poziva kada se klikne na neko dugme iz paginacije
    $scope.paginate=function() {
        $scope.baseURL=$scope.baseURL+$scope.bigCurrentPage; //postavlja se base url sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.baseURLSearch=$scope.baseURLSearch+$scope.bigCurrentPage; //postavlja se base url search-a sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.checkForPageURL($scope.bigCurrentPage,$scope.prevURLNumber); //Provera na koju stranu sledeca animacija da ide
        $scope.prevURLNumber=$scope.bigCurrentPage;
        if($scope.querry==null||$scope.querry==''){ //Ukoliko ne postoje podaci za search, paginacija se vrsi bez podataka za search
            $scope.loadPaginatedItems($scope.baseURL);
        }else {
            $scope.search($scope.baseURLSearch,$scope.querry); //Ukoliko postoji podaci za search paginacija se vrsi za search podacima
        }
    };
    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije
    $scope.loadPaginatedItems=function(url) {
        $scope.baseURL=$scope.baseUrlPagination + '/api/location/addons/'+$cookies.get('location_id')+'?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/addons?page=';
        console.log($scope.baseURL);
        AddOnsService.getAddOns(url).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.addOns = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(reject) {
            console.log(reject);
        });

    };
    // END Functions for paginations

    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije i search podacima
    $scope.search = function (url,querry) {
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/addons?page=';
        $scope.baseURL=$scope.baseUrlPagination + '/api/location/addons/'+$cookies.get('location_id')+'?page=';
        AddOnsService.searchService(url, querry).then(function(response) {
            console.log(response);
            $scope.checkForAnimationAppearance();
            $scope.addOns = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(error) {
            console.log(error);
        });
    };
}]);