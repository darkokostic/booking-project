var app = angular.module('booking');
app.service('EditAddOnsService', function($q,$http,$rootScope,$cookies) {
	return {
		getAddOn: function(id) {
            console.log(id)
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/addons/'+id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },

        editAddon: function(newAddOn,options,tax,id) {
            console.log(newAddOn)
            var defer = $q.defer();
            $http({
                method: 'POSt',
                data: {
                    'name':newAddOn.name,
                    'description':newAddOn.description,
                    'quantity': newAddOn.quantity,
                    'price': parseInt(newAddOn.price.substring(2,newAddOn.price.length)),
                    'addon_category_id': newAddOn.category.id,
                    'location_id': $cookies.get('location_id'),
                    'unit_of_measure': newAddOn.unit_of_measure,
                    'package_options': options,
                    'is_taxable': tax
                    // 'images'
                },
                url: 'api/addons/'+id
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }, 

        getOptions: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/package/options/'+$cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getAddOnWithOption:function(addOnId) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'id':addOnId,
                    'location_id':$cookies.get('location_id')
                },
                url: '/api/location/options/addon'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});