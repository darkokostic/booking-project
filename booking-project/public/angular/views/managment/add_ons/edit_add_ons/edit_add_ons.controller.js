var app = angular.module('booking');
app.controller('EditAddOnsCtrl', ['$scope','EditAddOnsService','$stateParams','$sce','$window','NewAddOnsService','$cookies','$timeout','$q','$state', function($scope,EditAddOnsService,$stateParams,$sce,$window,NewAddOnsService,$cookies,$timeout,$q,$state) {
	
    $scope.activeCheckbox=[];
    $scope.numbersOfOption=0;
    $scope.categories=[];
    $scope.newAddOn=[];
    $scope.optionsForDTBS=[];
    $scope.newAddOn.image=[];

    $scope.getNumber = function(num) {
        return new Array(num);
    }

    $scope.checkForOptions=function() {
        for (var i = 0; i < $scope.activeCheckbox.length; i++) {
            if($scope.activeCheckbox[i].active==true){
                return false;
            }
        }
        return true;
    }

    $scope.isTaxable = function(tax){
        if(tax.answer=='No'){
            return 0;
        }
        return 1;
    }
    $scope.makeCheckBoxChange=function(index) {
        if($scope.activeCheckbox[index].active==true) {
            $scope.activeCheckbox[index].active=false;
        }
        else {
            $scope.activeCheckbox[index].active=true;
        }
    }

    $scope.cancelEditAddOn= function() {
        $state.go('side_menu.add_ons');
    };

    $scope.editAddon = function() {
        console.log("Ide edit add on");
        $scope.optionsForDTBS=$scope.convertOptionForDatabase($scope.activeCheckbox);
        if ($scope.myDropzone.getQueuedFiles().length > 0) {
            $scope.myDropzone.processQueue();
        }
        else {
            // Upload anyway without files
            var tax=$scope.isTaxable($scope.newAddOn.isTaxable);
            $scope.optionsForDTBS=$scope.stringifyArray($scope.optionsForDTBS);
            EditAddOnsService.editAddon($scope.newAddOn,$scope.optionsForDTBS,tax,$stateParams.id).then(function(response) {
                console.log(response);
                $('body').pgNotification({
                    style: 'simple',
                    message: response.data.message,
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.add_ons');
            }).catch(function(error) {
                $('body').pgNotification({
                    style: 'simple',
                    message: error.data.message,
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            })
        }
        console.log($scope.newAddOn);
    }

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

     $scope.taxable = [{
        answer: 'Yes'
    }, {
        answer: 'No'
    }];

    function convertDataForView(data) {
        var deferred = $q.defer();
        $scope.newAddOn=data;
        $scope.newAddOn.isTaxable=[];
        if(data.is_taxable==1) {
            $scope.newAddOn.isTaxable.answer='Yes';
        }else {
            $scope.newAddOn.isTaxable.answer='No';
        }
        if($scope.newAddOn.price!=null) {
            $scope.newAddOn.price='$ '+$scope.newAddOn.price;
        }
        if(data.all_options!=null){
            $scope.hasOptions = true;
            for (var i = 0; i < data.all_options.length; i++) {
                data.all_options[i].active=false;
                if(data.all_options[i].isAddon==true){
                    data.all_options[i].active=true;
                }
                $scope.activeCheckbox.push(data.all_options[i]);
                console.log($scope.activeCheckbox);
            }
        }else {
            $scope.hasOptions=false;
        }
        NewAddOnsService.getCategories().then(function(response) {
            for (var i = 0; i < response.data.entity.length; i++) {
                if(response.data.entity[i].id==$scope.newAddOn.addon_category_id){
                    $scope.newAddOn.category=response.data.entity[i];
                }else {
                    $scope.categories.push(response.data.entity[i]);
                }
            }
            $scope.categories=response.data.entity;
            deferred.resolve(response);
        return response;
        }).catch(function(error) {
            deferred.reject(error);
            console.log(error);
        return error;
        });
        return deferred.promise;
    }

    $scope.dropzoneConfig = {
        init            : function() {
            $scope.myDropzone = this;
            var responseAddOn = EditAddOnsService.getAddOnWithOption($stateParams.id);
            responseAddOn.then(function(response) {
                console.log(response)
                convertDataForView(response.data.entity);
                $timeout(function(){
                    var mockFile = { name: "Image", size: 12345 };
                    $scope.numbersOfOption=response.data.entity.all_options.length;
                    // Call the default addedfile event handler
                    $scope.myDropzone.emit("addedfile", mockFile);
                    // And optionally show the thumbnail of the file:
                    if($scope.newAddOn.image!=null) {
                        $scope.myDropzone.emit("thumbnail", mockFile,$scope.newAddOn.image.path);
                    }
                });
            },function(reject) {
                console.log(reject);
            });

            this.on('addedfile', function() {
                $scope.$apply(function() {
                    console.log('yo');
                    $scope.filesUploading = true;
                });
            });
            this.on('success', function (file, response) {
                console.log("RADI SRANJE MRTVO");
                $('body').pgNotification({
                    style: 'simple',
                    message: "Successfuly edited Add-on",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.add_ons');
            });
            this.on('error', function(file, xhr){
                console.log('File failed to upload from dropzone 2.', file, xhr);
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occurs",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            });
            this.on('sending', function(file, xhr, formData){
                //this gets triggered
                console.log("FILE:",file);
                var tax=$scope.isTaxable($scope.newAddOn.isTaxable);
                formData.append('name', $scope.newAddOn.name);
                formData.append('description', $scope.newAddOn.description);
                formData.append('price', parseInt($scope.newAddOn.price.substring(2,$scope.newAddOn.price.length)));
                formData.append('location_id', $cookies.get('location_id'));
                formData.append('quantity', $scope.newAddOn.quantity);
                formData.append('addon_category_id', $scope.newAddOn.category.id);
                formData.append('unit_of_measure', $scope.newAddOn.unit_of_measure);
                formData.append('package_options', $scope.optionsForDTBS);
                formData.append('is_taxable', tax);
            });

        },


        headers: {
           'Authorization': 'Bearer ' + $cookies.get('access_token'),
        },
        url             : '/api/addons/'+$stateParams.id,
        parallelUploads : 3,
        maxFiles:1,
        paramName: 'images',
        uploadMultiple  : false,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        maxFileSize     : 30,
        addRemoveLinks  : 'dictCancelUpload'
    };
}]);