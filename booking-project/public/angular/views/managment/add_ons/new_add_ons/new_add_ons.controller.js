var app = angular.module('booking');
app.controller('NewAddOnsCtrl', ['$scope','NewAddOnsService', '$sce', '$timeout','$cookies','$state', function($scope,NewAddOnsService, $sce, $timeout, $cookies, $state) {
    $scope.newAddOn=[];
    $scope.dzMethods = {};
    $scope.categories=[];
    $scope.options=[];
    $scope.numbersOfBoxes=5; //Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBox=[];
    $scope.optionsForDTBS=[];

    $scope.isTaxable = function(tax){
        if(tax.answer=='No'){
            return 0;
        }
        return 1;
    }

    $scope.dropzoneConfig = {
        init            : function() {
            $scope.myDropzone = this;
            this.on('addedfile', function() {
                $scope.$apply(function() {
                    console.log('yo');
                    $scope.filesUploading = true;
                });
            });
            this.on('success', function (file, response) {
                $('body').pgNotification({
                    style: 'simple',
                    message: "Successfuly added Add-on",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.add_ons');
            });
            this.on('error', function(file, xhr){
                console.log('File failed to upload from dropzone 2.', file, xhr);
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occurs",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            });
            this.on('sending', function(file, xhr, formData){
                //this gets triggered
                // var csrftoken = $cookies.get('access_token');
                // formData.append("_token",csrftoken);
                var tax=$scope.isTaxable($scope.newAddOn.isTaxable);
                console.log($scope.optionsForDTBS)
                console.log('sending');
                formData.append('name', $scope.newAddOn.name);
                formData.append('description', $scope.newAddOn.description);
                formData.append('price', parseInt($scope.newAddOn.price.substring(2,$scope.newAddOn.price.length)));
                formData.append('location_id', $cookies.get('location_id'));
                formData.append('quantity', $scope.newAddOn.quantity);
                formData.append('addon_category_id', $scope.newAddOn.category.id);
                formData.append('unit_of_measure', $scope.newAddOn.unitOfMeasure);
                formData.append('package_options', $scope.optionsForDTBS);
                formData.append('is_taxable', tax);
            });
        },

        headers: {
           'Authorization': 'Bearer ' + $cookies.get('access_token'),
        },
        url             : '/api/addons',
        parallelUploads : 3,
        paramName: 'images',
        uploadMultiple  : false,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        maxFileSize     : 30,
        addRemoveLinks  : 'dictCancelUpload'
    };
    
    $scope.checkForOptions=function() {
        for (var i = 0; i < $scope.activeCheckBox.length; i++) {
            if($scope.activeCheckBox[i].active==true){
                return false;
            }
        }
        return true;
    }

    NewAddOnsService.getCategories().then(function(response) {
        console.log(response);
        $scope.categories=response.data.entity;
    }).catch(function(error) {
        console.log(error);
    });

    NewAddOnsService.getOptions().then(function(response) {
        console.log(response)
        $scope.numbersOfBoxes=response.data.entity.data.length;
        for (var i = 0; i < response.data.entity.data.length; i++) {
            response.data.entity.data[i].active=false;
            $scope.activeCheckBox.push(response.data.entity.data[i]);
        }
        console.log($scope.activeCheckBox)

    }).catch(function(error) {
        console.log(error);
    });

    $scope.getNumber = function(num) {
        return new Array(num);
    }

    $scope.makeCheckBoxChange=function(index) {
        if($scope.activeCheckBox[index].active==true) {
            $scope.activeCheckBox[index].active=false;
        }
        else {
            $scope.activeCheckBox[index].active=true;
        }
    }

    //Odavde za already exist checkbox
    //add addons
    $scope.addAddon = function(){
        if(!$scope.myDropzone.files || !$scope.myDropzone.files.length){        
            $('body').pgNotification({
            style: 'simple',
            message: "Please insert a photo",
            position: 'top-right',
            timeout: 4000,
            type: 'success'
        }).show();
        }else {
            $scope.optionsForDTBS=$scope.convertOptionForDatabase($scope.activeCheckBox);
            $scope.myDropzone.processQueue();
        }
    };

	$scope.test= function() {
		console.log('Test uspesan');	
	};

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.people = [{
        name: 'Adam',
        email: 'adam@email.com',
        age: 12,
        country: 'United States'
    }, {
        name: 'pera',
        email: 'pera@email.com',
        age: 13,
        country: 'Srbija'
    }];
	
    $scope.taxable = [{
        answer: 'Yes'
    }, {
        answer: 'No'
    }];

    $scope.unitOfMeasure = [{
        value: 'Quantity'
    }, {
        value: 'Wait'
    }];

    $scope.category = [{
        value: 'Category 1'
    }, {
        value: 'Category 2'
    }];
}]);