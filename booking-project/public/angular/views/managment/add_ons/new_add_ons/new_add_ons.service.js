var app = angular.module('booking');
app.service('NewAddOnsService', function($q,$http,$cookies) {
	return {
		addAddon: function(newAddOn,options) {
            console.log(newAddOn)
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':newAddOn.name,
                    'description':newAddOn.description,
                    'quantity': newAddOn.quantity,
                    'price': newAddOn.price,
                    'addon_category_id': newAddOn.category.id,
                    'location_id': $cookies.get('location_id'),
                    'unit_of_measure': newAddOn.unitOfMeasure,
                    'package_options': options
                    // 'images'
                },
                url: 'api/addons'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },  

        getOptions: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/package/options/'+$cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getCategories: function(newAddOn) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/addon-categories'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }

    }
});