var app = angular.module('booking');
app.service('EditUserService', function($q,$http) {
	return {
        editUser: function(user, id) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'firstname':user.firstname,
                    'lastname':user.lastname,
                    'username': user.username,
                    'password': user.password,
                    'phone': user.phone,
                    'email': user.email,
                    'status': user.status,
                    'permissions': user.permissions,
                    'menu_position': user.menuPosition,
                    'template_src': user.userTheme
                },
                url: 'api/users/' + id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },

		getUser: function(id) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/users/' + id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        }
	};
});