var app = angular.module('booking');
app.controller('EditUserCtrl', ['$scope', 'EditUserService', '$stateParams', '$sce', 'ThemeService', function ($scope, EditUserService, $stateParams, $sce, ThemeService) {
    $scope.builder = [];
    $scope.builder.theme = [];
    $scope.builder.colorId = [];
    $scope.app = [];
    $scope.app.layout = [];
    $scope.app.layout.theme = [];
    $scope.user = [];
    $scope.menuPosition = 'Left';
    $scope.lockScreenPosition = 'Yes';
    $scope.allPermissions = [{
        name: "Manage Packages",
        value: "manage-packages"
    }, {
        name: "Manage Options",
        value: "manage-options"
    }, {
        name: "Manage Add ons",
        value: "manage-add-ons"
    }, {
        name: "Manage Time Slots",
        value: "manage-time-slots"
    }, {
        name: "Make package available",
        value: "make-packages-available"
    }, {
        name: "Edit orders",
        value: "edit-orders"
    }, {
        name: "Reschedule orders",
        value: "reschedule-orders"
    }, {
        name: "Cancel orders",
        value: "cancel-orders"
    }, {
        name: "Book event",
        value: "book-event"
    }, {
        name: "Place Event Date/Time",
        value: "place-event-date-time"
    }, {
        name: "Make time slot private",
        value: "time-slot-private"
    }, {
        name: "Add one time event",
        value: "add-one-time-event"
    }];
    $scope.checkedPermissions = [];

    EditUserService.getUser($stateParams.id)
        .then(function(response){
            console.log(response);
            $scope.user = response.data.entity;
            if ($scope.user.status){
                $scope.selectedStatus = $scope.userStatus[0];
            } else {
                $scope.selectedStatus = $scope.userStatus[1];
            }
            if($scope.user.user_settings.menu_position == "left") {
                $scope.checkboxModel = false;
                ThemeService.foo = '/css/pages.css';
                ThemeService.notifyObservers();
                $scope.menuPosition = 'Left';
            } else {
                $scope.checkboxModel = true;
                ThemeService.foo = '/css/pages.rtl.css';
                ThemeService.notifyObservers();
                $scope.menuPosition = 'Right';
            }
            for(var i = 0; i < $scope.allPermissions.length; i++) {
                for(var j = 0; j < $scope.user.permissions.length; j++) {
                    if($scope.allPermissions[i].value == $scope.user.permissions[j].name) {
                        $scope.allPermissions[i].active = true;
                        $scope.checkedPermissions.push($scope.allPermissions[i].value);
                    }
                }
            }
            var userSettingsTheme = $scope.user.user_settings.template_src.split('/');
            var theme;
            if(userSettingsTheme.length == 3) {
                theme = $scope.user.user_settings.template_src.split('/')[2].split('.')[0];
            } else {
                theme = $scope.user.user_settings.template_src.split('/')[3].split('.')[0];
            }
             
            if(theme == "pages") {
                $scope.builder.theme = "default";
                $scope.app.layout.theme = 'pages/css/pages.css';
                $scope.userTheme = 'pages/css/pages.css';
            } else {
                $scope.builder.theme = theme;
                $scope.userTheme = 'pages/css/themes/' + theme + '.css';
                $scope.app.layout.theme = 'pages/css/themes/' + theme + '.css';
            }
            
            console.log($scope.checkedPermissions);
        }).catch(function(error){
            console.log(error);
    });

    $scope.makeCheckBoxChange = function(index) {
        if($scope.allPermissions[index].active) {
            $scope.allPermissions[index].active = false;
            for(var i = 0; i < $scope.checkedPermissions.length; i++) {
                if($scope.checkedPermissions[i] == $scope.allPermissions[index].value) {
                    $scope.checkedPermissions.splice(i, 1);
                }
            }
        } else {
            $scope.allPermissions[index].active = true;
            $scope.checkedPermissions.push($scope.allPermissions[index].value);
        }
    };

    $scope.onSelectedStatus = function(item) {
        if(item.status == 'Active') {
            $scope.user.status = 1;
        } else {
            $scope.user.status = 0;
        }
    };

    $scope.changeMenuPosition = function (check) {
        console.log(check)
        if (check == true) {
            console.log(check)
            ThemeService.foo = '/css/pages.rtl.css';
            ThemeService.notifyObservers();
            $scope.menuPosition = 'Right';
        }
        else {
            console.log(check)
            ThemeService.foo = '/css/pages.css';
            ThemeService.notifyObservers();
            $scope.menuPosition = 'Left';
        }
    }

    $scope.lockScreen = function (check) {
        console.log(check)
        if (check == true) {
            console.log(check)
            $scope.lockScreenPosition = 'No';
        }
        else {
            console.log(check)
            $scope.lockScreenPosition = 'Yes';
        }
    }


    $scope.trustAsHtml = function (value) {
        return $sce.trustAsHtml(value);
    };


    $scope.userStatus = [{
        status: 'Active'
    }, {
        status: 'Inactive'
    }];

    $scope.setTheme = function(theme, id) {
        $scope.builder.theme = theme;
        $scope.builder.colorId = id;

        if (theme == 'default') {
            $scope.app.layout.theme = 'pages/css/pages.css';
            $scope.userTheme = 'pages/css/pages.css';
        } else {
            $scope.userTheme = 'pages/css/themes/' + theme + '.css';
            $scope.app.layout.theme = 'pages/css/themes/' + theme + '.css';
        }
    };

    $scope.editUser = function() {
        $scope.user.menuPosition = $scope.menuPosition;
        $scope.user.userTheme = $scope.userTheme;
        $scope.user.permissions = $scope.checkedPermissions;
        EditUserService.editUser($scope.user, $stateParams.id)
        .then(function(response) {
            console.log(response);
        }).catch(function(response) {
            console.log(response);
        }); 
    };
}]);