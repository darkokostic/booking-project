var app = angular.module('booking');
app.service('NewUserService', function($q,$http) {
    return {
        addUser: function(newUser) {
            console.log(newUser)
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'firstname':newUser.firstname,
                    'lastname':newUser.lastname,
                    'username': newUser.username,
                    'password': newUser.password,
                    'phone': newUser.phone,
                    'email': newUser.email,
                    'status': newUser.status,
                    'permissions': newUser.permissions,
                    'menu_position': newUser.menuPosition,
                    'template_src': newUser.userTheme
                },
                url: 'api/users'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    }
});