var app = angular.module('booking');
app.controller('NewUserCtrl', ['$scope','NewUserService', '$sce','ThemeService', '$window','$cookies', function($scope,NewUserService, $sce,ThemeService, $window,$cookies) {

    $scope.builder = [];
    $scope.builder.theme = [];
    $scope.builder.colorId = [];
    $scope.app = [];
    $scope.app.layout = [];
    $scope.app.layout.theme = [];
    $scope.menuPosition = 'Left';
    $scope.lockScreenPosition = 'Yes';
    $scope.newUser=[];
    $scope.allPermissions = [{
        name: "Manage Packages",
        value: "manage-packages"
    }, {
        name: "Manage Options",
        value: "manage-options"
    }, {
        name: "Manage Add ons",
        value: "manage-add-ons"
    }, {
        name: "Manage Time Slots",
        value: "manage-time-slots"
    }, {
        name: "Make package available",
        value: "make-packages-available"
    }, {
        name: "Edit orders",
        value: "edit-orders"
    }, {
        name: "Reschedule orders",
        value: "reschedule-orders"
    }, {
        name: "Cancel orders",
        value: "cancel-orders"
    }, {
        name: "Book event",
        value: "book-event"
    }, {
        name: "Place Event Date/Time",
        value: "place-event-date-time"
    }, {
        name: "Make time slot private",
        value: "time-slot-private"
    }, {
        name: "Add one time event",
        value: "add-one-time-event"
    }];
    $scope.checkedPermissions = [];
    $scope.userTheme = 'pages/css/pages.css';

    $scope.changeMenuPosition = function(check) {
        if (check=='right') {
         //   ThemeService.foo = '/css/pages.rtl.css';
            //ThemeService.notifyObservers();
            $scope.menuPosition = 'right';
        }
        else {
           // ThemeService.foo = '/css/pages.css';
           // ThemeService.notifyObservers();
            $scope.menuPosition = 'left';
        }
    }

    $scope.lockScreen = function(check) {
        console.log(check)
        if (check==true) {
            console.log(check)
            $scope.lockScreenPosition = 'No';
        }
        else {
            console.log(check)
            $scope.lockScreenPosition = 'Yes';
        }
    }

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };
    
    $scope.userStatus = [{
        status: 'Active'
    }, {
        status: 'Inactive'
    }];

    $scope.onSelectedStatus = function(item) {
        if(item.status == 'Active') {
            $scope.newUser.status = 1;
        } else {
            $scope.newUser.status = 0;
        }
    };

    $scope.setTheme = function(theme, id) {
        $scope.builder.theme = theme;
        $scope.builder.colorId = id;

        if (theme == 'default') {
            $scope.app.layout.theme = 'pages/css/pages.css';
            $scope.userTheme = 'pages/css/pages.css';
        } else {
            $scope.userTheme = 'pages/css/themes/' + theme + '.css';
            $scope.app.layout.theme = 'pages/css/themes/' + theme + '.css';
        }
    };

    $scope.makeCheckBoxChange = function(index) {
        if($scope.allPermissions[index].active) {
            $scope.allPermissions[index].active = false;
            for(var i = 0; i < $scope.checkedPermissions.length; i++) {
                if($scope.checkedPermissions[i] == $scope.allPermissions[index].value) {
                    $scope.checkedPermissions.splice(i, 1);
                }
            }
        } else {
            $scope.allPermissions[index].active = true;
            $scope.checkedPermissions.push($scope.allPermissions[index].value);
        }
    };

    $scope.dropzoneConfig = {
        init            : function() {
            $scope.myDropzone = this;
            this.on('addedfile', function() {
                $scope.$apply(function() {
                    console.log('yo');
                    $scope.filesUploading = true;
                });
            });
            this.on('success', function (file, response) {
                $('body').pgNotification({
                    style: 'simple',
                    message: "Successfuly added Add-on",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.add_ons');
            });
            this.on('error', function(file, xhr){
                console.log('File failed to upload from dropzone 2.', file, xhr);
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occurs",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            });
            this.on('sending', function(file, xhr, formData){
                console.log($scope.newUser.permissions)
                formData.append('firstname', $scope.newUser.firstname);
                formData.append('lastname', $scope.newUser.lastname);
                formData.append('username', $scope.newUser.username);
                formData.append('password', $scope.newUser.password);
                formData.append('phone', $scope.newUser.phone);
                formData.append('email', $scope.newUser.email);
                formData.append('status', $scope.newUser.status);
                formData.append('permissions', $scope.newUser.permissions);
                formData.append('menu_position', $scope.newUser.menuPosition);
                formData.append('template_src', $scope.newUser.userTheme);
            });
        },

        headers: {
           'Authorization': 'Bearer ' + $cookies.get('access_token'),
        },
        url             : '/api/users',
        parallelUploads : 3,
        paramName: 'images',
        uploadMultiple  : false,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        maxFileSize     : 30,
        addRemoveLinks  : 'dictCancelUpload'
    };
    $scope.addUser = function () {
        $scope.newUser.menuPosition = $scope.menuPosition;
        $scope.newUser.userTheme = $scope.userTheme;
        $scope.newUser.permissions = $scope.chechedPermissions;
        if(!$scope.myDropzone.files || !$scope.myDropzone.files.length){        
            NewUserService.addUser($scope.newUser).then(function(response){
               
            }).catch(function(error){

            });
        }else {
            $scope.myDropzone.processQueue();
        }
    };

    
}]);