var app = angular.module('booking');
app.controller('UsersCtrl', ['$scope','UsersService', '$location', '$sce','$timeout', '$cookies', function($scope,UsersService, $location, $sce, $timeout, $cookies) {
	
    $scope.users=[];
    $scope.baseURL=$scope.baseUrlPagination +'/api/users/?page='; //default url za paginaciju sa obicnim podacima
    $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/users?page='; //default url za paginaciju sa searchom
    $scope.maxSize = 0;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 0;
    $scope.prevURLNumber='';
    $scope.querry='';

    UsersService.getUsers(null).then(function(response) {
        $scope.users = response.data.entity;
        $scope.maxSize = response.data.entity.per_page;
        $scope.bigTotalItems = response.data.entity.total;
        $scope.bigCurrentPage = response.data.entity.current_page;
    }).catch(function(error) {
    });

    //delete
    $scope.deleteUser = function(UserId,index) {
        console.log(UserId);
        UsersService.deleteUser(UserId).then(function(response) {
            $scope.users.data.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    }
    //funkcija koja se poziva kada se klikne na neko dugme iz paginacije
    $scope.paginate=function() {
            $scope.baseURL=$scope.baseURL+$scope.bigCurrentPage; //postavlja se base url sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
            $scope.baseURLSearch=$scope.baseURLSearch+$scope.bigCurrentPage; //postavlja se base url search-a sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
            $scope.checkForPageURL($scope.bigCurrentPage,$scope.prevURLNumber); //Provera na koju stranu sledeca animacija da ide
            $scope.prevURLNumber=$scope.bigCurrentPage;
        if($scope.querry==null||$scope.querry==''){ //Ukoliko ne postoje podaci za search, paginacija se vrsi bez podataka za search
            $scope.loadPaginatedItems($scope.baseURL);
        }else {
            $scope.search($scope.baseURLSearch,$scope.querry); //Ukoliko postoji podaci za search paginacija se vrsi za search podacima
        }
    };
    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije
    $scope.loadPaginatedItems=function(url) {
        $scope.baseURL=$scope.baseUrlPagination +'/api/users/?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/users?page=';
        console.log($scope.baseURL);
        UsersService.getUsers(url).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.users=response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(reject) {
            console.log(reject);
        });
        
    };
    // END Functions for paginations

    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije i search podacima 
    $scope.search = function (url,querry) {
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/users?page=';
        $scope.baseURL=$scope.baseUrlPagination +'/api/users/?page=';
        UsersService.searchService(url, querry).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.users = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(error) {
            console.log(error);
        });
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };
}]);