var app = angular.module('booking');
app.service('UsersService', function($q,$http,$cookies) {
	return {
		getUsers: function(url) {
		    var url = url;
		    if (url == null) {
		        url = 'api/users';
            }
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        deleteUser: function(UserId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/users/'+UserId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        searchService: function(url, querry) {
            if(url==null) {
                url='api/search/users';
            }
            console.log(querry);
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'search': querry,
                    'location_id': $cookies.get('location_id')
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	};
});