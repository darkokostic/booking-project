var app = angular.module('booking');
app.controller('EditOrderController', ['$scope','EditOrderService', '$location', '$sce','$rootScope','$timeout','$cookies', function($scope,EditOrderService, $location, $sce,$rootScope,$timeout,$cookies) {
	 $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.people = [{
        name: 'Option 1',
        age: 12,
        country: 'Group 1'
    }, {
        name: 'Option 2',
        age: 13,
        country: 'Group 2'
    }];
}]);