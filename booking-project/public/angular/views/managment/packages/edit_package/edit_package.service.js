var app = angular.module('booking');
app.service('EditPackageService', function($q,$http,$rootScope,$cookies) {
	return {
        getPackage: function(id) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/packages/' + id
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response;
            }, function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
        },

        editPackage: function(package,options) {

            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':package.name,
                    'description':package.description,
                    'package_options': options,
                    'location_id': $cookies.get('location_id')
                },
                url: 'api/packages/'+package.id
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response;
            }, function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
        },

        getPackageOptions: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/package/options/' + $cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response;
            }, function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
        }
	}
});