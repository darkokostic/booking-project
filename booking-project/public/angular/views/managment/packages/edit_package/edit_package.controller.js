var app = angular.module('booking');
app.controller('EditPackageCtrl', ['$scope','EditPackageService','$location','$stateParams','$window','$cookies','$timeout', '$state',function($scope,EditPackageService,$location,$stateParams, $window,$cookies,$timeout,$state) {
    $scope.package = [];
    $scope.selectedOptions = [];

    $scope.cancelEditOption = function() {
        console.log($scope.option);
        $window.history.back();
    };

    $scope.makeCheckBoxChange = function(index) {
        if($scope.packageOptions[index].active) {
            $scope.packageOptions[index].active = false;
            for(var i = 0; i < $scope.selectedOptions.length; i++) {
                if($scope.selectedOptions[i] == $scope.packageOptions[index].id) {
                    $scope.selectedOptions.splice(i, 1);
                    console.log($scope.selectedOptions);
                }
            }
        } else {
            $scope.packageOptions[index].active = true;
            $scope.selectedOptions.push($scope.packageOptions[index].id);
        }
    };
    $scope.dropzoneConfig = {
        init            : function() {
            $scope.myDropzone = this;

            EditPackageService.getPackage($stateParams.id)
            .then(function(response) {
                $scope.package = response.entity;
                console.log(response);
                for(var i = 0; i < response.entity.package_options.length; i++) {
                    $scope.selectedOptions.push(response.entity.package_options[i].id);
                } 
                EditPackageService.getPackageOptions().then(function(response) {
                    $timeout(function(){
                        var mockFile = { name: "Image", size: 12345 };
                        // Call the default addedfile event handler
                        $scope.myDropzone.emit("addedfile", mockFile);
                        // And optionally show the thumbnail of the file:
                        if($scope.package.images[0]!=null) {
                            $scope.myDropzone.emit("thumbnail", mockFile,$scope.package.images[0].path);
                        }
                    });
                    $scope.packageOptions = response.entity.data;
                    for(var i = 0; i < $scope.packageOptions.length; i++) {
                        for(j = 0; j < $scope.selectedOptions.length; j++) {
                            if($scope.packageOptions[i].id == $scope.selectedOptions[j]) {
                                $scope.packageOptions[i].active = true;
                            }
                        }
                    }
                    console.log($scope.packageOptions);
                },function(reject) {
                    console.log(reject);
                });

            },function(reject) {
                console.log(reject);
            });

            this.on('addedfile', function() {
                $scope.$apply(function() {
                    console.log('yo');
                    $scope.filesUploading = true;
                });
            });
            this.on('success', function (file, response) {
                console.log("RADI SRANJE MRTVO");
                $('body').pgNotification({
                    style: 'simple',
                    message: "Successfuly edited Package",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.packages');
            });
            this.on('error', function(file, xhr){
                console.log('File failed to upload from dropzone 2.', file, xhr);
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occured",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            });
            this.on('sending', function(file, xhr, formData){
                //this gets triggered
                formData.append('name', $scope.package.name);
                formData.append('description', $scope.package.description);
                formData.append('package_options', $scope.stringifyArray($scope.selectedOptions));
                formData.append('location_id', $cookies.get('location_id'));
            });

        },

        headers: {
           'Authorization': 'Bearer ' + $cookies.get('access_token'),
        },
        url             : 'api/packages/'+$stateParams.id,
        parallelUploads : 3,
        maxFiles:1,
        paramName: 'images',
        uploadMultiple  : false,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        maxFileSize     : 30,
        addRemoveLinks  : 'dictCancelUpload'
    };

    $scope.editPackage = function() {
        console.log("Ide edit package");
        if ($scope.myDropzone.getQueuedFiles().length > 0) {
            $scope.myDropzone.processQueue();
        }
        else {
            // Upload anyway without files
            EditPackageService.editPackage($scope.package,$scope.stringifyArray($scope.selectedOptions)).then(function(response) {
                console.log(response);
                $('body').pgNotification({
                    style: 'simple',
                    message: response.data.message,
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.packages');
            }).catch(function(error) {
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occured",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            })
        }
    }

}]);