var app = angular.module('booking');
app.service('PackageService', function($q,$http,$rootScope, $cookies) {
	return {
		getPackageOptions: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: $rootScope.urlLocation
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },loadMorePackages: function(url){
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: $rootScope.urlLocation
            }).then(function successCallback(response) {
                    response.data.next_page_url=$rootScope.urlLocation;
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },
        getPackages: function(url, id) {
            console.log(url);
            var url = url;
            if (url == null) {
                url = 'api/location/packages/' + id;
            }
            var defer = $q.defer();
            $http({
                method: 'GET',
                data: {
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        deletePackage: function(PackageId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/packages/'+PackageId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        searchService: function(url,querry) {
            if(url==null) {
                url='api/search/packages';
            }
            console.log(querry);
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'search': querry,
                    'location_id': $cookies.get('location_id')
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	};
});