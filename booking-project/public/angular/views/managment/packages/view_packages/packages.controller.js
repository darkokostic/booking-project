var app = angular.module('booking');
app.controller('PackageCtrl', ['$scope','PackageService', '$location', '$sce', '$rootScope','$timeout','$cookies', function($scope,PackageService, $location, $sce,$rootScope,$timeout,$cookies) {
	
    $scope.packages=[];
    // $scope.viewMoreActivePackages = true;
    // $scope.events.data=[];
    $scope.baseURL=$scope.baseUrlPagination +'/api/location/packages/'+$cookies.get('location_id')+'?page=';
    $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/packages?page='; //default url za paginaciju sa searchom
    $scope.maxSize = 0;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 0;
    $scope.prevURLNumber='';
    $scope.querry='';
    
        // var responsePackages = PackageService.getPackageOptions();
        // responsePackages.then(function(response) {
        //     for(var i=0;i<11;i++){
        //         $scope.packages.push(response.data[i]);
        //     }
        //     console.log(response);
        // },function(reject) {
        //     console.log(reject);
        // });

    PackageService.getPackages(null, $cookies.get('location_id')).then(function(response) {
        console.log(response);
        $scope.packages = response.data.entity;
        $scope.maxSize = response.data.entity.per_page;
        $scope.bigTotalItems = response.data.entity.total;
        $scope.bigCurrentPage = response.data.entity.current_page;
        $scope.message = response.data.message;
        console.log($scope.packages);
    }).catch(function(error) {
        console.error(error);
    });

    //delete
    $scope.deletePackage = function(PackageId,index) {
        console.log(PackageId);
        PackageService.deletePackage(PackageId).then(function(response) {
            $scope.packages.data.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            console.log($scope.packages);
            console.log(response);
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    }

    //funkcija koja se poziva kada se klikne na neko dugme iz paginacije
    $scope.paginate=function() {
        $scope.baseURL=$scope.baseURL+$scope.bigCurrentPage; //postavlja se base url sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.baseURLSearch=$scope.baseURLSearch+$scope.bigCurrentPage; //postavlja se base url search-a sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.checkForPageURL($scope.bigCurrentPage,$scope.prevURLNumber); //Provera na koju stranu sledeca animacija da ide
        $scope.prevURLNumber=$scope.bigCurrentPage;
        if($scope.querry==null||$scope.querry==''){ //Ukoliko ne postoje podaci za search, paginacija se vrsi bez podataka za search
            $scope.loadPaginatedItems($scope.baseURL);
        }else {
            $scope.search($scope.baseURLSearch,$scope.querry); //Ukoliko postoji podaci za search paginacija se vrsi za search podacima
        }
    };
    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije
    $scope.loadPaginatedItems=function(url) {
        $scope.baseURL=$scope.baseUrlPagination +'/api/location/packages/'+$cookies.get('location_id')+'?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/packages?page=';
        console.log($scope.baseURL);
        PackageService.getPackages(url).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.packages = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(reject) {
            console.log(reject);
        });

    };
    // END Functions for paginations

    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije i search podacima
    $scope.search = function (url,querry) {
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/packages?page=';
        $scope.baseURL=$scope.baseUrlPagination +'/api/location/packages/'+$cookies.get('location_id')+'?page=';
        PackageService.searchService(url, querry).then(function(response) {
            console.log(response);
            $scope.checkForAnimationAppearance();
            $scope.packages = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(error) {
            console.log(error);
        });
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };
}]);