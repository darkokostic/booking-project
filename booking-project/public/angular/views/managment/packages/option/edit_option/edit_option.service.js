var app = angular.module('booking');
app.service('EditOptionService', function($q,$http) {
	return {
		getAddOns: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                data: {},
                url: 'https://jsonplaceholder.typicode.com/posts'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});