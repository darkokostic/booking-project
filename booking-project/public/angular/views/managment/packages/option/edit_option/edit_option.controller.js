var app = angular.module('booking');
app.controller('EditOptionCtrl', ['$scope','EditOptionService', '$sce','$window', function($scope,EditOptionService, $sce,$window) {

    $scope.option=[];
    $scope.option.scheduledBlocks=[];
    $scope.container = {items: [], effectAllowed: 'move'};
    $scope.addOns=[];
    
    $scope.editOption= function() {
        console.log($scope.option);
        $window.history.back();
    };

    //Odavde za already exist checkbox
    $scope.numbersOfAlreadyExistsBoxes=5; //Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBoxAlreadyExists=[];
    for (var i = 0; i < $scope.numbersOfAlreadyExistsBoxes; i++) {
        var box={title:'title'+i, desc:'desc'+i,active:false};
        $scope.activeCheckBoxAlreadyExists.push(box);
    }
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckBoxAlreadyExistChange=function(index) {
        if($scope.activeCheckBoxAlreadyExists[index].active==true) {
            $scope.activeCheckBoxAlreadyExists[index].active=false;
        }
        else {
            $scope.activeCheckBoxAlreadyExists[index].active=true;
        }
    }
    // Dovde

    //Odavde za addOn checkboxeve
    $scope.numbersOfAddOnBoxes=10; //Changing value of this variable you will change number of boxes apears in view.
    for (var i = 0; i < $scope.numbersOfAddOnBoxes; i++) {
        $scope.container.items.push({label: 'title'+i, effectAllowed: 'move'});
    }
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckBoxAddOnChange=function(index) {
        if($scope.container.items[index].active==true) {
            $scope.container.items[index].active=false;
        }
        else {
            $scope.container.items[index].active=true;
        }
    }
    // Dovde

    EditOptionService.getAddOns().then(function(response) {
        console.log(response);
        $scope.addOns=response.data.entity;
    }).catch(function(error) {
        console.log(error);
    });

    $scope.addOption= function() {
        console.log($scope.option);
        $window.history.back();
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.people = [{
        name: 'Adam',
        email: 'adam@email.com',
        age: 12,
        country: 'United States'
    }, {
        name: 'pera',
        email: 'pera@email.com',
        age: 13,
        country: 'Srbija'
    }];

    $scope.askGuestCount = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.askGuestCount2 = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.askGuestCount3 = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.partyDuration = [{
        time: '2 hr'
    }, {
        time: '1 hr 30 min'
    }]

    $scope.birthdayPersonFree = [{
        free: 'Yes'
    }, {
        free: 'No'
    }]

    $scope.twoGuestOfHonor = [{
        numberOfGuests: 'Yes'
    }, {
        numberOfGuests: 'No'
    }]

    $scope.firstParty = [{
        party: '8 AM'
    }, {
        party: '9 AM'
    }]

    $scope.lastParty = [{
        party: '8 PM'
    }, {
        party: '9 PM'
    }]

    $scope.interval = [{
        time: '2 hrs'
    }, {
        time: '3 hrs'
    }]

    $scope.consecutives = [{
        number: '1'
    }, {
        number: '2'
    }]

    $scope.pricingModel = [{
        price: 'Pricing Model 1'
    }, {
        price: 'Pricing Model 2'
    }]

    $scope.balanceDue = [{
        value: 'Add event'
    }, {
        value: '5 days after the event'
    }, {
        value: '10 days after the event'
    }]
    $scope.dragoverCallback = function(index, external, type, callback) {
        $scope.logListEvent('dragged over', index, external, type);
        // Invoke callback to origin for container types.
        if (type == 'container' && !external) {
            console.log('Container being dragged contains ' + callback() + ' items');
        }
        return index < 10; // Disallow dropping in the third row.
    };

    $scope.dropCallback = function(index, item, external, type) {
        $scope.logListEvent('dropped at', index, external, type);
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };

    $scope.logEvent = function(message) {
        console.log(message);
    };

    $scope.logListEvent = function(action, index, external, type) {
        var message = external ? 'External ' : '';
        message += type + ' element was ' + action + ' position ' + index;
        console.log(message);
    };

    // Initialize model
    $scope.model = [[], []];
    var id = 10;
    angular.forEach(['all', 'move', 'copy', 'link', 'copyLink', 'copyMove'], function(effect, i) {
        // $scope.container = {items: [], effectAllowed: 'move'};
        // for (var k = 0; k < 6; ++k) {
        //     $scope.container.items.push({label: 'move' + ' ' + id++, effectAllowed: 'move'});
        // }
        $scope.model[i % $scope.model.length].push($scope.container);
    });

    $scope.$watch('model', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);
}]);