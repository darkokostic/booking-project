var app = angular.module('booking');
app.controller('AddOptionCtrl', ['$scope','AddOptionService', '$sce','$rootScope','$window','NewScheduledBlocksService','$state', function($scope,AddOptionService,$sce,$rootScope,$window,NewScheduledBlocksService,$state) {

    $scope.scheduledBlock=[];
    $scope.scheduledBlock.days=[];
    $scope.scheduledBlock.days.workingDays=[];
    $scope.scheduledBlock.days.weekend=[];
    $scope.option=[];
    $scope.option.scheduledBlocks=[];
    $scope.container = {items: [], effectAllowed: 'move'};
    $scope.addOns=[];
    $scope.activeCheckBox=[];
    $scope.options=[];
    $scope.options.steps=[];
    $scope.tier1=[];
    $scope.scheduledBlock.optionsId=[];
    $scope.option.addOns=[];
    $scope.option.tier1={guests_included:0, per_additional_guest:0,min_guest: 0,max_guests: 0,label_singular: '', label_plural: '', ask_guest_count: false};

    $scope.cancelEditOption= function() {
        console.log($scope.option);
        $state.go('side_menu.packages');
    };
    //Scheduled blocks
     $scope.makeCheckBoxChange=function(index) {
        if($scope.activeCheckBox[index].active==true) {
            $scope.activeCheckBox[index].active=false;
        }
        else {
            $scope.activeCheckBox[index].active=true;
        }
    }

    $scope.moment = function(date) {
        return moment(date);
    };

    $scope.checkForOptions=function() {
        for (var i = 0; i < $scope.activeCheckBox.length; i++) {
            if($scope.activeCheckBox[i].active==true){
                return false;
            }
        }
        return true;
    }
    AddOptionService.getSchedules().then(function(response) {
        console.log(response);
        $scope.numbersOfBoxes=response.data.entity.data.length;
        for (var i = 0; i < response.data.entity.data.length; i++) {
            response.data.entity.data[i].active=false;
            $scope.activeCheckBox.push(response.data.entity.data[i]);
        }
        console.log($scope.activeCheckBox)

    }).catch(function(error) {
        console.log(error);
    });

    $scope.from=[{start:'01 AM'},{start:'02 AM'},{start:'03 AM'},{start:'04 AM'},{start:'05 AM'},{start:'06 AM'},{start:'07 AM'},{start:'08 AM'},{start:'09 AM'},{start:'10 AM'},{start:'11 AM'},{start:'12 AM'},{start:'01 PM'},{start:'02 PM'},{start:'03 PM'},{start:'04 PM'},{start:'05 PM'},{start:'06 PM'},{start:'07 PM'},{start:'08 PM'},{start:'09 PM'},{start:'10 PM'},{start:'11 PM'},{start:'12 PM'}];
    $scope.to=[{end:'01 AM'},{end:'02 AM'},{end:'03 AM'},{end:'04 AM'},{end:'05 AM'},{end:'06 AM'},{end:'07 AM'},{end:'08 AM'},{end:'09 AM'},{end:'10 AM'},{end:'11 AM'},{end:'12 AM'},{end:'01 PM'},{end:'02 PM'},{end:'03 PM'},{end:'04 PM'},{end:'05 PM'},{end:'06 PM'},{end:'07 PM'},{end:'08 PM'},{end:'09 PM'},{end:'10 PM'},{end:'11 PM'},{end:'12 PM'}];
    $scope.inter=[{interval:'00:30 hrs'},{interval:'01:00 hrs'},{interval:'01:30 hrs'},{interval:'02:00 hrs'},{interval:'02:30 hrs'},{interval:'03:00 hrs'}];
    $scope.consecutives = [{number: '1'}, {number: '2'}, {number: '3'}, {number: '4'}, {number: '5'}, {number: '6'}, {number: '7'}, {number: '8'}, {number: '9'}];

    // Dovde

    //Odavde za addOn checkboxeve

    AddOptionService.getAddOns().then(function(response) {

        $scope.numbersOfAddOnBoxes=response.data.entity.data.length;
        for (var i = 0; i < response.data.entity.data.length; i++) {
            response.data.entity.data[i].active=false;
            $scope.container.items.push({label: response.data.entity.data[i].name, effectAllowed: 'move',star: false});
        }

    }).catch(function(error) {
        console.log(error);
    }); //Changing value of this variable you will change number of boxes apears in view.
   
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }

    $scope.makeCheckBoxAddOnChange=function(index) {
        if($scope.container.items[index].active==true) {
            $scope.container.items[index].active=false;
        }
        else {
            $scope.container.items[index].active=true;
        }
    }
    // Funkcija za menjanje zvezdice na active i ne active
    $scope.favoriteBox=function(index) {
        if($scope.container.items[index].star==true) {
            $scope.container.items[index].star=false;
        }
        else {
            $scope.container.items[index].star=true;
        }
    }
    // Dovde
    var changeToBool = function(value) {
        if(value=='Yes'){
            return true;
        }
        return false;
    }

    var convertScheduledBlocksForDatabse=function() {
        for (var i = 0; i < $scope.activeCheckBox.length; i++) {
            if($scope.activeCheckBox[i].active==true){
                $scope.option.scheduledBlocks.push($scope.activeCheckBox[i].id);
            }
        }
        $scope.option.birthdayFree=changeToBool($scope.option.isBirthdayPersonFree.free);
        if(angular.isDefined($scope.option.scheduledBlock)){
            $scope.option.scheduledBlock.days=$scope.convertDaysForDatabase($scope.scheduledBlock.days);
        }
        if(angular.isDefined($scope.option.scheduledBlock)){
            $scope.option.scheduledBlock.interval=$scope.convertHoursInMinutes($scope.scheduledBlock.interval);
        }
        $scope.option.convertedPartyTime=$scope.convertHoursInMinutes($scope.option.partyDuration);
    }

	$scope.addOption= function() {
        $scope.option.tier1=$scope.tier1;
        for (var i = 0; i < $scope.container.items.length; i++) {
            var num = $scope.container.items[i].id;
            $scope.option.addOns.push(num);
        }
        convertScheduledBlocksForDatabse();
        console.log($scope.option);
        console.log($scope.option.scheduledBlock);
        console.log($scope.container.items); 
        AddOptionService.addOption($scope.option).then(function(response) {
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
            console.log(response);
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
	};

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };
    $scope.askGuestCount = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.askGuestCount2 = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.askGuestCount3 = [{
        name: 'Yes'
    }, {
        name: 'No'
    }];

    $scope.partyDuration = [{
        time: '2 hr'
    }, {
        time: '1 hr 30 min'
    }]

    $scope.birthdayPersonFree = [{
        free: 'Yes'
    }, {
        free: 'No'
    }]

    $scope.twoGuestOfHonor = [{
        numberOfGuests: 'Yes'
    }, {
        numberOfGuests: 'No'
    }]

    $scope.firstParty = [{
        party: '8 AM'
    }, {
        party: '9 AM'
    }]

    $scope.lastParty = [{
        party: '8 PM'
    }, {
        party: '9 PM'
    }]

    $scope.interval = [{
        time: '2 hrs'
    }, {
        time: '3 hrs'
    }]

    $scope.consecutives = [{
        number: '1'
    }, {
        number: '2'
    }]

    $scope.pricingModel = [{
        price: 'Pricing Model 1'
    }, {
        price: 'Pricing Model 2'
    }]

    $scope.balanceDue = [{
        value: 'Add event'
    }, {
        value: '5 days after the event'
    }, {
        value: '10 days after the event'
    }]
    $scope.dragoverCallback = function(index, external, type, callback) {
        $scope.logListEvent('dragged over', index, external, type);
        // Invoke callback to origin for container types.
        if (type == 'container' && !external) {
            console.log('Container being dragged contains ' + callback() + ' items');
        }
        return index < 10; // Disallow dropping in the third row.
    };

    $scope.dropCallback = function(index, item, external, type) {
        $scope.logListEvent('dropped at', index, external, type);
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };

    $scope.logEvent = function(message) {
        console.log(message);
    };

    $scope.logListEvent = function(action, index, external, type) {
        var message = external ? 'External ' : '';
        message += type + ' element was ' + action + ' position ' + index;
        console.log(message);
    };

    // Initialize model
    $scope.model = [[], []];
    var id = 10;
    angular.forEach(['all', 'move', 'copy', 'link', 'copyLink', 'copyMove'], function(effect, i) {
        // $scope.container = {items: [], effectAllowed: 'move'};
        // for (var k = 0; k < 6; ++k) {
        //     $scope.container.items.push({label: 'move' + ' ' + id++, effectAllowed: 'move'});
        // }
        $scope.model[i % $scope.model.length].push($scope.container);
    });

    $scope.$watch('model', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);
}]);