var app = angular.module('booking');
app.service('AddOptionService', function($q,$http,$cookies) {
	return {
		addOption: function(option) {
            var defer = $q.defer();
            console.log(option.tier1.per_additional_guest);
            console.log(option.tier1.per_additional_guest.substring(2,option.tier1.per_additional_guest.length));
            if(angular.isUndefined(option.scheduledBlock)){
                option.scheduledBlock=[];
                option.scheduledBlock.start=[];
                option.scheduledBlock.end=[];
                option.scheduledBlock.consecutives=[];
            }
            $http({
                method: 'POST',
                data: {
                	'name':option.name,
                    'tier1':JSON.stringify(option.tier1),
                    'schedules':option.scheduledBlocks,
                    'pricing_model':option.pricingModel.price,
                    'guests_included':option.tier1.guests_included,
                    'min_guest':option.tier1.min_guest,
                    'max_guests':option.tier1.max_guest,
                    'label_singular':option.tier1.label_singular,
                    'label_plural':option.tier1.label_plural,
                    'ask_guest_count':option.tier1.ask_guest_count.name,
                    'deposit':parseInt(option.tier1Deposit.substring(2,option.tier1Deposit.length)),
                    'deposit_type':'FLAT',
                    'balance_due':option.balanceDue.value,
                    // 'is_additional_guests':
                    'per_additional_guest':parseInt(option.tier1.per_additional_guest.substring(2,option.tier1.per_additional_guest.length)),
                    'two_guest_of_honor':option.twoGuestsOfHonor.numberOfGuests,
                    'is_birthday_person_free':option.birthdayFree,
                    'deposit_percent':option.tier1PercDeposit,
                    'second_guest_of_honor_charge':parseInt(option.secondGuestCharge.substring(2,option.secondGuestCharge)),
                    'party_duration':option.convertedPartyTime,
                    'base_price':parseInt(option.basePrice.substring(2,option.basePrice.length)),
                    'days':JSON.stringify(option.days),
                    'from':option.scheduledBlock.from,
                    'to':option.scheduledBlock.to,
                    'start':option.scheduledBlock.start.start,
                    'end':option.scheduledBlock.end.end,
                    'interval':option.scheduledBlock.interval,
                    'consecutive':option.scheduledBlock.consecutives.number,
                    'addons':option.addOns

                },
                url: 'api/package/options'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getAddOns: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/addons/'+$cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getSchedules:function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/schedules'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});