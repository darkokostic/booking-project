var app = angular.module('booking');
app.controller('NewPackageCtrl', ['$scope','NewPackageService', '$sce','$location','$cookies','$state', function($scope,NewPackageService,$sce,$location,$cookies,$state) {
    $scope.package = [];
    $scope.selectedOptions = [];
    
    NewPackageService.getPackageOptions()
    .then(function(response) {
        $scope.packageOptions = response.entity.data;
    },function(reject) {
        console.log(reject);
    });

    $scope.addOption = function() {
        $location.path('/managment/packages/add_options');
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.makeCheckBoxChange = function(index) {
        if($scope.packageOptions[index].active) {
            $scope.packageOptions[index].active = false;
            for(var i = 0; i < $scope.selectedOptions.length; i++) {
                if($scope.selectedOptions[i] == $scope.packageOptions[index].id) {
                    $scope.selectedOptions.splice(i, 1);
                    console.log($scope.selectedOptions);
                }
            }
        } else {
            $scope.packageOptions[index].active = true;
            $scope.selectedOptions.push($scope.packageOptions[index].id);
        }
    };

    $scope.dropzoneConfig = {
        init            : function() {
            $scope.myDropzone = this;
            this.on('addedfile', function() {
                $scope.$apply(function() {
                    console.log('yo');
                    $scope.filesUploading = true;
                });
            });
            this.on('success', function (file, response) {
                $('body').pgNotification({
                    style: 'simple',
                    message: "Successfuly added Package",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'success'
                }).show();
                $state.go('side_menu.packages');
            });
            this.on('error', function(file, xhr){
                console.log('File failed to upload from dropzone 2.', file, xhr);
                $('body').pgNotification({
                    style: 'simple',
                    message: "An error occurs",
                    position: 'top-right',
                    timeout: 4000,
                    type: 'warning'
                }).show();
            });
            this.on('sending', function(file, xhr, formData){
                console.log('sending');
                console.log($scope.selectedOptions);
                formData.append('name', $scope.package.name);
                formData.append('description', $scope.package.description);
                formData.append('package_options', $scope.selectedOptions);
                formData.append('location_id', $cookies.get('location_id'));

            });
        },

        headers: {
           'Authorization': 'Bearer ' + $cookies.get('access_token'),
        },
        url             : 'api/packages',
        parallelUploads : 3,
        paramName: 'images',
        uploadMultiple  : false,
        autoProcessQueue: false,
        acceptedFiles: 'image/*',
        maxFileSize     : 30,
        addRemoveLinks  : 'dictCancelUpload'
    };

    $scope.addPackage = function() {
        if(!$scope.myDropzone.files || !$scope.myDropzone.files.length){        
            $('body').pgNotification({
            style: 'simple',
            message: "Please insert a photo",
            position: 'top-right',
            timeout: 4000,
            type: 'success'
        }).show();
        }else {
            $scope.myDropzone.processQueue();
        }
    };
}]);