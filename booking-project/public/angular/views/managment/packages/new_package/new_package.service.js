var app = angular.module('booking');
app.service('NewPackageService', function($q,$http, $cookies) {
	return {
		addPackage: function(package) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':package.name,
                    'description':package.description,
                    'package_options': package.packageOptions,
                    'location_id': $cookies.get('location_id')
                },
                url: 'api/packages'
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }, function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
        },

        getPackageOptions: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/package/options/' + $cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response;
            }, function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
        }
	}
});