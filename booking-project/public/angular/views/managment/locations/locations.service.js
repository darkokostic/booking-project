var app = angular.module('booking');
app.service('LocationsService', function($q,$http,$rootScope) {
    return {
    	getLocations: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/locations'
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        addLocation: function(newLocation) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                	'name':newLocation.name,
                	'location':newLocation.location
                },
                url: 'api/locations'
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        deleteLocation: function(locationId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/locations/'+locationId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        editLocation: function(location) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'name':location.name,
                    'location':location.location
                },
                url: 'api/locations/'+location.id
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    }
});