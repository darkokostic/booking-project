var app = angular.module('booking');
app.controller('LocationsController', ['$scope','LocationsService', function($scope, LocationsService) {
	$scope.locations=[];
	$scope.newLocation=[];
	$scope.locationForEdit=[];

	LocationsService.getLocations().then(function(response) {
		$scope.locations = response.data.entity.locations;
        console.log($scope.locations);
    }).catch(function(error) {
        console.error(error);
    });

    $scope.addLocation = function(){
	    LocationsService.addLocation($scope.newLocation).then(function(response) {
	        $scope.locations.splice(0,0,response.data.entity);
	        $scope.newLocation=[];
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
	    }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
	        console.error(error);
	    });
	};

	$scope.deleteLocation = function(locationId,index) {
		LocationsService.deleteLocation(locationId).then(function(response) {
			$scope.locations.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
		}).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
			console.log(error);
		});
	};

	$scope.setLocationForEdit = function(locationName,locationAddress,locationId,index) {
		$scope.locationForEdit.name=locationName;
		$scope.locationForEdit.location=locationAddress;
		$scope.locationForEdit.index=index;
		$scope.locationForEdit.id=locationId;	
	}

	$scope.editLocation = function(){
	    LocationsService.editLocation($scope.locationForEdit).then(function(response) {
	        $scope.locations.splice($scope.locationForEdit.index,1,response.data.entity);
	        $scope.locationForEdit=[];
	        $('#edit-location').modal('hide');
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
	    }).catch(function(error) {
	        $scope.locationForEdit=[];
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
	        console.error(error);
	    });
	};
}]);