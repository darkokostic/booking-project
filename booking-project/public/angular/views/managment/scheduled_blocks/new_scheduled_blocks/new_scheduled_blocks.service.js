var app = angular.module('booking');
app.service('NewScheduledBlocksService', function($q,$http,$cookies) {
	return {
		getOptions:function() {
			console.log($cookies.get('location_id'))
			var defer = $q.defer();
            $http({
                method: 'GET',
                data: {
                	// 'package_id':null
                },
                url: 'api/location/package/options/'+$cookies.get('location_id')
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
		},
		addScheduledBlock: function(scheduledBlock,days,interval) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                	'days':JSON.stringify(days),
		            'from':scheduledBlock.from,
		            'to':scheduledBlock.to,
		            'start':scheduledBlock.start.start,
		            'end':scheduledBlock.end.end,
		            'interval':interval,
		            'consecutive':scheduledBlock.consecutives.number,
		            'package_options':scheduledBlock.packagesId
                },
                url: 'api/schedules'
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        }
	}
});