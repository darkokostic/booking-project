var app = angular.module('booking');
app.service('EditScheduledBlocksService', function($q,$http,$rootScope) {
	return {
		getScheduledBlock: function(id) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/schedules/'+id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },
        editScheduledBlock: function(scheduledBlock,days,interval) {
		    console.log(scheduledBlock);
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'days':JSON.stringify(days),
                    'from':scheduledBlock.from,
                    'to':scheduledBlock.to,
                    'start':scheduledBlock.start.start,
                    'end':scheduledBlock.end.end,
                    'interval':interval,
                    'consecutive':scheduledBlock.consecutives.number,
                    'package_options':scheduledBlock.packagesId
                },
                url: 'api/schedules/'+scheduledBlock.id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }, function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        }
	}
});