var app = angular.module('booking');
app.controller('EditScheduledBlocksCtrl', ['$scope','EditScheduledBlocksService','$stateParams', '$sce','NewScheduledBlocksService','$window', function($scope,EditScheduledBlocksService,$stateParams, $sce, NewScheduledBlocksService, $window) {

    $scope.scheduledBlock=[];
    $scope.numbersOfBoxes=0; //Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBox=[];
    $scope.options=[];


    var formatForView=function() {
        $scope.scheduledBlock.to=moment($scope.scheduledBlock.to).format('MM/DD/YYYY');
        $scope.scheduledBlock.from=moment($scope.scheduledBlock.from).format('MM/DD/YYYY');
        $scope.scheduledBlock.days=$scope.convertDaysForView($scope.scheduledBlock.days);
        $scope.scheduledBlock.end={end:$scope.scheduledBlock.end};
        $scope.scheduledBlock.start={start:$scope.scheduledBlock.start};
        $scope.scheduledBlock.interval={interval:$scope.convertMinutesIntoHours($scope.scheduledBlock.interval)};
        $scope.scheduledBlock.consecutives={number:$scope.scheduledBlock.consecutive};
    }

    EditScheduledBlocksService.getScheduledBlock($stateParams.id).then(function(response) {
    	$scope.scheduledBlock=response.data.entity;
        formatForView();
        $scope.scheduledBlock.packagesId=[];
        console.log("Block:",$scope.scheduledBlock)
        NewScheduledBlocksService.getOptions().then(function(responseOptions) {
        console.log("Options:",responseOptions)
            $scope.numbersOfBoxes=responseOptions.data.entity.data.length;
            for (var i = 0; i < responseOptions.data.entity.data.length; i++) {
                responseOptions.data.entity.data[i].active=false;
                for (var j = 0; j < $scope.scheduledBlock.options.length; j++) {
                    if($scope.scheduledBlock.options[j].id==responseOptions.data.entity.data[i].id){
                        responseOptions.data.entity.data[i].active=true;
                    }
                }
                    $scope.activeCheckBox.push(responseOptions.data.entity.data[i]);
            }
        }).catch(function(error) {
            console.log(error);
        });
    }).catch(function(reject) {
    	console.log(reject);
    });

    $scope.getNumber = function(num) {
        return new Array(num);
    }
    $scope.makeCheckBoxChange=function(index) {
        if($scope.activeCheckBox[index].active==true) {
            $scope.activeCheckBox[index].active=false;
        }
        else {
            $scope.activeCheckBox[index].active=true;
        }
    }

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.from=[{start:'01 AM'},{start:'02 AM'},{start:'03 AM'},{start:'04 AM'},{start:'05 AM'},{start:'06 AM'},{start:'07 AM'},{start:'08 AM'},{start:'09 AM'},{start:'10 AM'},{start:'11 AM'},{start:'12 AM'},{start:'01 PM'},{start:'02 PM'},{start:'03 PM'},{start:'04 PM'},{start:'05 PM'},{start:'06 PM'},{start:'07 PM'},{start:'08 PM'},{start:'09 PM'},{start:'10 PM'},{start:'11 PM'},{start:'12 PM'}];
    $scope.to=[{end:'01 AM'},{end:'02 AM'},{end:'03 AM'},{end:'04 AM'},{end:'05 AM'},{end:'06 AM'},{end:'07 AM'},{end:'08 AM'},{end:'09 AM'},{end:'10 AM'},{end:'11 AM'},{end:'12 AM'},{end:'01 PM'},{end:'02 PM'},{end:'03 PM'},{end:'04 PM'},{end:'05 PM'},{end:'06 PM'},{end:'07 PM'},{end:'08 PM'},{end:'09 PM'},{end:'10 PM'},{end:'11 PM'},{end:'12 PM'}];
    $scope.inter=[{interval:'00:30 hrs'},{interval:'01:00 hrs'},{interval:'01:30 hrs'},{interval:'02:00 hrs'},{interval:'02:30 hrs'},{interval:'03:00 hrs'}];
    $scope.consecutives = [{number: '1'}, {number: '2'}, {number: '3'}, {number: '4'}, {number: '5'}, {number: '6'}, {number: '7'}, {number: '8'}, {number: '9'}];

    $scope.editScheduledBlock = function(){
        for (var i = 0; i < $scope.activeCheckBox.length; i++) {
            if($scope.activeCheckBox[i].active==true){
                $scope.scheduledBlock.packagesId.push($scope.activeCheckBox[i].id);
            }
        }
        var days=$scope.convertDaysForDatabase($scope.scheduledBlock.days);
        var interval=$scope.convertHoursInMinutes($scope.scheduledBlock.interval);
        EditScheduledBlocksService.editScheduledBlock($scope.scheduledBlock,days,interval).then(function(response) {
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
            console.log(response)
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    }

    $scope.checkForOptions=function() {
        for (var i = 0; i < $scope.activeCheckBox.length; i++) {
            if($scope.activeCheckBox[i].active==true){
                return false;
            }
        }
        return true;
    }

}]);