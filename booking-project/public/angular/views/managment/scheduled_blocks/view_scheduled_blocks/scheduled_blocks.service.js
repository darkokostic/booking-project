var app = angular.module('booking');
app.service('ScheduledBlocksService', function($q,$http,$rootScope) {
	return {
		getScheduledBlocks: function(url) {
            var url=url;
            if(url == null) {
                url='api/schedules';
            }
            var defer = $q.defer();
            $http({
                method: 'GET',
                data: {
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        deleteScheduledBlock: function(scheduledBlockId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/schedules/'+scheduledBlockId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        searchService: function(url,searchTerm) {
            if(url==null) {
                url='api/search/schedules';
            }
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'from': searchTerm.from,
                    'to': searchTerm.to
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	};
});