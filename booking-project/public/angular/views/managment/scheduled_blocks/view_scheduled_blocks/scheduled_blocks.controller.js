var app = angular.module('booking');
app.controller('ScheduledBlocksCtrl', ['$scope','ScheduledBlocksService', '$location', '$sce','$rootScope','$timeout', function($scope,ScheduledBlocksService, $location, $sce,$rootScope,$timeout) {
	
    $scope.scheduledBlocks=[];
    $scope.viewMoreActiveScheduledBlocks = true;
    $scope.baseURL=$scope.baseUrlPagination +'/api/schedules?page=';
    $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/schedules?page='; //default url za paginaciju sa searchom
    $scope.maxSize = 0;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 0;
    $scope.prevURLNumber='';
    $scope.searchTerm = [];
    $scope.searchTerm.from = '';
    $scope.searchTerm.to = '';

    var responseItems = ScheduledBlocksService.getScheduledBlocks(null);
    responseItems.then(function(response) {
            $scope.scheduledBlocks=response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        console.log(response);
    },function(reject) {
        console.log(reject);
    });

    //delete
    $scope.deleteScheduledBlock = function(scheduledBlockId,index) {
        console.log(scheduledBlockId)
        ScheduledBlocksService.deleteScheduledBlock(scheduledBlockId).then(function(response) {
            $scope.scheduledBlocks.data.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    };

    $scope.paginate=function() {
        $scope.baseURL=$scope.baseURL+$scope.bigCurrentPage; //postavlja se base url sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.baseURLSearch=$scope.baseURLSearch+$scope.bigCurrentPage; //postavlja se base url search-a sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.checkForPageURL($scope.bigCurrentPage,$scope.prevURLNumber);
        $scope.prevURLNumber=$scope.bigCurrentPage;
        if($scope.searchTerm.from!=''&&$scope.searchTerm.to!=''){ 
            $scope.search($scope.baseURLSearch,$scope.searchTerm); //Ukoliko postoji podaci za search paginacija se vrsi za search podacima
        }else {
            console.log("Dobnar prolaz")
            $scope.loadPaginatedItems($scope.baseURL);//Ukoliko ne postoje podaci za search, paginacija se vrsi bez podataka za search
        }
    }
    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije
    $scope.loadPaginatedItems=function(url) {
        $scope.baseURL=$scope.baseUrlPagination +'/api/schedules?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/schedules?page='; //default url za paginaciju sa searchom
        console.log($scope.baseURL);
        ScheduledBlocksService.getScheduledBlocks(url).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.scheduledBlocks=response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(reject) {
            console.log(reject);
        });

    };

    $scope.search = function (url) {
        $scope.baseURL=$scope.baseUrlPagination +'/api/schedules?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/schedules?page='; //default url za paginaciju sa searchom
        if($scope.searchTerm.from==''&&$scope.searchTerm.to==''){
            $scope.loadPaginatedItems($scope.baseURL);
            return true;
        }
        console.log($scope.searchTerm.from+''+$scope.searchTerm.to)
        if($scope.searchTerm.from!=''&&$scope.searchTerm.to!='') {
            console.log("Prolaz search")
            ScheduledBlocksService.searchService(url,$scope.searchTerm).then(function(response) {
                $scope.checkForAnimationAppearance();
                $scope.scheduledBlocks=response.data.entity;
                $scope.maxSize = response.data.entity.per_page;
                $scope.bigTotalItems = response.data.entity.total;
                $scope.bigCurrentPage = response.data.entity.current_page;
            }).catch(function(error) {
                console.log(error);
            });
        }
    };
    // End of functions for paginations

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };
}]);