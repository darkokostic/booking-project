var app = angular.module('booking');
app.service('PolicyService', function($q,$http,$rootScope, $cookies) {
	return {
		getPolices: function(url, id) {
		    var url = url;
		    if (url == null) {
		        url = 'api/location/policies/' + id;
            }
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }).catch( function errorCallback(response) {
                defer.reject(response);
            });
            return defer.promise;
         },

        deletePolicy: function(policyId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/policies/'+policyId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        searchService: function(url, querry) {
            if(url==null) {
                url='api/search/policies';
            }
            console.log(querry);
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'search': querry,
                    'location_id': $cookies.get('location_id')
                },
                url: url
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
        // ,loadMorePolicy: function(url){
        //     var defer = $q.defer();
        //     $http({
        //         method: 'GET',
        //         url: $rootScope.urlLocation
        //     }).then(function successCallback(response) {
        //             response.data.next_page_url=$rootScope.urlLocation;
        //             defer.resolve(response);
        //         return response;
        //     }, function errorCallback(response) {
        //             defer.reject(response);
        //     });
        //     return defer.promise;
        // }
	}
});