var app = angular.module('booking');
app.controller('PolicyCtrl', ['$scope','PolicyService', '$location', '$sce','$rootScope','$timeout','$cookies', function($scope,PolicyService, $location, $sce,$rootScope,$timeout,$cookies) {
	
    $scope.viewMoreActivePolicy = true;
    $scope.policies=[];
    $scope.baseURL=$scope.baseUrlPagination +'/api/location/policies/'+$cookies.get('location_id')+'?page=';
    $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/policies?page='; //default url za paginaciju sa searchom
    $scope.maxSize = 0;
    $scope.bigTotalItems = 0;
    $scope.bigCurrentPage = 0;
    $scope.prevURLNumber='';
    $scope.querry='';

    var responsePolicies = PolicyService.getPolices(null, $cookies.get('location_id'));
    responsePolicies.then(function(response) {
        $scope.policies = response.data.entity;
        $scope.maxSize = response.data.entity.per_page;
        $scope.bigTotalItems = response.data.entity.total;
        $scope.bigCurrentPage = response.data.entity.current_page;
        console.log($scope.policies);
    }).catch(function(reject) {
        console.log(reject);
    });

    //delete
    $scope.deletePolicy = function(policyId,index) {
        PolicyService.deletePolicy(policyId).then(function(response) {
            $scope.policies.data.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    };

    //funkcija koja se poziva kada se klikne na neko dugme iz paginacije
    $scope.paginate=function() {
        $scope.baseURL=$scope.baseURL+$scope.bigCurrentPage; //postavlja se base url sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.baseURLSearch=$scope.baseURLSearch+$scope.bigCurrentPage; //postavlja se base url search-a sa brojem koji se kliknuo na paginaciju, kako bi se otislo na odredjenu stranu
        $scope.checkForPageURL($scope.bigCurrentPage,$scope.prevURLNumber); //Provera na koju stranu sledeca animacija da ide
        $scope.prevURLNumber=$scope.bigCurrentPage;
        if($scope.querry==null||$scope.querry==''){ //Ukoliko ne postoje podaci za search, paginacija se vrsi bez podataka za search
            $scope.loadPaginatedItems($scope.baseURL);
        }else {
            $scope.search($scope.baseURLSearch,$scope.querry); //Ukoliko postoji podaci za search paginacija se vrsi za search podacima
        }
    };
    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije
    $scope.loadPaginatedItems=function(url) {
        $scope.baseURL=$scope.baseUrlPagination +'/api/location/policies/'+$cookies.get('location_id')+'?page=';
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/policies?page=';
        console.log($scope.baseURL);
        PolicyService.getPolices(url).then(function(response) {
            $scope.checkForAnimationAppearance();
            $scope.policies = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(reject) {
            console.log(reject);
        });

    };
    // END Functions for paginations

    //Funkcija koja prikuplja podatke za sledecu stranicu koja odgovara broju paginacije i search podacima
    $scope.search = function (url,querry) {
        $scope.baseURLSearch=$scope.baseUrlPagination +'/api/search/policies?page=';
        $scope.baseURL=$scope.baseUrlPagination +'/api/location/policies/'+$cookies.get('location_id')+'?page=';
        PolicyService.searchService(url, querry).then(function(response) {
            console.log(response);
            $scope.checkForAnimationAppearance();
            $scope.policies = response.data.entity;
            $scope.maxSize = response.data.entity.per_page;
            $scope.bigTotalItems = response.data.entity.total;
            $scope.bigCurrentPage = response.data.entity.current_page;
        }).catch(function(error) {
            console.log(error);
        });
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    // $scope.loadMorePolicy=function(url) {
    //
    //     $scope.disableSlideLeft=true;
    //     $scope.disableSlideRight=false;
    //     $scope.disableSlideRightBack=false;
    //     $scope.disableSlideLeftBack=true;
    //
    //     $scope.viewMoreActivePolicy = false;
    //     console.log(url)
    //
    //     $timeout(function() {
    //         $scope.policies=[];
    //         $scope.disableSlideLeft=false;
    //         $scope.disableSlideRight=true;
    //         $scope.disableSlideRightBack=false;
    //         $scope.disableSlideLeftBack=false;
    //         var responsePolicies = PolicyService.loadMorePolicy(url);
    //         responsePolicies.then(function(response) {
    //         $scope.nextPageUrl=response.data.next_page_url;
    //         for(var i=12;i<23;i++){
    //             $scope.policies.push(response.data[i]);
    //         }
    //         console.log(response);
    //         },function(reject) {
    //             console.log(reject);
    //         });
    //     }, 500);
    // }
}]);