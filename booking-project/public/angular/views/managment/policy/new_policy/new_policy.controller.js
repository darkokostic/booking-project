var app = angular.module('booking');
app.controller('NewPolicyCtrl', ['$scope','NewPolicyService', '$sce', '$state', '$window', function($scope,NewPolicyService, $sce, $state, $window) {
    $scope.hasOptions = true;
    $scope.newPolicy = [];
    $scope.packages = [];
    $scope.numOfQuestions = 1;
    $scope.questions=[];
    $scope.packageOptions = [];
    $scope.checkedPackageOptions = [];
    $scope.itemsPerPage = 0;
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.selectedPackageId = 0;

    $scope.addPolicy = function() {
        $scope.newPolicy.packageOptions = $scope.checkedPackageOptions;
        $scope.newPolicy.questions = $scope.questions;
        if ($scope.newPolicy.isGlobal == null) {
            $scope.newPolicy.isGlobal = true;
        } else {
            $scope.newPolicy.isGlobal = false;
        }
        NewPolicyService.addPolicy($scope.newPolicy).then(function(response) {
            $('body').pgNotification({
                style: 'simple',
                message: response.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
            console.log(response);
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            $window.history.back();
            console.error(error);
        });
    };

    NewPolicyService.getPackages().then(function(response) {
        $scope.packages = response.data.entity.data;
    }).catch(function(error) {
        console.error(error);
    });

    $scope.addMoreQuestions=function() {
        $scope.numOfQuestions++;
    };

    $scope.getNumber = function(num) {
        return new Array(num);
    };

    $scope.onSelected = function (selectedItem) {
        $scope.selectedPackageId = selectedItem.id;
        NewPolicyService.getOptions(selectedItem.id)
        .then(function(response) {
            if(response.entity != null) {
                $scope.hasOptions = true;
                $scope.packageOptions = response.entity.data;
                $scope.itemsPerPage = response.entity.per_page;
                $scope.totalItems = response.entity.total;
                $scope.currentPage = response.entity.current_page;
            } else {
                $scope.packageOptions = response.entity;
                $scope.hasOptions = false;
            }
        }).catch(function(error) {
            console.error(error);
        })
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.makeCheckBoxChange = function(index) {
        if($scope.packageOptions[index].active == true) {
            $scope.packageOptions[index].active = false;
            for(var i = 0; i < $scope.checkedPackageOptions.length; i++) {
                if($scope.checkedPackageOptions[i] == $scope.packageOptions[index].id) {
                    $scope.checkedPackageOptions.splice(i, 1);
                    console.log($scope.checkedPackageOptions);
                }
            }

        } else {
            $scope.packageOptions[index].active=true;
            $scope.checkedPackageOptions.push($scope.packageOptions[index].id);
            console.log($scope.checkedPackageOptions);
        }
    };

    $scope.paginate = function(currentPage) {
        NewPolicyService.getOptionsPaginate($scope.selectedPackageId, currentPage)
        .then(function(response) {
            if(response.entity != null) {
                $scope.hasOptions = true;
                $scope.packageOptions = response.entity.data;
                for(var i = 0; i < $scope.packageOptions.length; i++) {
                    for(var j = 0; j < $scope.checkedPackageOptions.length; j++) {
                        if($scope.packageOptions[i].id == $scope.checkedPackageOptions[j]) {
                            $scope.packageOptions[i].active = true;
                        }
                    }
                }
                $scope.currentPage = response.entity.current_page;
            } else {
                $scope.packageOptions = response.entity;
                $scope.hasOptions = false;
            }
        }).catch(function(error) {
            console.error(error);
        })
    };
}]);