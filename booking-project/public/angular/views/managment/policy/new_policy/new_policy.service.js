var app = angular.module('booking');
app.service('NewPolicyService', function($q, $http,$cookies) {
	return {
        addPolicy: function(newPolicy) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':newPolicy.name,
                    'description':newPolicy.description,
					'questions': newPolicy.questions,
					'is_global': newPolicy.isGlobal,
                    'package_options': newPolicy.packageOptions,
					'location_id': $cookies.get('location_id')
                },
                url: 'api/policies'
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getPackages: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/packages/'+$cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getOptions: function(id) {
            var defer = $q.defer();
            var params = {
                package_id: id
            };
            $http({
                method: 'GET',
                url: 'api/location/package/options/' + $cookies.get('location_id'), params
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }).catch(function errorCallback(response) {
                defer.reject(response.data);
                return response.data;
            });
            return defer.promise;
        },
        getOptionsPaginate: function(packageId, pageNumber) {
            var defer = $q.defer();
            var params = {
                package_id: packageId
            };
            $http({
                method: 'GET',
                url: 'api/location/package/options/'+$cookies.get('location_id')+'?page='+pageNumber, params
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }).catch(function errorCallback(response) {
                defer.reject(response.data);
                return response.data;
            });
            return defer.promise;
        }
	}
});