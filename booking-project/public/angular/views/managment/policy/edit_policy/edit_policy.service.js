var app = angular.module('booking');
app.service('EditPolicyService', function($q, $http, $cookies) {
	return {
		getPolicy: function(id) {
			console.log()
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/policies/'+id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }).catch (function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },

        getPackages: function(id) {
		    var locationId = $cookies.get('location_id');
		    if(locationId) {
                locationId = id;
            }
            console.log("Location ID: ", locationId);
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/packages/' + locationId
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getOptions: function(id) {
            var defer = $q.defer();
            var params = {
                package_id: id
            };
            $http({
                method: 'GET',
                url: 'api/location/package/options/' + $cookies.get('location_id'), params
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }).catch(function errorCallback(response) {
                defer.reject(response.data);
                return response.data;
            });
            return defer.promise;
        },

        getOptionsPaginate: function(locationId, packageId, pageNumber) {
            var defer = $q.defer();
            var params = {
                package_id: packageId
            };
            $http({
                method: 'GET',
                url: 'api/location/package/options/'+locationId+'?page='+pageNumber, params
            }).then(function successCallback(response) {
                defer.resolve(response.data);
                return response.data;
            }).catch(function errorCallback(response) {
                defer.reject(response.data);
                return response.data;
            });
            return defer.promise;
        },

        editPolicy: function(policy) {
		    console.log(policy);
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'name':policy.name,
                    'description':policy.description,
                    'questions': policy.questions,
                    'is_global': policy.is_global,
                    'package_options': policy.packageOptions,
                    'location_id': $cookies.get('location_id')
                },
                url: 'api/policies/'+policy.id
            }).then(function successCallback(response) {
                console.log(response);
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
	}
});