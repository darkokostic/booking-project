var app = angular.module('booking');
app.controller('EditPolicyCtrl', ['$scope','EditPolicyService','$stateParams', '$sce', '$window', function($scope,EditPolicyService,$stateParams,$sce, $window) {

    $scope.policy = [];
    $scope.questions = [];
    $scope.packages = [];
    $scope.newQuestions = [];
    $scope.numOfQuestions = 0;
    $scope.numOfNewQuestions = 0;
    $scope.packageLocationId = 0;
    $scope.packageOptions = [];
    $scope.checkedPackageOptions = [];
    $scope.itemsPerPage = 0;
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.selectedPackageId = 0;
    
    EditPolicyService.getPolicy($stateParams.id).then(function(response) {
    	$scope.policy = response.data.entity;
    	$scope.questions = response.data.entity.questions;
    	$scope.numOfQuestions = $scope.questions.length;
    	if(response.data.entity.options) {
    	    $scope.selectedPackageId = response.data.entity.options[0].package_id;
    	    $scope.packageLocationId = response.data.entity.options[0].location_id;
    	    for(var i = 0; i < response.data.entity.options.length; i++) {
                $scope.checkedPackageOptions.push(response.data.entity.options[i].id);
            }

            EditPolicyService.getPackages($scope.packageLocationId)
            .then(function(response) {
                $scope.packages = response.data.entity.data;
                for(var i = 0; i < response.data.entity.data.length; i++) {
                    if($scope.packages[i].id == $scope.selectedPackageId) {
                        $scope.selectedPackage = response.data.entity.data[i];
                    }
                }
                EditPolicyService.getOptions($scope.selectedPackageId)
                    .then(function(response) {
                        if(response.entity != null) {
                            $scope.hasOptions = true;
                            $scope.packageOptions = response.entity.data;
                            $scope.itemsPerPage = response.entity.per_page;
                            $scope.totalItems = response.entity.total;
                            $scope.currentPage = response.entity.current_page;
                            for(var i = 0; i < $scope.packageOptions.length; i++) {
                                for(var j = 0; j < $scope.checkedPackageOptions.length; j++) {
                                    if($scope.packageOptions[i].id == $scope.checkedPackageOptions[j]) {
                                        $scope.packageOptions[i].active = true;
                                    }
                                }
                            }
                            console.log("Package Options ======", $scope.packageOptions);
                        } else {
                            $scope.packageOptions = response.entity;
                            $scope.hasOptions = false;
                        }
                    }).catch(function(error) {
                    console.error(error);
                });
            }).catch(function(error) {
                console.error(error);
            });
        }
    	console.log(response);
    }).catch(function(reject) {
    	console.log(reject);
    });

    $scope.edit = function() {
        $scope.policy.packageOptions = $scope.checkedPackageOptions;
        $scope.policy.questions = $scope.newQuestions;
        console.log($scope.policy);
        EditPolicyService.editPolicy($scope.policy)
        .then(function(response) {
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
        }).catch(function(error) {
            console.error(error);
        });
    };

    $scope.onSelected = function (selectedItem) {
        $scope.selectedPackageId = selectedItem.id;
        EditPolicyService.getOptions(selectedItem.id)
            .then(function(response) {
                if(response.entity != null) {
                    $scope.hasOptions = true;
                    $scope.packageOptions = response.entity.data;
                    $scope.itemsPerPage = response.entity.per_page;
                    $scope.totalItems = response.entity.total;
                    $scope.currentPage = response.entity.current_page;
                } else {
                    $scope.packageOptions = response.entity;
                    $scope.hasOptions = false;
                }
            }).catch(function(error) {
            console.error(error);
        })
    };

    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.getNumber = function(num) {
        return new Array(num);
    };

    $scope.addMoreQuestions=function() {
        $scope.numOfNewQuestions++;
    };

    //Odavde za already exist checkbox
    $scope.numbersOfBoxes=5; //Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBox=[];
    for (var i = 0; i < $scope.numbersOfBoxes; i++) {
        var box={title:'title'+i, desc:'desc'+i,active:false};
        $scope.activeCheckBox.push(box);
    }

    $scope.makeCheckBoxChange=function(index) {
        if($scope.packageOptions[index].active == true) {
            $scope.packageOptions[index].active = false;
            for(var i = 0; i < $scope.checkedPackageOptions.length; i++) {
                if($scope.checkedPackageOptions[i] == $scope.packageOptions[index].id) {
                    $scope.checkedPackageOptions.splice(i, 1);
                    console.log($scope.checkedPackageOptions);
                }
            }

        } else {
            $scope.packageOptions[index].active=true;
            $scope.checkedPackageOptions.push($scope.packageOptions[index].id);
            console.log($scope.checkedPackageOptions);
        }
    };

    $scope.paginate = function(currentPage) {
        EditPolicyService.getOptionsPaginate($scope.packageLocationId, $scope.selectedPackageId, currentPage)
            .then(function(response) {
                console.log("Current Page ===== ", currentPage);
                console.log("Response ==== ", response);
                if(response.entity != null) {
                    $scope.hasOptions = true;
                    $scope.packageOptions = response.entity.data;
                    for(var i = 0; i < $scope.packageOptions.length; i++) {
                        for(var j = 0; j < $scope.checkedPackageOptions.length; j++) {
                            if($scope.packageOptions[i].id == $scope.checkedPackageOptions[j]) {
                                $scope.packageOptions[i].active = true;
                            }
                        }
                    }
                    $scope.currentPage = response.entity.current_page;
                } else {
                    $scope.packageOptions = response.entity;
                    $scope.hasOptions = false;
                }
            }).catch(function(error) {
            console.error(error);
        })
    };
}]);