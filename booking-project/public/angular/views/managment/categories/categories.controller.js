var app = angular.module('booking');
app.controller('CategoriesController', ['$scope','CategoriesService', function($scope, CategoriesService) {

	$scope.categories=[];
	$scope.newCategorie=[];
	$scope.categoryForEdit=[];

	CategoriesService.getCategories().then(function(response) {
		$scope.categories = response.data.entity;
        console.log($scope.categories);
    }).catch(function(error) {
        console.error(error);
    });

    $scope.addCategorie = function(){
	    CategoriesService.addCategorie($scope.newCategorie).then(function(response) {
	        console.log($scope.categories);
	        $scope.newCategorie=[];
	        $scope.categories.splice(0,0,response.data.entity);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
	    }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
	        console.error(error);
	    });
	};

	$scope.deleteCategorie = function(categorieId,index) {
		console.log(categorieId);
		CategoriesService.deleteCategorie(categorieId).then(function(response) {
			$scope.categories.splice(index,1);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
		}).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
			console.log(error);
		});
	};

	$scope.setCategoryForEdit = function(categoryName,categoryId,index) {
		$scope.categoryForEdit.name=categoryName;
		$scope.categoryForEdit.index=index;
		$scope.categoryForEdit.id=categoryId;	
	}

	$scope.editCategory = function(){
	    CategoriesService.editCategory($scope.categoryForEdit).then(function(response) {
	        $scope.categories.splice($scope.categoryForEdit.index,1,response.data.entity);
	        $scope.categoryForEdit=[];
	        $('#edit-category').modal('hide');
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
	    }).catch(function(error) {
	        $scope.categoryForEdit=[];
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
	        console.error(error);
	    });
	};
}]);