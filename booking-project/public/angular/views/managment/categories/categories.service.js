var app = angular.module('booking');
app.service('CategoriesService', function($q,$http,$rootScope) {
    return {
    	getCategories: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/addon-categories'
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        addCategorie: function(newCategorie) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':newCategorie.categorie
                },
                url: 'api/addon-categories'
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        deleteCategorie: function(categorieId) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: 'api/addon-categories/'+categorieId
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        editCategory: function(category) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'name':category.name
                },
                url: 'api/addon-categories/'+category.id
            }).then(function successCallback(response) {
                console.log(response)
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    }
});