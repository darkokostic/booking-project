var app = angular.module('booking');
app.service('EditEventsService', function($q,$http,$rootScope) {
	return {
		getEvent: function(id) {
            console.log(id)
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/events/'+id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        },

        editEvent: function(event) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                data: {
                    'name':event.name,
                    'description':event.description
                },
                url: 'api/events/'+event.id
            }).then(function successCallback(response) {
                    defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                    defer.reject(response);
            });
            return defer.promise;
        }
	}
});