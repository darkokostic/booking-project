var app = angular.module('booking');
app.controller('EditEventsCtrl', ['$scope','EditEventsService', '$sce','$stateParams','$window', function($scope,EditEventsService, $sce,$stateParams,$window) {

    $scope.numOfQuestions = 1;
    $scope.questions=[];
    $scope.questions[0]=[];
    $scope.questions[0].numOfOptionsDropDown=3;
    $scope.questions[0].numOfOptions=3;
    $scope.isFirstDropDown=true;
    $scope.event=[];
    //Odavde za already exist checkbox
    $scope.numbersOfOptions=0;//Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBoxOption=[];
    var responseEvent = EditEventsService.getEvent($stateParams.id);
    responseEvent.then(function(response) {
        $scope.event=response.data.entity;
        $scope.questions[0]=[];
        $scope.questions[0].numOfOptionsDropDown=3;
        $scope.questions[0].numOfOptions=3;
        $scope.questions=response.data.entity.questions;
        $scope.numbersOfOptions=1;
        var box={title:response.data.entity.package_option.packages.name,active:true};
        $scope.activeCheckBoxOption.push(box);
        console.log($scope.event);
        console.log($scope.activeCheckBoxOption);


    // Initialize model
    $scope.model = [[], []];
    var id = 10;
    angular.forEach(['all', 'move', 'copy', 'link', 'copyLink', 'copyMove'], function(effect, i) {
        $scope.container = {items: [], effectAllowed: 'can\'t'};
        for (var k = 0; k < 5; ++k) {
            if($scope.event.steps[k]==1){
                $scope.container.items.push({label: 'Date & Time' + ' ' + 1,id:1, effectAllowed: 'move'});
            }
            if($scope.event.steps[k]==2){
                $scope.container.items.push({label: 'Party Details' + ' ' + 2,id:2, effectAllowed: 'move'});
            }
            if($scope.event.steps[k]==3){
                $scope.container.items.push({label: 'Add-Ons' + ' ' + 3,id:3, effectAllowed: 'move'});
            }
            if($scope.event.steps[k]==4){
                $scope.container.items.push({label: 'Host & GoH' + ' ' + 4,id:4, effectAllowed: 'move'});
            }
            if($scope.event.steps[k]==5){
                $scope.container.items.push({label: 'Policies' + ' ' + 5,id:5, effectAllowed: 'move'});
            }

        }
    });
    },function(reject) {
        console.log(reject);
    });
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckBoxOptionChange=function(index) {
        if($scope.activeCheckBoxOption[index].active==true) {
            $scope.activeCheckBoxOption[index].active=false;
        }
        else {
            $scope.activeCheckBoxOption[index].active=true;
        }
    }

    $scope.saveEvent = function() {
        console.log($scope.questions);
    }

    $scope.addMoreOptions=function(index) {
        $scope.questions[index].numOfOptions++;
        console.log($scope.questions)
    }

    $scope.addMoreOptionsDropDown=function(index) {
        $scope.questions[index].numOfOptionsDropDown++;
    }

    $scope.addMoreQuestions=function() {
        $scope.numOfQuestions++;
        $scope.questions[$scope.numOfQuestions-1]=[];
        $scope.questions[$scope.numOfQuestions-1].numOfOptionsDropDown=3;
        $scope.questions[$scope.numOfQuestions-1].numOfOptions=3;
    }

    $scope.resetDropDown = function(index) {
        console.log(index)
        if($scope.isFirstDropDown==true) {
            $scope.isFirstDropDown=false;
        }else {
            $scope.questions[index].options=[];
            $scope.questions[index].numOfOptionsDropDown=3;
            $scope.questions[index].numOfOptions=3;
        }
    }
    //Odavde kopiraj
    $scope.dragoverCallback = function(index, external, type, callback) {
        $scope.logListEvent('dragged over', index, external, type);
        // Invoke callback to origin for container types.
        if (type == 'container' && !external) {
            console.log('Container being dragged contains ' + callback() + ' items');
        }
        return index < 10; // Disallow dropping in the third row.
    };

    $scope.dropCallback = function(index, item, external, type) {
        $scope.logListEvent('dropped at', index, external, type);
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };

    $scope.logEvent = function(message) {
        console.log(message);
    };

    $scope.logListEvent = function(action, index, external, type) {
        var message = external ? 'External ' : '';
        message += type + ' element was ' + action + ' position ' + index;
        console.log(message);
    };


    $scope.$watch('model', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

    //Dovde
    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.questionOptions = [{
        name: 'Dropdown',
        description: 'User can choose one answer from list of answers'
    }, {
        name: 'Checkbox',
        description: 'User can choose single or multiple answers from list of answers'
    }, {
        name: 'Text',
        description: 'User can type his answer'
    }];

    $scope.editEvent = function() {
        EditEventsService.editEvent($scope.event).then(function(response) {
            console.log(response);
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    }
    
}])
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});