var app = angular.module('booking');
app.service('NewEventsService', function($q,$http,$cookies) {
    return {
        getPackages: function() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/packages/' + $cookies.get('location_id')
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        getOptions: function(id) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: 'api/location/package/options/' + id
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        },

        addEvent: function(event,option,questions) {
            console.log(questions)
        	var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                	'name':event.name,
                	'description':event.description,
                	'package_option_id':option.id,
                	'location_id':$cookies.get('location_id'),
                	'steps':event.steps,
                    'questions':questions
                },
                url: 'api/events'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }

    }
});