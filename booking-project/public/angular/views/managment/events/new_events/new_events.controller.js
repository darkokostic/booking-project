var app = angular.module('booking');
app.controller('NewEventsCtrl', ['$scope','NewEventsService', '$sce', '$window', function($scope,NewEventsService, $sce, $window) {

    $scope.hasOptions = true;
    $scope.numOfQuestions = 1;
    $scope.event=[];
    $scope.event.steps=[];
    $scope.questions=[];
    $scope.questions.options=[];
    $scope.questions[0]=[];
    $scope.questions[0].numOfOptionsDropDown=3;
    $scope.questions[0].numOfOptions=3;
    $scope.isFirstDropDown=true;
    $scope.isOneOptionSelected=false;
    $scope.packages=[];
    $scope.numbersOfOptions=0;//Changing value of this variable you will change number of boxes apears in view.
    $scope.activeCheckBoxOption=[];
    $scope.optionToSave=[];
    //Variables for checking status of fields, if true further fields will be shown
    $scope.packageSelected=false;
    $scope.optionsSelected=false;
    $scope.eventNameDirty=false;
    $scope.eventDescDirty=false;
    //Functions for setting variables to false and showing further fields  
    $scope.setPackageSelectedToTrue=function() {
        $scope.packageSelected=true;
    }
    $scope.setEventNameDirtyToTrue=function() {
        $scope.eventNameDirty=true;
    }
    $scope.setEventDescDirtyToTrue=function() {
        $scope.eventDescDirty=true;
    }
    // Finished setter function

    NewEventsService.getPackages().then(function(response) {
            $scope.packages=response.data.entity;
            $scope.numbersOfOptions=response.data.entity.length;
            console.log(response);
        }).catch(function(reject) {
            console.log(reject);
    });

    //Odavde za already exist checkbox
    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
    $scope.makeCheckBoxChange=function(index) {
        $scope.optionsSelected=true;
        if($scope.activeCheckBoxOption[index].active==true) {
            $scope.activeCheckBoxOption[index].active=false;
        }
        else {
            $scope.activeCheckBoxOption[index].active=true;
            $scope.optionToSave=$scope.activeCheckBoxOption[index];
            for (var i = 0; i < $scope.activeCheckBoxOption.length; i++) {
                $scope.activeCheckBoxOption[i].active=false;
                if($scope.activeCheckBoxOption[index].id==$scope.activeCheckBoxOption[i].id){
                    $scope.activeCheckBoxOption[index].active=true;
                }
            }
        }
    }
    // $scope.makeCheckBoxOptionChange=function(index) {
    //     $scope.optionsSelected=true;
    //     if($scope.activeCheckBoxOption[index].active==true) {
    //         $scope.activeCheckBoxOption[index].active=false;
    //         $scope.isOneOptionSelected=false;
    //     }
    //     else {
    //         if($scope.isOneOptionSelected==false){
    //             $scope.activeCheckBoxOption[index].active=true;
    //             $scope.optionToSave=$scope.activeCheckBoxOption[index];
    //             $scope.isOneOptionSelected=true;
    //         }
    //     }
    // }
    // Dovde

    $scope.saveEvent = function() {
        console.log($scope.event);
    }

    $scope.addMoreOptions=function(index) {
        $scope.questions[index].numOfOptions++;
        console.log($scope.questions)
    }

    $scope.addMoreOptionsDropDown=function(index) {
        $scope.questions[index].numOfOptionsDropDown++;
    }

    $scope.addMoreQuestions=function() {
        $scope.numOfQuestions++;
        $scope.questions[$scope.numOfQuestions-1]=[];
        $scope.questions[$scope.numOfQuestions-1].numOfOptionsDropDown=3;
        $scope.questions[$scope.numOfQuestions-1].numOfOptions=3;
    }

    $scope.resetDropDown = function(index) {
        console.log(index)
        if($scope.isFirstDropDown==true) {
            $scope.isFirstDropDown=false;
        }else {
            $scope.questions[index].options=[];
            $scope.questions[index].numOfOptionsDropDown=3;
            $scope.questions[index].numOfOptions=3;
        }
    }
    //Odavde kopiraj 
    $scope.dragoverCallback = function(index, external, type, callback) {
        $scope.logListEvent('dragged over', index, external, type);
        // Invoke callback to origin for container types.
        if (type == 'container' && !external) {
            console.log('Container being dragged contains ' + callback() + ' items');
        }
        return index < 10; // Disallow dropping in the third row.
    };

    $scope.dropCallback = function(index, item, external, type) {
        $scope.logListEvent('dropped at', index, external, type);
        // Return false here to cancel drop. Return true if you insert the item yourself.
        return item;
    };

    $scope.logEvent = function(message) {
        console.log(message);
    };

    $scope.logListEvent = function(action, index, external, type) {
        var message = external ? 'External ' : '';
        message += type + ' element was ' + action + ' position ' + index;
        console.log(message);
    };

    // Initialize model 
    $scope.model = [[], []];
    var id = 10;
    angular.forEach(['all', 'move', 'copy', 'link', 'copyLink', 'copyMove'], function(effect, i) {
      $scope.container = {items: [], effectAllowed: 'move'};
            $scope.container.items.push({label: 'Date & Time' + ' ' + 1,id:1, effectAllowed: 'move'});
            $scope.container.items.push({label: 'Party Details' + ' ' + 2,id:2, effectAllowed: 'move'});
            $scope.container.items.push({label: 'Add-Ons' + ' ' + 3,id:3, effectAllowed: 'move'});
            $scope.container.items.push({label: 'Host & GoH' + ' ' + 4,id:4, effectAllowed: 'move'});
            $scope.container.items.push({label: 'Policies' + ' ' + 5,id:5, effectAllowed: 'move'});
      
    });

    $scope.$watch('model', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

    //Dovde
    $scope.trustAsHtml = function(value) {
        return $sce.trustAsHtml(value);
    };

    $scope.questionOptions = [{
        name: 'Dropdown',
        description: 'User can choose one answer from list of answers'
    }, {
        name: 'Checkbox',
        description: 'User can choose single or multiple answers from list of answers'
    }, {
        name: 'Text',
        description: 'User can type his answer'
    }];

    var changeFormatOfQuestionForDatabase = function(questions) {
        console.log(questions)
        var questionToReturn=[];
        for(var i=0;i<questions.length;i++){
            if(angular.isUndefined(questions[i].typeOfQuestion)) {
                questions[i].typeOfQuestion=[];
                questions[i].typeOfQuestion.name="Text";
            }
            if(!angular.isUndefined(questions[i].question)){
                var question={type:questions[i].typeOfQuestion.name,options:questions[i].options,description:questions[i].question};
                questionToReturn.push(question);
            }
        }
        console.log(questionToReturn);
        return questionToReturn;
    }

    $scope.addEvent = function() {
        console.log($scope.container.items);
        for (var i = 0; i < $scope.container.items.length; i++) {
            var num = $scope.container.items[i].id;
            $scope.event.steps.push(num);
        }
        var questionsForDTBS=changeFormatOfQuestionForDatabase($scope.questions);
        NewEventsService.addEvent($scope.event,$scope.optionToSave,questionsForDTBS).then(function(response) {
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
            $window.history.back();
            console.log(response);
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.log(error);
        });
    };

    $scope.onSelected = function (selectedItem) {
        NewEventsService.getOptions(selectedItem.id)
            .then(function(response) {
                console.log(response);
                if(response.data.entity != null) {
                    $scope.hasOptions = true;
                    $scope.packageOptions = response.data.entity.data;
                    $scope.numbersOfOptions=response.data.entity.data.length;
                    console.log(response.data.entity.data.length);
                    for (var i = 0; i < response.data.entity.data.length; i++) {
                        response.data.entity.data[i].active=false;
                        $scope.activeCheckBoxOption.push(response.data.entity.data[i]);
                        console.log($scope.activeCheckBoxOption[i]);
                    }
                } else {
                    $scope.isOneOptionSelected=false;
                    $scope.packageOptions = [];
                    $scope.numbersOfOptions=0;
                    $scope.activeCheckBoxOption=[];
                    $scope.hasOptions = false;
                }
                console.log($scope.packageOptions);
            }).catch(function(error) {
            console.error(error);
        });
    };
}]);