var app = angular.module('booking');
app.service('RegisterService', function($http,$q) {
	return {
		registerUser: function(user) {
			var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'firstname' : user.firstname,
                    'lastname' : user.lastname,
                    'username' :  user.username,
                    'password' : user.password,
                    'email' : user.mail
                },
                url: '/api/register'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
		},

		checkForUsername: function(username,email) {
			var defer = $q.defer();
			$http({
				method: 'POST',
				data:{
					'username':username,
					'email':email
				},
				url:'/api/check-username'
			}).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
		}

	}
});