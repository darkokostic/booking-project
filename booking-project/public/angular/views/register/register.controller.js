var app = angular.module('booking');
app.controller('RegisterController', ['$scope','RegisterService','LoginService','$state','$cookies', function($scope,RegisterService,LoginService,$state,$cookies) {
	
	$scope.user=[];
	$scope.usernameExist=false;
	$scope.emailExist=false;

	$scope.registerUser = function() {
		console.log("Registration")
		RegisterService.registerUser($scope.user).then(function(response) {
			console.log(response.data);
			LoginService.login($scope.user).then(function(response) {
				console.log(response.data);
				//Token for accessing data
	            $cookies.put('access_token', response.data.access_token);
	            // var today = new Date();
	            // var expiresValue = new Date(today);
	            // //Set 'expires' option in 4 hours
	            // expiresValue.setMinutes(today.getMinutes() + 240);  
	            // $cookies.putObject('userCookie', userCookie,{expires:expiresValue});
	            //Use refresh token to get new one
	            $cookies.put('refresh_token', response.data.refresh_token);
	            $state.go('side_menu.dashboard');
			}).catch(function(error) {
	            // $cookies.remove('token');
	            console.error(error);
			});
		}).catch(function(error) {
            console.error(error);
		});
		return false;
	}

	$scope.checkForExistingUser=function() {
		RegisterService.checkForUsername($scope.user.username,$scope.user.mail).then(function(response) {
			console.log(response);
			$scope.usernameExist=response.data.entity.username_exist;
			$scope.emailExist=response.data.entity.email_exist;
		}).catch(function(error) {
			console.log(error);
			$scope.usernameExist=error.data.entity.username_exist;
			$scope.emailExist=error.data.entity.email_exist;
		});
	}

}]);