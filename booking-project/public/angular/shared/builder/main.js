/* ============================================================
 * File: main.js
 * Main Controller to set global scope variables. 
 * ============================================================ */


var app = angular.module('booking')
app.controller('AppCtrl', ['$scope', '$rootScope', '$state','ThemeService', function($scope, $rootScope, $state,ThemeService) {
    $scope.baseUrlPagination = "http://homestead.dev";
    // App globals
    $scope.app = {
        name: 'Pages',
        description: 'Admin Dashboard UI kit',
        layout: {
            menuPin: false,
            menuBehind: false,
            theme: '/css/pages.css'
        },
        author: 'Revox'
    }
    
    var updateTheme=function() {
        $scope.app = {
        name: 'Pages',
        description: 'Admin Dashboard UI kit',
        layout: {
            menuPin: false,
            menuBehind: false,
            theme: ThemeService.foo
        },
        author: 'Revox'
        }
    }

    $scope.moment = function(date) {
        return moment(date);
    };

    ThemeService.registerObserverCallback(updateTheme);
    // Checks if the given state is the current state
    $scope.is = function(name) {
        return $state.is(name);
    }

    // Checks if the given state/child states are present
    $scope.includes = function(name) {
        return $state.includes(name);
    }

    $scope.stringifyArray=function(array) {
        var string='';
        for (var i = 0; i < array.length; i++) {
            if((i+1)==array.length){
                string+=array[i];
            }else {
                string+=array[i]+",";
            }
        }
        return string;
    }

    // Broadcasts a message to pgSearch directive to toggle search overlay
    $scope.showSearchOverlay = function() {
        $scope.$broadcast('toggleSearchOverlay', {
            show: true
        })
    }

    $scope.summernote_options = {
    disableDragAndDrop:true,
    height: 200,
    onfocus: function(e) {
        $('body').addClass('overlay-disabled');
    },
    onblur: function(e) {
        $('body').removeClass('overlay-disabled');
    }
    }

    $scope.focus = function(e) {
        $('body').addClass('overlay-disabled');
    };
    $scope.blur = function(e) {
        $('body').removeClass('overlay-disabled');
    };

    // Functions for scheduled blocks formating for database 
    $scope.convertDaysForDatabase = function(days) {
        var workingDays=[];
        var weekend=[];
        if(days.workingDays.mon==true){
            workingDays.push('Mon');
        }
        if(days.workingDays.tue==true){
            workingDays.push('Tue');
        }
        if(days.workingDays.wed==true){
            workingDays.push('Wed');
        }
        if(days.workingDays.thu==true){
            workingDays.push('Thu');
        }
        if(days.workingDays.fri==true){
            workingDays.push('Fri');
        }
        if(days.weekend.sat==true){
            weekend.push('Sat');
        }
        if(days.weekend.sun==true){
            weekend.push('Sun');
        }
        var arrayToRet=[];
        arrayToRet.push(workingDays);
        arrayToRet.push(weekend);
        return arrayToRet;
    }
    
    $scope.convertHoursInMinutes=function(time) {
        var minutes=0;
        if(parseInt(time.interval.substring(0,1))==0){
            var minutes = parseInt((time.interval.substring(1,2)*60)+parseInt(time.interval.substring(3,5)));
        }else {
            var minutes = parseInt((time.interval.substring(0,2)*60)+parseInt(time.interval.substring(3,5)));
        }
        return minutes;
    }

    // Functions for scheduled blocks formating for view 
    $scope.convertDaysForView = function(string) {
        var days=[];
        var workingDays=[];
        var weekend=[];
        if(string.includes("Mon")) {
            workingDays.mon=true;
        }
        if(string.includes("Tue")) {
            workingDays.tue=true;
        }
        if(string.includes("Wed")) {
            workingDays.wed=true;
        }
        if(string.includes("Thu")) {
            workingDays.thu=true;
        }
        if(string.includes("Fri")) {
            workingDays.fri=true;
        }
        if(string.includes("Sat")) {
            weekend.sat=true;
        }
        if(string.includes("Sun")) {
            weekend.sun=true;
        }
        days.workingDays=workingDays;
        days.weekend=weekend;
        return days;
    }

    $scope.convertMinutesIntoHours=function(number) {
        var result=0;
        // if(number < 60){
        // result = (number) + 'm';        
        // }
        if(number%60==0){
            result = (number-number%60)/60 +':00 hrs';
            result='0'+result;        
        }
        else{
             result = ((number-number%60)/60 +':'+ number%60 + ' hrs');
             result='0'+result;
        }
        return result;
    }

    // Converting options from checkboxes to format for database 

    $scope.convertOptionForDatabase = function(checkboxes) {
        var optionsForDTBS=[];
                console.log(checkboxes)
        for (var i = 0; i < checkboxes.length; i++) {
            if(checkboxes[i].active==true){
                optionsForDTBS.push(checkboxes[i].id);
                console.log(optionsForDTBS[i])
            }
        }
        return optionsForDTBS;
    }
    
    // Functions for paginations
    $scope.checkForPageURL = function(currentPage,prevUrl){
        console.log('Current:'+currentPage+' PrevUrL:'+prevUrl)
        if (currentPage > prevUrl) {
            $scope.disableSlideLeft=true;
            $scope.disableSlideRight=false;
            $scope.disableSlideRightBack=false;
            $scope.disableSlideLeftBack=true;
        }
        if(currentPage < prevUrl) {
            $scope.disableSlideLeft=false;
            $scope.disableSlideRight=false;
            $scope.disableSlideRightBack=true;
            $scope.disableSlideLeftBack=false;
        }
    };

    $scope.checkForAnimationAppearance = function() {
        if ($scope.disableSlideLeft==true) {
            $scope.disableSlideLeft=false;
            $scope.disableSlideRight=true;
            $scope.disableSlideRightBack=false;
            $scope.disableSlideLeftBack=false;
        }
        if($scope.disableSlideRightBack ==true) {
            $scope.disableSlideRight=false;
            $scope.disableSlideLeft=true;
            $scope.disableSlideRightBack=false;
            $scope.disableSlideLeftBack=false;
        }
    };

    $scope.getNumber = function(num) { 
        return new Array(num);   
    }
}]);

    app.factory('ThemeService', function () {

      var observerCallbacks = [];

        //register an observer
        return{
              registerObserverCallback : function(callback){
                observerCallbacks.push(callback);
              },

              //call this when you know 'foo' has been changed
              notifyObservers : function(){
                angular.forEach(observerCallbacks, function(callback){
                    callback();
                });
              }
        }

    });


var app= angular.module('booking')
    app.directive('includeReplace', function() {
        return {
            require: 'ngInclude',
            restrict: 'A',
            link: function(scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    })