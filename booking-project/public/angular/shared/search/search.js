'use strict';

/* Controllers */

var app = angular.module('booking');
app.controller('SearchCtrl', ['$scope', 'SearchService', '$location', function($scope, SearchService, $location) {

    $scope.searchResult = [];

    $scope.liveSearch = function(querry) {
        SearchService.quickSearch(querry).then(function(response) {
            $scope.searchResult = response.data.entity;
            console.log($scope.searchResult);
        }).catch(function(error) {
            console.log(error);
        });
        console.log("Live search for: " + $scope.search.query);
    };

    $scope.editAddonGo = function (id) {
        $scope.url = '/managment/add_ons/edit/';
        $location.path($scope.url + id);
        $scope.$broadcast('toggleSearchOverlay', {
            show: false
        });
    };

    $scope.editEventGo = function (id) {
        $scope.url = '/managment/events/edit/';
        $location.path($scope.url + id);
        $scope.$broadcast('toggleSearchOverlay', {
            show: false
        });
    };

    $scope.editPackageGo = function (id) {
        $scope.url = '/managment/packages/edit/';
        $location.path($scope.url + id);
        $scope.$broadcast('toggleSearchOverlay', {
            show: false
        });
    };

    $scope.editUserGo = function (id) {
        $scope.url = '/managment/users/edit/';
        $location.path($scope.url + id);
        $scope.$broadcast('toggleSearchOverlay', {
            show: false
        });
    };
}]);