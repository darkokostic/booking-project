var app = angular.module('booking');
app.service('SearchService', function($q,$http,$rootScope) {
    return {
        quickSearch: function(querry) {
            console.log(querry);
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'keyword': querry
                },
                url: 'api/global/search'
            }).then(function successCallback(response) {
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    };
});