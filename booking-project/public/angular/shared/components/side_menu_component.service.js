var app = angular.module('booking');
app.service('SideMenuService', function($q,$http,$rootScope) {
    return {
        addLocation: function(newLocation) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                data: {
                    'name':newLocation.name,
                    'location':newLocation.location
                },
                url: 'api/locations'
            }).then(function successCallback(response) {
                console.log(response);
                defer.resolve(response);
                return response;
            }).catch(function errorCallback(response) {
                defer.reject(response);
                return response;
            });
            return defer.promise;
        }
    };
});