var app = angular.module('booking');
app.controller('SideMenuController', ['$scope','$location','$rootScope', '$state', '$cookies','LoginService', 'SideMenuService', function($scope,$location,$rootScope, $state, $cookies,LoginService, SideMenuService) {

    $scope.firstname = $cookies.get('firstname');
    $scope.lastname = $cookies.get('lastname');
    $scope.avatar_url = $cookies.get('avatar_url');
    $scope.newLocation=[];
    $scope.locations=[];
    $scope.locationToChange='';

    $scope.logout = function() {
        $cookies.remove('access_token');
        $cookies.remove('refresh_token');
        $cookies.remove('location_id');
        $state.go('login');
    };
    LoginService.getLocations().then(function(response) {
    	$scope.locations=response.data.entity.locations;
    	for (var i =0; i < $scope.locations.length; i++){
    	    if ($scope.locations[i].id == $cookies.get('location_id')) {
    	        $scope.locations[i].active = true;
    	        $scope.locationToChange=i;
            }
        }
    }).catch(function(error) {
        console.log(error);
    })

    $scope.setActiveLocation = function(locationId,index) {
        $scope.locations[$scope.locationToChange].active=null;
        $scope.locations[index].active=true;
        $scope.locationToChange=index;
        $cookies.put('location_id',locationId);
        $location.path('/dashboard');
    }

    $scope.addLocation = function(){
        SideMenuService.addLocation($scope.newLocation).then(function(response) {
            $scope.locations.push(response.data.entity);
            $scope.newLocation=[];
            $('#modalFillIn').modal('hide');
            $('body').pgNotification({
                style: 'simple',
                message: response.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'success'
            }).show();
        }).catch(function(error) {
            $('body').pgNotification({
                style: 'simple',
                message: error.data.message,
                position: 'top-right',
                timeout: 4000,
                type: 'warning'
            }).show();
            console.error(error);
        });
    };


	// $scope.location1 = function() {
	// 	$rootScope.urlLocation = 'https://jsonplaceholder.typicode.com/posts/';
	// 	$rootScope.urlLocationSingle = 'https://jsonplaceholder.typicode.com/posts/';
	// 	if ($location.path()=='/managment/packages') {
	// 		$rootScope.firstPagePackages();
	// 	};
	// 	if ($location.path()=='/managment/add_ons') {
	// 		$rootScope.firstPageAddOns();
	// 	};
	// 	if ($location.path()=='/managment/policy') {
	// 		$rootScope.firstPagePolicy();
	// 	};
	// 	if ($location.path()=='/managment/scheduled_blocks') {
	// 		$rootScope.firstPageScheduledBlocks();
	// 	};
	// 	if ($location.path()=='/managment/events') {
	// 		$rootScope.firstPageEvents();
	// 	};
	// }
	
	// $scope.location2 = function() {
	// 	$rootScope.urlLocationSingle = 'https://jsonplaceholder.typicode.com/posts/';
	// 	$rootScope.urlLocation = 'https://jsonplaceholder.typicode.com/posts/?_start=20&_end=80';
	// 	if ($location.path()=='/managment/packages') {
	// 		$rootScope.firstPagePackages();
	// 	};
	// 	if ($location.path()=='/managment/add_ons') {
	// 		$rootScope.firstPageAddOns();
	// 	};
	// 	if ($location.path()=='/managment/policy') {
	// 		$rootScope.firstPagePolicy();
	// 	};
	// 	if ($location.path()=='/managment/scheduled_blocks') {
	// 		$rootScope.firstPageScheduledBlocks();
	// 	};
	// 	if ($location.path()=='/managment/events') {
	// 		$rootScope.firstPageEvents();
	// 	};
	// }

}]);