var app = angular.module('booking');
app.factory('httpInterceptor', function($q, $cookies, $location) {
    function request(config) {
        if($cookies.get('access_token')) {
            config.headers.Authorization = 'Bearer ' + $cookies.get('access_token');
        }
        return config;
    }

    function response(response) {
        return response;
    }

    function responseError(error) {
        console.log(error);
        switch (error.status) {
            case 400:
                if(error.data.entity != null) {
                    angular.forEach(error.data.entity, function (error, value) {
                        for(var i = 0; i < error.length; i++) {
                            $('body').pgNotification({
                                style: 'simple',
                                message: error[i],
                                position: 'top-right',
                                timeout: 4000,
                                type: 'error'
                            }).show();
                        }
                    });
                } else {
                    $('body').pgNotification({
                        style: 'simple',
                        message: error.data.message,
                        position: 'top-right',
                        timeout: 4000,
                        type: 'error'
                    }).show();
                }

                break;
            case 401:
                $cookies.remove('access_token');
                $cookies.remove('refresh_token');
                $cookies.remove('location_id');
                $location.path('/');
                break;
            case 403:
                $cookies.remove('access_token');
                $cookies.remove('refresh_token');
                $cookies.remove('location_id');
                $location.path('/');
                break;
            case 404:
                error.data.entity = null;
                error.data.message = "Not Found";
                break;
            case 500:
                error.data.entity = null;
                error.data.message = "Something went wrong with server";
        }
        return $q.reject(error);
    }

    return {
        request: request,
        response: response,
        responseError: responseError
    };
});