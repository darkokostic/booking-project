
angular.module('booking')
    .directive('pgSearch', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                console.log("Prolaz pocetak",element)
                $(element).search();
                scope.$on('toggleSearchOverlay', function(scopeDetails, status) {
                    if(status.show){
                        $(element).data('pg.search').toggleOverlay('show');
                    } else {
                        $(element).data('pg.search').toggleOverlay('hide');
                    }
                })

            }
        }
    }]);