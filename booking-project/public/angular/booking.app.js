var myApp = angular.module('booking', [
    'ui.bootstrap',
    'ui.router',
    'oc.lazyLoad',
    'angularCSS',
    'ngCookies',
    'dndLists',
    'angular-svg-round-progressbar',
    'ui.bootstrap.modal'
    //UCITAJ OSTATAK
    ]);

myApp.run(function($rootScope, $state, $cookies, $http){
    // $rootScope.rad="Radi";
    if($cookies.get('access_token')) {
        $state.go('side_menu.dashboard');
    }
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
       if(toState.url == '/' && $cookies.get('access_token')) { 
            $state.go('side_menu.dashboard');
       }
        if (toState.authenticate && !$cookies.get('access_token')){
            // User isn’t authenticated
            $state.go("login");
            event.preventDefault();
        }
    });
});
myApp.config(function($httpProvider, $stateProvider, $ocLazyLoadProvider, $urlRouterProvider) {
    $httpProvider.interceptors.push('httpInterceptor');

    var dashboardState = {
        name: 'side_menu.dashboard',
        url: '/dashboard',
        templateUrl: '/angular/views/dashboard/dashboard.view.html',
        controller: 'DashboardCtrl',
        authenticate: true,
        css:'/angular/views/dashboard/dashboard.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'rickshaw',
                    'metrojs',
                    'nvd3'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var eventsState = {
        name: 'side_menu.events',
        url: '/managment/events',
        templateUrl: '/angular/views/managment/events/view_events/events.view.html',
        controller: 'EventsCtrl',
        authenticate: true,
        css:'/angular/views/managment/events/view_events/events.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newEventsState = {
        name: 'side_menu.new_events',
        url: '/managment/events/new_events',
        templateUrl: '/angular/views/managment/events/new_events/new_events.view.html',
        controller: 'NewEventsCtrl',
        authenticate: true,
        css:'/angular/views/managment/events/new_events/new_events.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'jquery-ui',
                    'ui-grid',
                    'ui-jq',
                    'select',
                    'ios-list',
                    'autonumeric',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editEventsState = {
        name: 'side_menu.edit_events',
        url: '/managment/events/edit/:id',
        templateUrl: '/angular/views/managment/events/edit_events/edit_events.view.html',
        controller: 'EditEventsCtrl',
        authenticate: true,
        css:'/angular/views/managment/events/edit_events/edit_events.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var packageState = {
        name: 'side_menu.packages',
        url: '/managment/packages',
        templateUrl: '/angular/views/managment/packages/view_packages/packages.view.html',
        controller: 'PackageCtrl',
        authenticate: true,
        css: '/angular/views/managment/packages/view_packages/packages.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newPackageState = {
        name: 'side_menu.new_package',
        url: '/managment/packages/new_package',
        templateUrl: '/angular/views/managment/packages/new_package/new_package.view.html',
        controller: 'NewPackageCtrl',
        authenticate: true,
        css:'/angular/views/managment/packages/new_package/new_package.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'dropzone',
                    'datepicker',
                    'select',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var addOptionState = {
        name: 'side_menu.add_option',
        url: '/managment/packages/option/add_option',
        templateUrl: '/angular/views/managment/packages/option/add_option/add_option.view.html',
        controller: 'AddOptionCtrl',
        authenticate: true,
        css:'/angular/views/managment/packages/option/add_option/add_option.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'select',
                    'autonumeric',
                    'moment'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editOptionState = {
        name: 'side_menu.edit_option',
        url: '/managment/packages/option/edit/:id',
        templateUrl: '/angular/views/managment/packages/option/edit_option/edit_option.view.html',
        controller: 'EditOptionCtrl',
        authenticate: true,
        css:'/angular/views/managment/packages/option/edit_option/edit_option.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'select',
                    'autonumeric',
                    'moment'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editPackageState = {
        name: 'side_menu.edit_package',
        url: '/managment/packages/edit/:id',
        templateUrl: '/angular/views/managment/packages/edit_package/edit_package.view.html',
        controller: 'EditPackageCtrl',
        authenticate: true,
        css:'/angular/views/managment/packages/edit_package/edit_package.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var mainState = {
        name: 'main',
        controller: 'AppCtrl'
    };

    var addOnsState = {
        name: 'side_menu.add_ons',
        url: '/managment/add_ons',
        templateUrl: '/angular/views/managment/add_ons/view_add_ons/add_ons.view.html',
        controller: 'AddOnsCtrl',
        authenticate: true,
        css:'/angular/views/managment/add_ons/view_add_ons/add_ons.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editAddOnsState = {
        name: 'side_menu.edit_add_ons',
        url: '/managment/add_ons/edit/:id',
        templateUrl: '/angular/views/managment/add_ons/edit_add_ons/edit_add_ons.view.html',
        controller: 'EditAddOnsCtrl',
        authenticate: true,
        css:'/angular/views/managment/add_ons/edit_add_ons/edit_add_ons.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newAddOnsState = {
        name: 'side_menu.new_add_ans',
        url: '/managment/add_ons/new_add_ons',
        templateUrl: '/angular/views/managment/add_ons/new_add_ons/new_add_ons.view.html',
        controller: 'NewAddOnsCtrl',
        authenticate: true,
        css:'/angular/views/managment/add_ons/new_add_ons/new_add_ons.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var policyState = {
        name: 'side_menu.policy',
        url: '/managment/policy',
        templateUrl: '/angular/views/managment/policy/view_policy/policy.view.html',
        controller: 'PolicyCtrl',
        authenticate: true,
        css:'/angular/views/managment/policy/view_policy/policy.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editPolicyState = {
        name: 'side_menu.edit_policy',
        url: '/managment/policy/edit/:id',
        templateUrl: '/angular/views/managment/policy/edit_policy/edit_policy.view.html',
        controller: 'EditPolicyCtrl',
        authenticate: true,
        css:'/angular/views/managment/policy/edit_policy/edit_policy.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newPolicyState = {
        name: 'side_menu.new_policy',
        url: '/managment/policy/new_policy',
        templateUrl: '/angular/views/managment/policy/new_policy/new_policy.view.html',
        controller: 'NewPolicyCtrl',
        authenticate: true,
        css:'/angular/views/managment/policy/new_policy/new_policy.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'select',
                    'dropzone',
                    'ios-list',
                    'autonumeric',
                    'ui-jq',
                    'wysihtml5',
                    'summernote'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var usersState = {
        name: 'side_menu.users',
        url: '/managment/users',
        templateUrl: '/angular/views/managment/users/view_users/users.view.html',
        controller: 'UsersCtrl',
        authenticate: true,
        css:'/angular/views/managment/users/view_users/users.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editUserState = {
        name: 'side_menu.edit_user',
        url: '/managment/users/edit/:id',
        templateUrl: '/angular/views/managment/users/edit_user/edit_user.view.html',
        controller: 'EditUserCtrl',
        authenticate: true,
        css:'/angular/views/managment/users/edit_user/edit_user.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'dropzone',
                    'scrollbar',
                    'switchery',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newUserState = {
        name: 'side_menu.new_user',
        url: '/managment/users/new_user',
        templateUrl: '/angular/views/managment/users/new_user/new_user.view.html',
        controller: 'NewUserCtrl',
        authenticate: true,
        css:'/angular/views/managment/users/new_user/new_user.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'dropzone',
                    'scrollbar',
                    'switchery',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var scheduledBlocksState = {
        name: 'side_menu.scheduled_blocks',
        url: '/managment/scheduled_blocks',
        templateUrl: '/angular/views/managment/scheduled_blocks/view_scheduled_blocks/scheduled_blocks.view.html',
        controller: 'ScheduledBlocksCtrl',
        authenticate: true,
        css:'/angular/views/managment/scheduled_blocks/view_scheduled_blocks/scheduled_blocks.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'select',
                    'moment'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var editScheduledBlocksState = {
        name: 'side_menu.edit_scheduled_blocks',
        url: '/managment/scheduled_blocks/edit/:id',
        templateUrl: '/angular/views/managment/scheduled_blocks/edit_scheduled_blocks/edit_scheduled_blocks.view.html',
        controller: 'EditScheduledBlocksCtrl',
        authenticate: true,
        css:'/angular/views/managment/scheduled_blocks/edit_scheduled_blocks/edit_scheduled_blocks.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'select',
                    'autonumeric',
                    'moment'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var newScheduledBlocksState = {
        name: 'side_menu.new_scheduled_blocks',
        url: '/managment/scheduled_blocks/new_scheduled_blocks',
        templateUrl: '/angular/views/managment/scheduled_blocks/new_scheduled_blocks/new_scheduled_blocks.view.html',
        controller: 'NewScheduledBlocksCtrl',
        authenticate: true,
        css:'/angular/views/managment/scheduled_blocks/new_scheduled_blocks/new_scheduled_blocks.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'select',
                    'autonumeric',
                    'moment'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    var generalPolicyState = {
        name: 'side_menu.general_policy',
        url: '/policy',
        templateUrl: '/angular/views/policy/policy.view.html',
        controller: 'GeneralPolicyCtrl',
        css:'/angular/views/policy/policy.css'
    };

    var termsState = {
        name: 'side_menu.terms',
        url: '/terms',
        templateUrl: '/angular/views/terms/terms.view.html',
        controller: 'TermsCtrl',
        css:'/angular/views/terms/terms.css'
    };

    var sideMenuState = {
        name: 'side_menu',
        templateUrl: '/angular/shared/components/side_menu_component.html',
        controller: 'SideMenuController',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'pace',
                    'scrollbar',
                    'font-awesome'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var searchState = {
        name: 'search_state',
        controller: 'SearchCtrl'
    };
    var LoginState = {
        name: 'login',
        url: '/',
        templateUrl: '/angular/views/login/login.view.html',
        controller: 'LoginController',
        css:'/angular/views/login/login.view.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'font-awesome',
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var RegisterState = {
        name: 'register',
        url: '/register',
        templateUrl: '/angular/views/register/register.view.html',
        controller: 'RegisterController',
        css:'/angular/views/register/register.view.css'
    };
    var lockScreenState = {
        name: 'lockscreen',
        url: '/lockscreen',
        templateUrl: '/angular/views/lock_screen/lock_screen.view.html',
        controller: 'LockscreenController',
        css:'/angular/views/lock_screen/lock_screen.view.css'
    };
    var couldntFindPageState = {
        name: '404page',
        url: '/404',
        templateUrl: '/angular/views/404_page/404_page.view.html',
        controller: '404pageController',
        css:'/angular/views/404_page/404_page.view.css'
    };
    var profileState = {
        name: 'side_menu.profile',
        url: '/settings/profile',
        authenticate: true,
        templateUrl: '/angular/views/settings/profile/profile.html',
        controller: 'ProfileController',
        css:'/angular/views/settings/profile/profile.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'dropzone',
                    'scrollbar',
                    'font-awesome',
                    'scrollbar',
                    'switchery',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var generalSettingsState = {
        name: 'side_menu.general_settings',
        url: '/settings/general',
        authenticate: true,
        templateUrl: '/angular/views/settings/general/general.view.html',
        controller: 'GeneralSettingsController',
        css:'/angular/views/settings/general/general.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'scrollbar',
                    'font-awesome',
                    'dropzone',
                    'scrollbar',
                    'switchery',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var bookingEventState= {
        name: 'booking_event',
        url: '/booking_event/:packageId',
        // authenticate: true,
        templateUrl: '/angular/views/booking_event/booking_event.view.html',
        controller: 'bookingEventController',
        css:'/angular/views/booking_event/booking_event.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'switchery',
                    'datepicker',
                    'font-awesome',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var categoriesState = {
        name: 'side_menu.categories',
        url: '/managment/categories',
        authenticate: true,
        templateUrl: '/angular/views/managment/categories/categories.view.html',
        controller: 'CategoriesController',
        css:'/angular/views/managment/categories/categories.css'
    };
    var locationsState = {
        name: 'side_menu.locations',
        url: '/managment/locations',
        authenticate: true,
        templateUrl: '/angular/views/managment/locations/locations.view.html',
        controller: 'LocationsController',
        css:'/angular/views/managment/locations/locations.css'
    };
    var listOrderState = {
        name: 'side_menu.list_order',
        url: '/managment/list_order',
        authenticate: true,
        templateUrl: '/angular/views/managment/order/list_order/list_order.view.html',
        controller: 'ListOrderController',
        css:'/angular/views/managment/order/list_order/list_order.css'
    };
    var editOrderState = {
        name: 'side_menu.edit_order',
        url: '/managment/edit_order',
        authenticate: true,
        templateUrl: '/angular/views/managment/order/edit_order/edit_order.view.html',
        controller: 'ListOrderController',
        css:'/angular/views/managment/order/edit_order/edit_order.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'inputMask',
                    'select',
                    'datepicker',
                    'ui-jq',
                    'ios-list',
                    'autonumeric'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };
    var editPackageOrderState = {
        name: 'side_menu.edit_package_order',
        url: '/managment/edit_package_order',
        authenticate: true,
        templateUrl: '/angular/views/managment/order/edit_package_order/edit_package_order.view.html',
        controller: 'EditPackageOrderController',
        css:'/angular/views/managment/order/edit_package_order/edit_package_order.css',
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'switchery',
                    'datepicker',
                    'font-awesome',
                    'ui-jq',
                    'ios-list',
                    'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                });
            }]
        }
    };

    $stateProvider.state(dashboardState); //DASHOBARD
    $stateProvider.state(eventsState);  //EVENTS
    $stateProvider.state(newEventsState);  //NEW_EVENTS
    $stateProvider.state(editEventsState);  //EDIT_EVENTS
    $stateProvider.state(packageState);  //PACKAGE
    $stateProvider.state(newPackageState);  //NEW PACKAGE
    $stateProvider.state(editPackageState);  //EDIT PACKAGE
    $stateProvider.state(sideMenuState); //SIDE MENU
    $stateProvider.state(searchState); // Search
    $stateProvider.state(LoginState); //LOGIN
    $stateProvider.state(RegisterState); //REGISTER
    $stateProvider.state(addOnsState); //ADD ONS
    $stateProvider.state(editAddOnsState); //EDIT ONS
    $stateProvider.state(newAddOnsState); //NEW ONS
    $stateProvider.state(policyState); //POLICY
    $stateProvider.state(editPolicyState); //EDIT POLICY
    $stateProvider.state(newPolicyState); //NEW POLICY
    $stateProvider.state(usersState); //USERS
    $stateProvider.state(editUserState); //EDIT USER
    $stateProvider.state(newUserState); //NEW USER
    $stateProvider.state(scheduledBlocksState); //Scheduled Blocks
    $stateProvider.state(editScheduledBlocksState); //Edit Scheduled Blocks
    $stateProvider.state(newScheduledBlocksState); //New Scheduled Blocks
    $stateProvider.state(addOptionState); // ADD OPTIONS IN PACKAGES
    $stateProvider.state(editOptionState); // EDIT OPTIONS IN PACKAGES
    $stateProvider.state(generalPolicyState); // GENERAL POLICY
    $stateProvider.state(termsState); // TERMS
    $stateProvider.state(mainState); // MAIN
    $stateProvider.state(lockScreenState); // LOCKSCREEN
    $stateProvider.state(couldntFindPageState); // 404Page
    $stateProvider.state(profileState); // PROFILE
    $stateProvider.state(generalSettingsState); // GENERAL SETTINGS
    $stateProvider.state(bookingEventState); // BOOKING EVENT
    $stateProvider.state(categoriesState); // CATEGORIES
    $stateProvider.state(locationsState); // LOCATIONS
    $stateProvider.state(listOrderState); // ORDER_LIST
    $stateProvider.state(editOrderState); // EDIT ORDER
    $stateProvider.state(editPackageOrderState); // EDIT PACKAGE ORDER
    $urlRouterProvider.otherwise('/');
});