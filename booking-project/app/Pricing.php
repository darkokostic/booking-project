<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tier1', 'tier2', 'tier3', 'deposit_money', 'deposit_percent', 'depositType', 'balanceDue', 'isFree', 'isAdditionalGuests', 'perAdditionalGuest', 'balance_due', 'per_additional_guest', 'is_birthday_persone_free', 'two_guest_of_honor', 'second_guest_of_honor_charge'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'tier1' => 'array',
        'tier2' => 'array',
        'tier3' => 'array',
        'deposit' => 'integer',
        'isFree' => 'boolean',
        'isAdditionalGuests' => 'boolean',
        'perAdditionalGuest' => 'boolean'
    ];
}
