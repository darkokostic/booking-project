<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'package_option_id', 'location_id', 'steps'
    ];

    protected $casts = [
        'steps' => 'array',
    ];

    public function package_option() {
        return $this->belongsTo('App\PackageOption');
    }

    public function questions() {
        return $this->hasMany('App\Question');
    }

    public function images() {
        return $this->morphOne('App\Image', 'imageable');
    }

    /**
     * Eloquent Search
     */

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
