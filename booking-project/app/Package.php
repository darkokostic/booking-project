<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'location_id'
    ];

    public function images() {
    	return $this->morphMany('App\Image', 'imageable');
    }

    public function events() {
        return $this->hasMany('App\Event');
    }

    public function location() {
    	return $this->belongsTo('App\Location');
    }

    public function package_options() {
        return $this->belongsToMany('App\PackageOption', 'package_package_options', 'package_id');
    }

    /**
     * Eloquent Search
     */

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('images')->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
