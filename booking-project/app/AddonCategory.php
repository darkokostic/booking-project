<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddonCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function addons() {
    	return $this->hasMany('App\Addon');
    }
}
