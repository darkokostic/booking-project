<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class ImageUploader
{

    public static function upload($file, $path)
    {
        $url = Storage::put('uploads/'.$path, $file);

        return $url;
    }

    public static function remove($path) {
        return Storage::delete($path);
    }
}