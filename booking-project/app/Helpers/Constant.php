<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class Constant
{
    //App\Order statuses
    const ORDER_FAILED = 'FAILED';
    const ORDER_OPENED = 'OPENED';
    const ORDER_COMPLETED = 'COMPLETED';

    //Roles for user
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_CLIENT = 'ROLE_CLIENT';

    //Package Settings and Options
    const TIER_GUESTS_INCLUDED = 'guests_included';

    //Assets routes
    const IMAGES_PATH = '/uploads/';
    const PACKAGE_IMAGES_PATH = '/uploads/packages';
    const ADDONS_IMAGES_PATH = '/uploads/addons';
    const USERS_IMAGES_PATH = '/uploads/users';
    const EVENTS_IMAGES_PATH = '/uploads/events';

    //Class identifiers
    const PACKAGE_IDENTIFIER = 'App\Package';
    const ADDON_IDENTIFIER = 'App\Addon';
    const USER_IDENTIFIER = 'App\User';
    const EVENT_IDENTIFIER = 'App\Event';

    const URL_PACKAGES = 'packages';
    const URL_USERS = 'users';
    const URL_ADDONS = 'addons';
    const URL_EVENTS = 'events';

    /**
     * @param $constant
     * @return string
     */
    public static function absolutePath($constant)
    {
        return public_path() . constant('self::' . $constant);
    }

    /**
     * Copies a file from source
     * to destination
     * @param $source
     * @param $destination
     */
    public static function copy($source, $destination) {
        copy($source, $destination);
        return File::exists($destination);
    }

    /**
     * Creates a directory
     * @param $path
     */
    public static function makeDir($path) {
        File::exists($path) or File::makeDirectory($path, 0777, true);
    }

    /**
     * Delete recursively without deleting parent
     * @param $source
     */
    public static function removeDir($source) {
        return File::deleteDirectory($source, true);
    }
}