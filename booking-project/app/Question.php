<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'description', 'event_id',
    ];

    public function event() {
        return $this->belongsTo('App\Event');
    }

    public function question_answers() {
        return $this->hasMany('App\QuestionAnswer', 'question_id');
    }
}
