<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location'
    ];

    /**
     * User to which this location belongs to.
     */
    public function user() {
    	return $this->belongsTo('App\User');
    }

    /**
     * Get the addons for the location.
     */
    public function addons() {
        return $this->hasMany('App\Addon');
    }

    /**
     * Get orders for specific location
     */
    public function orders() {
        return $this->hasMany('App\Order');
    }    
}
