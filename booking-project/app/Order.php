<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'time_block_id','status', 'guests_count', 'total', 'order_date', 'directary_restrictions', 'notes', 'package_option_id',
        'location_id', 'host_firstname', 'host_lastname', 'host_email', 'host_phone', 'host_ext', 'mailing_address',
        'city', 'state', 'zip_code', 'birthday_firstname', 'birthday_lastname', 'birthday_date_of_birth',
        'guest_of_honor_firstname', 'guest_of_honor_lastname', 'guest_of_honor_date_of_birth', 'event_date'
    ];

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * Get the addons for the location.
     */
    public function package()
    {
        return $this->hasOne('App\Package');
    }

    /**
     * All addons for this order.
     */
    public function addons()
    {
        return $this->belongsToMany('App\Addon', 'addon_order');
    }
}
