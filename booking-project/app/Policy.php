<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'isGlobal'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'isGlobal' => 'boolean'
    ];

    public function packages() {
    	return $this->hasMany('App\Package');
    }

    public function questions() {
        return $this->hasMany('App\PolicyQuestion');
    }

    public function location() {
        return $this->belongsTo('App\Location');
    }

    public function options() {
        return $this->belongsToMany('App\PackageOption', 'policy_package_options', 'policy_id');
    }

    /**
     * Eloquent Search
     */

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
