<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeBlock extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    protected $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start', 'end', 'isBooked'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'isBooked' => 'boolean'
    ];

    public function schedule() {
    	return $this->belongsTo('App\Schedule');
    }
}
