<?php

namespace App\Console\Commands;

use App\Helpers\Constant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GenerateDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assets:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup application to properly work';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info("Building...");

        Constant::removeDir(Constant::absolutePath("IMAGES_PATH"));
        Constant::makeDir(Constant::absolutePath("PACKAGE_IMAGES_PATH"));
        Constant::makeDir(Constant::absolutePath("ADDONS_IMAGES_PATH"));
        Constant::makeDir(Constant::absolutePath("USERS_IMAGES_PATH"));

        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_USERS.'/rb13.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/cyboticx.jpg', 'public/uploads/'.Constant::URL_USERS.'/rb14.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_USERS.'/rb15.jpg');


        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_PACKAGES.'/rb14.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/cyboticx.jpg', 'public/uploads/'.Constant::URL_PACKAGES.'/rb15.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_PACKAGES.'/rb13.jpg');

        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_ADDONS.'/rb15.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/cyboticx.jpg', 'public/uploads/'.Constant::URL_ADDONS.'/rb14.jpg');
        Constant::copy(public_path() . '/../resources/assets/images/rb13.jpg', 'public/uploads/'.Constant::URL_ADDONS.'/rb13.jpg');

        $this->info("Build done.");
    }
}
