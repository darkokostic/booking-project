<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'username', 'phone', 'status', 'event_avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Set user's password and hash it.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function images() {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function locations() {
        return $this->hasMany('App\Location');
    }

    public function user_settings() {
        return $this->hasOne('App\UserSettings');
    }

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    /**
     * Eloquent Search
     */

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('images')->where(function ($query) use ($keyword) {
                $query->where([["firstname", "LIKE","%$keyword%"], ["status", "=", "1"]])
                    ->orWhere("lastname", "LIKE", "%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere("username", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
