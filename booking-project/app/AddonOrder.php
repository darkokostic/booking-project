<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddonOrder extends Model
{
    /**
     * @var array
     */
    public $fillable = [
        'addon_id', 'order_id', 'name', 'price', 'quantity', 'description', 'is_taxable', 'location_id', 'addon_category_id', 'unit_of_measure'
    ];

    /**
     * @var string
     */
    public $table = 'addon_order';
}
