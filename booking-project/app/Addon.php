<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addon extends Model
{
	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'quantity', 'description', 'image', 'isTaxable', 'addon_category_id', 'location_id', 'package_options','unit_of_measure',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'isTaxable' => 'boolean',
        'quantity' => 'integer'
    ];

    /**
     * Thumbnail for this particular addon.
     */
    public function image() {
    	return $this->morphOne('App\Image', 'imageable');
    }

    /**
     * Category this addon belongs to.
     */
    public function category() {
        return $this->belongsTo('App\AddonCategory');
    }

    /**
     * Location this addon belongs to.
     */
    public function location() {
        return $this->belongsTo('App\Location');
    }

    /**
     * Many package options in which this addon is.
     */
    public function options() {
        return $this->belongsToMany('App\PackageOption', 'addon_package_options', 'addon_id');
    }

    /**
     * Local search scope for addons by keyword
     * params searchable are [name, description]
     */
    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('image')->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    /**
     * Many orders in which this addon is in.
     */
    public function orders() {
        return $this->belongsToMany('App\Order', 'addon_order');
    }
}
