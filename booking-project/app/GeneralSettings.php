<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralSettings extends Model
{
    /**
     * @var array
     */
    public $fillable = ['menu_position', 'template_src'];

    /**
     * @var string
     */
    public $table = 'general_settings';
}
