<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_src', 'menu_position', 'ribbon_color', 'ribbon_name', 'user_id', 'ribbon_color_name',
    ];

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
