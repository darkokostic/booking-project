<?php

namespace App\Http\Controllers;

use App\Http\Requests\Location\EditLocationRequest;
use App\Http\Requests\Location\LocationRequest;
use App\Location;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $location = $user->locations;
        $user->locations= $location;
        if ($user) {
            return response()->custom(200,'Successfully get locations!', $user);
        }
        return response()->custom(200,'There are no locations!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        $user = Auth::user();
        $location = new Location();
        $location->fill($request->all());
        $location->user_id = $user->id;
        if ($location->save()) {
            return response()->custom(200,'Successfully created location!', $location);
        }
        return response()->custom(400,'Your location wasn\'t created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location = Location::where('id',$id)->get();
        if ($location) {
            return response()->custom(200,'Successfully get location!', $location);
        }
        return response()->custom(400,'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(EditLocationRequest $request, Location $location)
    {
        try {
            $location->fill($request->all());
            if ($location->save()) {
                return response()->custom(200, 'Successfully updated location!', $location);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your location wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $location = Location::findOrFail($id);
            if ($location && $location->delete()) {
                return response()->custom(200, 'Successfully deleted location!', null);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your location wasn\'t deleted!', null);
    }
}
