<?php

namespace App\Http\Controllers;

use App\AddonCategory;
use App\Http\Requests\AddonCategory\AddAddonCategoryRequest;
use App\Http\Requests\AddonCategory\EditAddonCategoryRequest;
use Illuminate\Http\Request;

class AddonCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = AddonCategory::get();
        if (count($category)) {
            return response()->custom(200,'Successfully retrieved categories!', $category);
        }
        return response()->custom(200,'There are no addon categories!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddAddonCategoryRequest $request)
    {
        $category = new AddonCategory();
        $category->fill($request->all());
        if ($category->save()) {
            return response()->custom(200,'Successfully create category!', $category);
        }
        return response()->custom(400,'Your category wasn\'t created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AddonCategory  $addonCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $addonCategory = AddonCategory::find($id);
        if (count($addonCategory)) {
            return response()->custom(200,'Successfully get location!', $addonCategory);
        }
        return response()->custom(400,'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AddonCategory  $addonCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AddonCategory $addonCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AddonCategory  $addonCategory
     * @return \Illuminate\Http\Response
     */
    public function update(EditAddonCategoryRequest $request, AddonCategory $addonCategory)
    {
        try {
            $category = AddonCategory::findOrFail($addonCategory->id);
            $category->fill($request->all());
            if ($category->save()) {
                return response()->custom(200, 'Successfully updated category!', $category);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your category wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AddonCategory  $addonCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = AddonCategory::find($id);
            if ($category && $category->delete()) {
                return response()->custom(200, 'Successfully deleted category!', null);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your category wasn\'t deleted!', null);

    }
}
