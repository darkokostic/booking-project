<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageUploader;
use App\Http\Requests\User\AddUserRequest;
use App\Image;
use App\User;
use App\UserSettings;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('images')->latest()->paginate(11);
        if (count($users)) {
            return response()->custom(200, 'Successfully retrieved users!', $users);
        }
        return response()->custom(200, 'There was an error!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        $filteredPerms = [];
        $permissions = json_encode($request->permissions);
        foreach ($permissions as $perm) {
            if ($perm) {
                array_push($filteredPerms, $perm);
            }
        }

        $user = new User();
        $user->fill($request->all());
        if ($user->save()) {
            //Permissions
            if ($filteredPerms) {
                $user->givePermissionTo($request->permissions);
            }

            $settings = new UserSettings();
            $settings->user_id = $user->id;
            if ($request->menu_position) {
                $settings->menu_position = $request->menu_position;
            }
            if ($request->template_src) {
                $settings->template_src = $request->template_src;
            }
            $settings->save();
            if ($request->hasFile('user_avatar')) {
                $url = ImageUploader::upload($request->file('user_avatar'), Constant::URL_USERS);
                $image = new Image();
                $image->imageable_id = $user->id;
                $image->imageable_type = Constant::USER_IDENTIFIER;
                $image->path = $url;
                $image->save();
            } else {
                $image = new Image();
                $image->imageable_id = $user->id;
                $image->imageable_type = Constant::USER_IDENTIFIER;
                $image->path = 'img/avatar.png';
                $image->save();
            }
            return response()->custom(200, 'Successfully created user!', $user->with('user_settings', 'images', 'permissions')->find($user->id));
        }
        return response()->custom(400, 'There was an error while creating user', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('images', 'user_settings', 'roles', 'permissions')->find($id);
        if ($user) {
            return response()->custom(200, 'Successfully retrieved user!', $user);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->fill($request->all());
            if ($request->hasFile('user_avatar')) {
                $user_avatar = Image::where('imageable_id', $user->id)->first();
                if ($user_avatar->path != 'img/avatar.png') {
                    ImageUploader::remove($user_avatar->path);
                }
                $url = ImageUploader::upload($request->file('user_avatar'), Constant::URL_USERS);
                $user_avatar->imageable_type = Constant::USER_IDENTIFIER;
                $user_avatar->path = $url;
                $user_avatar->save();
            }

            if ($user->save()) {
                $user->syncPermissions($request->permissions);

                $settings = UserSettings::where('user_id', $user->id)->first();
                if ($request->menu_position) {
                    $settings->menu_position = $request->menu_position;
                }
                if ($request->template_src) {
                    $settings->template_src = $request->template_src;
                }
                if ($settings->save()) {
                    return response()->custom(200, 'Successfully updated user!', $user->with('user_settings', 'images', 'permissions')->find($user->id));
                }
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'User wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            if ($user && $user->delete()) {
                return response()->custom(200, 'Successfully deleted user!', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your user wasn\'t deleted!', null);
    }

    public function searchUsersByName(Request $request)
    {
        $users = User::Search($request->search)->paginate(11);
        return response()->custom(200, 'Successfully retrieved users!', $users);
    }

}
