<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nikolag\Square\SquareCustomer;

class SquareController extends Controller
{
    public function charge()
    {
        $data = array(
            'firstName' => 'John',
            'lastName' => 'Doe',
            'companyName' => 'John Doe LTD.',
            'nickname' => 'Johny',
            'email' => 'john.doe@example.com',
            'phone' => '+123325990',
            'reference_id' => '555333',
            'note' => 'This is a trusted customer.'
        );
        $customer = new SquareCustomer($data);
        $amount = 200;
        $nonce = [];
        $transaction = $customer->charge($amount, $nonce);
    }
}
