<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\Event\AddEventRequest;
use App\Http\Requests\Event\EditEventRequest;
use App\Question;
use App\QuestionAnswer;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate(11);
        if (count($events)) {
            return response()->custom(200, 'Successfully retrieved events!', $events);
        }
        return response()->custom(200, 'There was an error!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddEventRequest $request)
    {
        $steps = $request->steps;
        $arr = array_merge(array_map('intval', array_slice($steps, 0)));
        $event = new Event();
        $event->fill($request->all());
        $event->steps = $arr;

        if ($event->save()) {
            if($request->questions) {
                foreach ($request->questions as $question) {
                    $new_question = new Question();
                    $new_question->type = $question['type'];
                    $new_question->description = $question['description'];
                    $new_question->event_id = $event->id;
                    if ($new_question->save()) {
                        if(isset($question['options'])) {
                            foreach ($question['options'] as $option) {
                                $question_answer = new QuestionAnswer();
                                $question_answer->answer = $option['answer'];
                                $question_answer->question_id = $new_question->id;
                                $question_answer->save();
                            }
                        }
                    }
                }
            }
            return response()->custom(200, 'Successfully created event!', $event->with('questions', 'questions.question_answers')->where('id', $event->id)->first());
        }
        return response()->custom(400, 'There was an error', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::with('package_option.packages', 'questions.question_answers')->find($id);
        if ($event) {
            return response()->custom(200, 'Successfully retrieved event!', $event);
        }
        return response()->custom(400, 'There was an error', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditEventRequest $request, $id)
    {
        try {
            $event = Event::findOrFail($id);
            $event->fill($request->all());
            if ($event->save()) {
                if ($request->questions) {
                    foreach ($request->questions as $question) {
                        $q = Question::find($question->id);
                        $q->type = $question['type'];
                        $q->description = $question['description'];
                        $q->event_id = $event->id;
                        if ($q->save()) {
                            foreach ($question['options'] as $option) {
                                $question_answer = QuestionAnswer::find($option['id']);
                                $question_answer->answer = $option['answer'];
                                $question_answer->question_id = $q->id;
                                $question_answer->save();
                            }
                        }
                    }
                }
                return response()->custom(200, 'Successfully updated event!', $event->with('questions', 'questions.question_answers')->where('id', $event->id)->first());
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Event was not updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $event = Event::find($id);
            if ($event && $event->delete()) {
                return response()->custom(200, 'Successfully deleted event!', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Event was not deleted!', null);
    }

    public function searchEventsByName(Request $request)
    {
        $events = Event::Search($request->search)->where('location_id', $request->location_id)->paginate(11);
        return response()->custom(200, 'Successfully retrieved events!', $events);
    }

    public function getEventsFromLocation($id)
    {
        $events = Event::where('location_id', $id)->latest()->paginate(11);
        if (count($events)) {
            return response()->custom(200, 'Successfully retrieved events!', $events);
        }
        return response()->custom(200, 'There was an error!', null);
    }
}
