<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageUploader;
use App\Http\Requests\Package\AddPackageRequest;
use App\Http\Requests\Package\DeletePackageRequest;
use App\Http\Requests\Package\EditPackageRequest;
use App\Image;
use App\Package;
use App\PackageOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPackageRequest $request)
    {
        $package = new Package();
        $package->fill($request->all());
        if ($package->save()) {
            if($request->package_options) {
                $package_options = explode(",", $request->package_options);
                foreach($package_options as $option) {
                    DB::table('package_package_options')->insert(
                        ['package_id' => $package->id, 'package_option_id' => $option]
                    );
                }
            }
           if($request->hasFile('images')) {
               foreach ($request->allFiles('images') as $file) {
                   $url = ImageUploader::upload($file, Constant::URL_PACKAGES);
                   $image = new Image();
                   $image->imageable_id = $package->id;
                   $image->imageable_type = Constant::PACKAGE_IDENTIFIER;
                   $image->path = $url;
                   $image->save();
               }
           }

            return response()->custom(200, 'Successfully created package!', $package->with('package_options')->find($package->id));
        }
        return response()->custom(400, 'Your package wasn\'t created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Package $package
     * @return \Illuminate\Http\Response
     */
    public function update(EditPackageRequest $request, Package $package, $id)
    {
        try {
            $package = Package::findOrFail($id);
            $package->fill($request->all());
            if ($package->save()) {
                if($request->package_options) {
                    $package_options = explode(",", $request->package_options);
                    $package->package_options()->detach();
                    $package->package_options()->attach($package_options);
                }
                if($request->hasFile('images')) {
                    foreach ($request->allFiles('images') as $file) {
                        $url = ImageUploader::upload($file, Constant::URL_PACKAGES);
                        $old_image = Image::where('imageable_id', $package->id)->first();
                        ImageUploader::remove($old_image->path);
                        $old_image->delete();
                        $image = new Image();
                        $image->imageable_id = $package->id;
                        $image->imageable_type = Constant::PACKAGE_IDENTIFIER;
                        $image->path = $url;
                        $image->save();
                    }
                }
                return response()->custom(200, 'Successfully updated package!', $package->with('package_options', 'images')->find($package->id));
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your package wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeletePackageRequest $request, $id)
    {
        try {
            $package = Package::find($id);
            if ($package && $package->delete()) {
                return response()->custom(200, 'Successfully deleted package!', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your package wasn\'t deleted!', null);
    }

    public function getPackagesFromLocation($id)
    {

        $package = Package::with('images')->where('location_id',$id)->latest()->paginate(11);
        if (count($package)) {
            return response()->custom(200, 'Successfully get packages!', $package);
        }
        return response()->custom(200, 'There are no packages!', null);
    }

    public function getPackageFromPackages($id)
    {
        $package = Package::with('images', 'package_options')->where('id',$id)->first();
        if ($package) {
            return response()->custom(200, 'Successfully get package!', $package);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    public function searchPackagesByName(Request $request)
    {
        $packages = Package::Search($request->search)->where('location_id', $request->location_id)->paginate(11);
        return response()->custom(200, 'Successfully retrieved packages', $packages);
    }
}
