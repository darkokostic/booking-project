<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Http\Requests\PackageOption\AddPackageOptionRequest;
use App\Http\Requests\PackageOption\DeletePackageOptionRequest;
use App\Http\Requests\PackageOption\EditPackageOptionRequest;
use App\PackageOption;
use App\Pricing;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPackageOptionsFromPackage(Request $request, $id)
    {
        if ($request->package_id) {
            $package_options = PackageOption::where('package_id', $request->package_id)->where('location_id', $id)->paginate(10);
        } else {
            $package_options = PackageOption::where('location_id', $id)->paginate(10);
        }
        if (count($package_options)) {
            return response()->custom(200, 'Successfully retrieved package options', $package_options);
        }
        return response()->custom(200, 'There are no package options', null);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPackageOptionRequest $request)
    {
        $pricing = new Pricing();
        $pricing->fill($request->all());
        if (!$pricing->tier2) {
            $pricing->tier2 = "";
        }
        if (!$pricing->tier3) {
            $pricing->tier3 = "";
        }

        if ($pricing->save()) {
            $option = new PackageOption();
            $option->pricing_id = $pricing->id;

            $option->base_price = $request->base_price;
            $option->party_duration = $request->party_duration;
            $option->name = $request->name;
            if ($option->save()) {
                if ($request->addons) {
                    $option->addons()->attach($request->addons);
                }
                if ($request->schedule) {
                    $new_schedule = new Schedule();
                    $new_schedule->days = $request->schedule['days'];
                    $new_schedule->from = new Carbon($request->schedule['from']);
                    $new_schedule->to = new Carbon($request->schedule['to']);
                    $new_schedule->start = $request->schedule['start'];
                    $new_schedule->end = $request->schedule['end'];
                    $new_schedule->interval = $request->schedule['interval'];
                    $new_schedule->consecutive = $request->schedule['consecutive'];
                    if ($new_schedule->save()) {
                        $new_schedule->options()->attach($option->id);
                    }
                }
                if ($request->schedules) {
                    $option->schedule()->attach($request->schedules);
                }
                return response()->custom(200, 'Successfully stored package option', $option->with('addons', 'schedule', 'pricing')->find($option->id));
            }
        }
        return response()->custom(400, 'There was an error', null);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageOption $packageOption
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package_option = PackageOption::with('pricing', 'schedule', 'policy', 'addons')->find($id);
        if ($package_option) {
            return response()->custom(200, 'Successfully get package option!', $package_option);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageOption $packageOption
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageOption $packageOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\PackageOption $packageOption
     * @return \Illuminate\Http\Response
     */
    public function update(EditPackageOptionRequest $request, $id)
    {
        $package_option = PackageOption::find($id);
        $pricing = Pricing::find($package_option->pricing_id);
        $pricing->fill($request->all());
        if (!$pricing->tier2) {
            $pricing->tier2 = "";
        }
        if (!$pricing->tier3) {
            $pricing->tier3 = "";
        }

        if ($pricing->save()) {
            $package_option->pricing_id = $pricing->id;

            $package_option->name = $request->name;
            if ($request->addons) {
                $package_option->addons()->detach();
                $package_option->addons()->attach($request->addons);
            }
            if ($package_option->save()) {
                if ($request->schedule) {
                    $old_schedule = Schedule::find($request->schedule['id']);
                    $old_schedule->days = $request->schedule['days'];
                    $old_schedule->from = new Carbon($request->schedule['from']);
                    $old_schedule->to = new Carbon($request->schedule['to']);
                    $old_schedule->start = $request->schedule['start'];
                    $old_schedule->end = $request->schedule['end'];
                    $old_schedule->interval = $request->schedule['interval'];
                    $old_schedule->consecutive = $request->schedule['consecutive'];
                    if ($old_schedule->save()) {

                    }
                }
                if ($request->schedules) {
                    $package_option->schedule()->detach();
                    $package_option->schedule()->attach($request->schedules);
                }
                return response()->custom(200, 'Successfully updated package option', $package_option);
            }
        }
        return response()->custom(400, 'There was an error', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageOption $packageOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeletePackageOptionRequest $request, $id)
    {
        try {
            $package_option = PackageOption::find($id);
            if ($package_option && $package_option->delete()) {
                return response()->custom(200, 'Successfully deleted package option', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Package option wasn\'t deleted!', null);
    }
}
