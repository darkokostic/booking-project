<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Helpers\Constant;
use App\Helpers\ImageUploader;
use App\Http\Requests\Addon\AddAddonRequest;
use App\Http\Requests\Addon\DeleteAddonRequest;
use App\Http\Requests\Addon\EditAddonRequest;
use App\Image;
use App\PackageOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddAddonRequest $request)
    {
        $package_options = explode(',', $request->package_options);
        $addon = new Addon();
        $addon->name = $request->name;
        $addon->price = $request->price;
        $addon->quantity = $request->quantity;
        $addon->description = $request->description;
        $addon->is_taxable = $request->is_taxable;
        $addon->addon_category_id = $request->addon_category_id;
        $addon->location_id = $request->location_id;
        $addon->unit_of_measure = $request->unit_of_measure;

        if ($addon->save()) {
            $addon->options()->attach($package_options);
            if ($request->hasFile('images')) {
                foreach ($request->allFiles('images') as $file) {
                    $url = ImageUploader::upload($file, Constant::URL_ADDONS);
                    $image = new Image();
                    $image->imageable_id = $addon->id;
                    $image->imageable_type = Constant::ADDON_IDENTIFIER;
                    $image->path = $url;
                    $image->save();
                }
            }
            $addon['image'] = $addon->image;
            return response()->custom(200, 'Successfully create addon!', $addon);
        }
        return response()->custom(400, 'Your addon was not created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $addon = Addon::with(['image', 'options'])->find($id);
        if ($addon) {
            return response()->custom(200, 'Successfully get addon!', $addon);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function edit(Addon $addon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function update(EditAddonRequest $request, $id)
    {

        try {
            $package_options = explode(',', $request->package_options);
            $addon = Addon::findOrFail($id);
            $addon->name = $request->name;
            $addon->price = $request->price;
            $addon->quantity = $request->quantity;
            $addon->description = $request->description;
            $addon->is_taxable = $request->is_taxable;
            $addon->addon_category_id = $request->addon_category_id;
            $addon->location_id = $request->location_id;
            $addon->unit_of_measure = $request->unit_of_measure;
            if ($addon->save()) {
                $addon->options()->detach();
                $addon->options()->attach($package_options);
                if ($request->hasFile('images')) {
                    foreach ($request->allFiles('images') as $file) {
                        $url = ImageUploader::upload($file, Constant::URL_ADDONS);
                        $old_image = Image::where('imageable_id', $addon->id)->first();
                        ImageUploader::remove($old_image->path);
                        $old_image->delete();
                        $image = new Image();
                        $image->imageable_id = $addon->id;
                        $image->imageable_type = Constant::ADDON_IDENTIFIER;
                        $image->path = $url;
                        $image->save();
                    }
                }
                return response()->custom(200, 'Successfully updated addon!', $addon);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your addon was not updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Addon $addon
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteAddonRequest $request, $id)
    {
        try {
            $location = Addon::find($id);
            if ($location && $location->delete()) {
                return response()->custom(200, 'Successfully deleted addon!', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your addon was not deleted!', null);
    }

    public function getAddonsInLocation($id)
    {
        $addons = Addon::with('image')->where('location_id', $id)->latest()->paginate(11);

        if (count($addons)) {
            return response()->custom(200, 'Successfully get addons!', $addons);
        }
        return response()->custom(200, 'There are no addons!', null);
    }

    public function searchAddonsByName(Request $request)
    {
        $addons = Addon::Search($request->search)->where('location_id', $request->location_id)->paginate(11);
        return response()->custom(200, 'Successfully retrieved addons!', $addons);
    }

    public function getWithAllOptions(Request $request)
    {
        $package_options = PackageOption::where('location_id', $request->location_id)->get();
        $addon = Addon::with(['image', 'options'])->find($request->id);

        foreach ($package_options as $option) {
            $contained = false;
            foreach ($addon->options as $addonOption) {
                if ($addonOption->id == $option->id) {
                    $contained = true;
                }
            }
            if ($contained) {
                $option['isAddon'] = true;
            } else {
                $option['isAddon'] = false;
            }
        }

        $addon['all_options'] = $package_options;
        if ($addon) {
            return response()->custom(200, 'Successfully get addon!', $addon);
        }
        return response()->custom(400, 'There was an error!', null);
    }
}
