<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $general_settings = GeneralSettings::get();
        if ($general_settings) {
            return response()->custom(200, 'Successfully retrieved general settings!', $general_settings);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateGeneralSettings(Request $request)
    {
        try {
            $general_settings = GeneralSettings::find(1);
            $general_settings->fill($request->all());
            if ($general_settings->save()) {
                return response()->custom(200, 'Successfully updated general settings!', $general_settings);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Settings wasn\'t updated!', null);
    }
}
