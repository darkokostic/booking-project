<?php

namespace App\Http\Controllers;

use App\Http\Requests\Policy\AddPolicyRequest;
use App\Http\Requests\Policy\PolicyRequest;
use App\PackageOption;
use App\Policy;
use App\PolicyQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPolicyRequest $request)
    {
        //dd($request->all());
        $policy = new Policy();
        $policy->name = $request->name;
        $policy->description = $request->description;
        $policy->is_global = $request->is_global;
        $policy->location_id = $request->location_id;
        if ($policy->save()) {
            if ($request->questions) {
                foreach ($request->questions as $question) {
                    $new_question = new PolicyQuestion();
                    $new_question->description = $question['description'];
                    $new_question->policy_id = $policy->id;
                    $new_question->save();
                }
            }
            foreach ($request->package_options as $optionId) {
                DB::table('policy_package_options')->insert(
                    ['policy_id' => $policy->id, 'package_option_id' => $optionId]
                );
            }
            return response()->custom(200, 'Successfully created policy!', $policy->with('questions', 'options')->find($policy->id));
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Policy $policy
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $policy = Policy::with('options', 'questions')->find($id);
        if ($policy) {
            return response()->custom(200,'Successfully get policy!', $policy);
        }
        return response()->custom(400,'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Policy $policy
     * @return \Illuminate\Http\Response
     */
    public function edit(Policy $policy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Policy $policy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Policy $policy)
    {
        try {
            $policy->name = $request->name;
            $policy->description = $request->description;
            $policy->is_global = $request->is_global;
            $policy->location_id = $request->location_id;
            if ($policy->save()) {
                if ($request->questions) {
                    foreach ($request->questions as $question) {
                        $new_question = new PolicyQuestion();
                        $new_question->description = $question['description'];
                        $new_question->policy_id = $policy->id;
                        $new_question->save();
                    }
                }
                foreach ($request->package_options as $optionId) {
                    DB::table('policy_package_options')->insert(
                        ['policy_id' => $policy->id, 'package_option_id' => $optionId]
                    );
                }
                return response()->custom(200, 'Successfully updated policy!', $policy->with('questions', 'options')->find($policy->id));
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your policy wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Policy $policy
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $policy = Policy::find($id);
            if ($policy && $policy->delete()) {
                return response()->custom(200, 'Successfully deleted policy!', null);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your policy wasn\'t deleted!', null);
    }

    public function getPoliciesFromLocation($id)
    {
        $policies = Policy::where('location_id',$id)->latest()->paginate(11);

        if (count($policies)) {
            return response()->custom(200,'Successfully get policies!', $policies);
        }
        return response()->custom(200,'There are no policies!', null);
    }

    public function searchPolicyByName(Request $request)
    {
        $policies = Policy::Search($request->search)->where('location_id', $request->location_id)->paginate(11);
        return response()->custom(200, 'Successfully retrieved policies!', $policies);
    }
}
