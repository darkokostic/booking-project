<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageUploader;
use App\Image;
use App\User;
use App\UserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserSettings $userSettings
     * @return \Illuminate\Http\Response
     */
    public function show(UserSettings $userSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserSettings $userSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(UserSettings $userSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\UserSettings $userSettings
     * @return \Illuminate\Http\Response
     */
    public function updateUserSettings(Request $request)
    {
        try {
            $user = Auth::user();
            $userSettings = UserSettings::where('user_id', $user->id)->first();
            $userSettings->fill($request->all());
            $userSettings->user_id = $user->id;

            if ($request->hasFile('user_avatar')) {
                $user_avatar = Image::where('imageable_id', $user->id)->first();
                if ($user_avatar->path != 'img/avatar.png') {
                    ImageUploader::remove($user_avatar->path);
                }
                $url = ImageUploader::upload($request->file('user_avatar'), Constant::URL_USERS);
                $user_avatar->imageable_type = Constant::USER_IDENTIFIER;
                $user_avatar->path = $url;
                $user_avatar->save();
            }

            if ($request->hasFile('event_logo')) {
                if ($user->event_logo != 'img/avatar.png') {
                    ImageUploader::remove($user->event_logo);
                }
                $user->event_logo = ImageUploader::upload($request->file('event_logo'), Constant::URL_EVENTS);
                $user->save();
            }

            if ($userSettings->save()) {
                if ($request->password) {
                    $user->password = $request->password;
                    $user->save();
                }
                return response()->custom(200, 'Successfully updated!', $user->with('images', 'user_settings')->first());
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserSettings $userSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSettings $userSettings)
    {
        //
    }

    public function getUserSettings()
    {
        $user = Auth::user();
        $user_settings = UserSettings::with('user')->where('user_id', $user->id)->get();
        if ($user_settings) {
            return response()->custom(200, 'Successfully retrieved user settings!', $user_settings);
        }
        return response()->custom(400, 'There was an error!', null);

    }
}
