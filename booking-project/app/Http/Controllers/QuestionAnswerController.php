<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionAnswer\AddQuestionAnswerRequest;
use App\Http\Requests\QuestionAnswer\EditQuestionAnswerRequest;
use App\QuestionAnswer;
use Illuminate\Http\Request;

class QuestionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = QuestionAnswer::with('question')->get();
        return response()->custom(200, 'Hello', $answers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddQuestionAnswerRequest $request)
    {
        $answer = new QuestionAnswer();
        $answer->fill($request->all());
        if ($answer->save()) {
            return response()->custom(200,'Successfully created answer!', $answer);
        }
        return response()->custom(400,'Your answer wasn\'t created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answer = QuestionAnswer::find($id);
        if ($answer) {
            return response()->custom(200,'Successfully retrieved answer!', $answer);
        }
        return response()->custom(400,'There was an error', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditQuestionAnswerRequest $request, $id)
    {
        try {
            $answer = QuestionAnswer::findOrFail($id);
            $answer->fill($request->all());

            if ($answer->save()) {
                return response()->custom(200,'Successfully updated answer!', $answer);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your answer wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $answer = QuestionAnswer::findOrFail($id);

            if ($answer && $answer->delete()) {
                return response()->custom(200,'Successfully deleted answer!', null);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your answer wasn\'t deleted!', null);
    }

    public function getAnswersFromQuestion($id)
    {
        $answers = QuestionAnswer::where('question_id', $id)->get();
        if (count($answers)) {
            return response()->custom(200,'Successfully retrieved answers!', $answers);
        }
        return response()->custom(200,'There are no answers!', null);
    }
}
