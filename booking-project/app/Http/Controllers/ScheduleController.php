<?php

namespace App\Http\Controllers;

use App\Http\Requests\Schedule\AddScheduleRequest;
use App\Http\Requests\Schedule\DeleteScheduleRequest;
use App\Http\Requests\Schedule\EditScheduleRequest;
use App\PackageOption;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::latest()->paginate(11);
        if (count($schedules)) {
            return response()->custom(200, 'Successfully retrieved schedules!', $schedules);
        }
        return response()->custom(200, 'There was an error!', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddScheduleRequest $request)
    {
        $schedule = new Schedule();
        $schedule->days = $request->days;
        $schedule->from = new Carbon($request->from);
        $schedule->to = new Carbon($request->to);
        $schedule->start = $request->start;
        $schedule->end = $request->end;
        $schedule->interval = $request->interval;
        $schedule->consecutive = $request->consecutive;

        if ($schedule->save()) {
            if ($request->package_options) {
                    $schedule->options()->attach($request->package_options);
            }
            return response()->custom(200, 'Successfully created schedule!', $schedule);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::with('options')->find($id);
        if ($schedule) {
            return response()->custom(200, 'Successfully created schedule!', $schedule);
        }
        return response()->custom(400, 'There was an error!', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(EditScheduleRequest $request, Schedule $schedule)
    {
        try {
            $schedule->days = $request->days;
            $schedule->from = new Carbon($request->from);
            $schedule->to = new Carbon($request->to);
            $schedule->start = $request->start;
            $schedule->end = $request->end;
            $schedule->interval = $request->interval;
            $schedule->consecutive = $request->consecutive;

            if ($schedule->save()) {
                if ($request->package_options) {
                    $schedule->options()->detach();
                    $schedule->options()->attach($request->package_options);
                }
                return response()->custom(200, 'Successfully updated schedule!', $schedule);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your schedule wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteScheduleRequest $request, $id)
    {
        try {
            $schedule = Schedule::findOrFail($id);
            if ($schedule && $schedule->delete()) {
                return response()->custom(200, 'Successfully deleted schedule!', null);
            }
        } catch (\Exception $e) {
            return response()->custom(400, $e->getMessage(), null);
        }
        return response()->custom(400, 'Your location wasn\'t deleted!', null);
    }

    public function searchSchedulesByName(Request $request)
    {
        $schedules = Schedule::Search(new Carbon($request->from), new Carbon($request->to))->paginate(11);
        return response()->custom(200, 'Successfully retrieved events!', $schedules);
    }
}
