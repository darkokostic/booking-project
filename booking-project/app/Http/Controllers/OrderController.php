<?php

namespace App\Http\Controllers;

use App\Addon;
use App\AddonOrder;
use App\Http\Requests\Order\AddOrderRequest;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('addons')->get();

        return response()->custom(200, 'Successfully retrieved orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddOrderRequest $request)
    {
        $order = new Order();
        $order->fill($request->all());

        if($order->save()) {
            if($request->addons) {
                foreach ($request->addons as $addonId) {
                    $addon = Addon::find($addonId)->toArray();
                    $addon['id'] = null;
                    $addonOrder = new AddonOrder();
                    $addonOrder->fill($addon);
                    $addonOrder->addon_id = $addonId;
                    $addonOrder->order_id = $order->id;

                    $addonOrder->save();
                }
            }
            $order['addons'] = $order->addons;
            return response()->custom(200, 'Successfully created order', $order);
        }
        return response()->custom(400, 'There was an error while creating order', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
