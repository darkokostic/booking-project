<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function upload(Request $request) {
        if($request->hasFile('image')) {
            $image = $request->file('image');

            $url = ImageUploader::upload($image, Constant::URL_USERS);

            return $url;
        }
    }
}
