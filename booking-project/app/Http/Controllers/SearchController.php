<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Event;
use App\Package;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search_global(Request $request)
    {
        $searched = [
            'users' => User::search($request->keyword)->limit(5)->get(),
            'addons' => Addon::search($request->keyword)->limit(5)->get(),
            'events' => Event::search($request->keyword)->limit(5)->get(),
            'packages' => Package::search($request->keyword)->limit(5)->get(),
        ];
        return response()->custom(200, 'Searched items', $searched);
    }
}
