<?php

namespace App\Http\Controllers\Auth;

use App\GeneralSettings;
use App\Helpers\Constant;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UsernameRequest;
use App\Image;
use App\User;
use App\Http\Controllers\Controller;
use App\UserSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
        ]);
    }

    public function customRegister(RegisterRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        if ($user->save()) {
            $image = new Image();
            $image->imageable_id = $user->id;
            $image->imageable_type = Constant::USER_IDENTIFIER;
            $image->path = 'img/avatar.png';
            $image->save();

            $general = GeneralSettings::first();

            $userSettings = new UserSettings();
            $userSettings->user_id = $user->id;
            $userSettings->menu_position = $general->menu_position;
            $userSettings->template_src =  $general->template_src;

            if ($userSettings->save()) {
                $http = new \GuzzleHttp\Client;
                $response = $http->post(url('/') . '/oauth/token', [
                    'json' => [
                        'grant_type' => 'password',
                        'client_id' => 2,
                        'client_secret' => 'dnPtWPGnQLcIAglu0hXQ3RfnHNIclpZYD7RwY914',
                        'username' => $request->username,
                        'password' => $request->password,
                        'scope' => '',
                    ],
                ]);
                return response()->custom(200, 'Successfully created account!', json_decode((string)$response->getBody(), true));
            }
        }
        return response()->custom(400, "Your account wasn't created!", null);
    }

    public function checkUsername(UsernameRequest $request)
    {
        $username = User::where('username', $request->username)->first();
        $email = User::where('email', $request->email)->first();
        if ($username) {
            $username = true;
        } else {
            $username = false;
        }
        if ($email) {
            $email = true;
        } else {
            $email = false;
        }
        $response = array("username_exist" => $username, "email_exist" => $email);
        return response()->custom(200, 'Successfully retrieved!', $response);
    }
}
