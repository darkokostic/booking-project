<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function customLogin(LoginRequest $request)
    {
        try{
            $user = User::with('images', 'user_settings')->where('username', $request->username)->first();
            if($user->status == 1) {
                $http = new \GuzzleHttp\Client;
                $response = $http->post(url('/').'/oauth/token', [
                    'json' => [
                        'grant_type' => 'password',
                        'client_id' => 2,
                        'client_secret' => 'dnPtWPGnQLcIAglu0hXQ3RfnHNIclpZYD7RwY914',
                        'username' => $request->username,
                        'password' => $request->password,
                        'scope' => '',
                    ],
                ]);
                $resp = json_decode((string) $response->getBody(), true);
                $resp['user'] = $user;
                $resp['user']['permissions'] = $user->getAllPermissions();
                return response()->custom(200, 'Successfully logged in!', $resp);
            }
            return response()->custom(403, "User is inactive!", null);
        }catch (\Exception $e){
            return response()->custom(400, "You are not logged in!", null);
        }
    }
}
