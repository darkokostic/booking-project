<?php

namespace App\Http\Controllers;

use App\Http\Requests\Question\AddQuestionRequest;
use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddQuestionRequest $request)
    {
        $question = new Question();
        $question->fill($request->all());
        if ($question->save()) {
            return response()->custom(200,'Successfully created question!', $question);
        }
        return response()->custom(400,'Your question wasn\'t created!', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $question = Question::findOrFail($id);
            $question->fill($request->all());
            if ($question->save()) {
                return response()->custom(200,'Successfully updated question!', $question);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your question wasn\'t updated!', null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $question = Question::findOrFail($id);
            if ($question && $question->delete()) {
                return response()->custom(200, 'Successfully deleted question!', null);
            }
        }
        catch (\Exception $e) {
            return response()->custom(400,$e->getMessage(), null);
        }
        return response()->custom(400,'Your question wasn\'t deleted!', null);
    }

    public function getQuestionsFromEvent($id)
    {
        $questions = Question::where('event_id', $id)->get();
        if (count($questions)) {
            return response()->custom(200,'Successfully retrieved questions!', $questions);
        }
        return response()->custom(200,'There are no questions!', null);
    }

    public function getQuestionFromQuestions($id)
    {
        $question = Question::find($id);
        if ($question) {
            return response()->custom(200,'Successfully retrieved question!', $question);
        }
        return response()->custom(400,'There was an error!', null);
    }
}
