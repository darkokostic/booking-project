<?php

namespace App\Http\Requests\Policy;

use Illuminate\Foundation\Http\FormRequest;

class AddPolicyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'package_options'   => 'required'
        ];
    }

    /**
     * @param array $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function response( array $errors ) {
        return response()->custom(400, 'There was an error', $errors);
    }
}
