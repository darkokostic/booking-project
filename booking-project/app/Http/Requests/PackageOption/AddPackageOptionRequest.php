<?php

namespace App\Http\Requests\PackageOption;

use App\Helpers\Constant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddPackageOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        if($user->hasRole(Constant::ROLE_ADMIN) || $user->can('manage-options')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tier1' => 'required|json',
            'deposit_type' => 'required|in:PERCENT,FLAT',
            'balance_due' => 'required|string',
            'per_additional_guest' => 'required|numeric',
            'party_duration'    =>  'required|numeric',
            'base_price'        =>  'required',
            'name' => 'required'
        ];
    }

    /**
     * @param array $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function response( array $errors ) {
        return response()->custom(400, 'There was an error', $errors);
    }
}
