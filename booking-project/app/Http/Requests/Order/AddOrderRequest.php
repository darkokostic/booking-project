<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_option_id'         => 'required|integer',
            'event_date'                => 'required|date',
            'time_block_id'              => 'required|integer',
            'guests_count'              => 'required|numeric',
//            'addons'                    => 'required|array',
            'host_firstname'            => 'required|string',
            'host_lastname'             => 'required|string',
            'host_email'                => 'required|string',
            'host_phone'                => 'required|string',
            'location_id'               => 'required|integer',
            'total'                     => 'required|numeric',

            'birthday_firstname'        => 'required|string',
            'birthday_lastname'         => 'required|string',
            'birthday_date_of_birth'    => 'required|date',
        ];
    }

    /**
     * @param array $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function response( array $errors ) {
        return response()->custom(400, 'There was an error', $errors);
    }
}
