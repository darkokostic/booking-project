<?php

namespace App\Http\Requests\Package;

use App\Helpers\Constant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        if($user->hasRole(Constant::ROLE_ADMIN) || $user->can('manage-packages')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'location_id' => 'required',
            'images'    => 'required|image|mimes:jpeg,bmp,png',
            'package_options' => 'required'
        ];
    }

    /**
     * @param array $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function response( array $errors ) {
        return response()->custom(400, 'There was an error', $errors);
    }
}
