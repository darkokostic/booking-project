<?php

namespace App\Http\Requests\Addon;

use App\Helpers\Constant;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddAddonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        if($user->hasRole(Constant::ROLE_ADMIN) || $user->can('manage-add-ons')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'price' => 'required',
            'addon_category_id' => 'required',
            'location_id' => 'required',
            'package_options' => 'required',
            'images' => 'required|image|mimes:jpeg,bmp,png',
            'unit_of_measure' => 'required|string'
        ];
    }
    
    /**
     * @param array $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function response( array $errors ) {
        return response()->custom(400, 'There was an error', $errors);
    }
}
