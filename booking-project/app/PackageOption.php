<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageOption extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'schedule_id', 'pricing_id', 'policy_id', 'package_id', 'location_id', 'party_duration', 'base_price'
    ];

    public function pricing() {
    	return $this->belongsTo('App\Pricing');
    }

    public function schedule() {
    	return $this->belongsToMany('App\Schedule', 'schedule_package_options', 'package_option_id');
    }

    public function addons() {
    	return $this->belongsToMany('App\Addon', 'addon_package_options', 'package_option_id');
    }

    public function policy() {
    	return $this->belongsToMany('App\Policy', 'policy_package_options', 'package_option_id');
    }

    public function packages() {
        return $this->belongsTo('App\Package', 'package_package_options', 'package_option_id');
    }

    public function location() {
        return $this->belongsTo('App\Location', 'location_id');
    }
}
