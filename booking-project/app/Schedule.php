<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'days', 'from', 'to', 'start', 'end', 'interval', 'consecutive'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'from',
        'to'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'days' => 'array'
    ];

    public function times() {
    	return $this->hasMany('App\TimeBlock');
    }
    
    public function options() {
    	return $this->belongsToMany('App\PackageOption', 'schedule_package_options', 'schedule_id');
	}

    /**
     * Local search scope for addons by keyword
     * params searchable are [name, description]
     */
    public function scopeSearch($query, $from, $to)
    {
        if ($from!='' && $to!='') {
            $query->where(function ($query) use ($from,$to) {
                $query->whereBetween("from",[$from, $to])
                ->orWhereBetween("to", [$from,$to]);
            });
        }
        return $query;
    }
}
