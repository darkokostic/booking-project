const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.scripts([
	    // INTERCEPTOR
        'public/angular/shared/interceptors/http.interceptor.js',

        //CONTROLLERS
        'public/angular/shared/controllers/portlets.js',
        'public/angular/shared/search/search.js',
		'public/angular/views/register/register.controller.js',
		'public/angular/views/login/login.controller.js',
    	'public/angular/views/dashboard/dashboard.controller.js',
    	'public/angular/views/managment/packages/view_packages/packages.controller.js',
    	'public/angular/views/managment/packages/new_package/new_package.controller.js',
    	'public/angular/views/managment/packages/edit_package/edit_package.controller.js',
    	'public/angular/views/managment/add_ons/view_add_ons/add_ons.controller.js',
    	'public/angular/views/managment/add_ons/edit_add_ons/edit_add_ons.controller.js',
    	'public/angular/views/managment/add_ons/new_add_ons/new_add_ons.controller.js',
    	'public/angular/views/managment/policy/view_policy/policy.controller.js',
    	'public/angular/views/managment/policy/edit_policy/edit_policy.controller.js',
    	'public/angular/views/managment/policy/new_policy/new_policy.controller.js',
    	'public/angular/views/managment/users/view_users/users.controller.js',
    	'public/angular/views/managment/users/edit_user/edit_user.controller.js',
    	'public/angular/views/managment/users/new_user/new_user.controller.js',
    	'public/angular/views/managment/scheduled_blocks/view_scheduled_blocks/scheduled_blocks.controller.js',
    	'public/angular/views/managment/scheduled_blocks/edit_scheduled_blocks/edit_scheduled_blocks.controller.js',
    	'public/angular/views/managment/scheduled_blocks/new_scheduled_blocks/new_scheduled_blocks.controller.js',
        'public/angular/views/managment/packages/option/add_option/add_option.controller.js',
        'public/angular/views/managment/packages/option/edit_option/edit_option.controller.js',
        'public/angular/views/policy/policy.controller.js',
        'public/angular/views/terms/terms.controller.js',
    	'public/angular/shared/components/side_menu_component.controller.js',
        'public/angular/shared/builder/main.js',
        'public/angular/views/lock_screen/lock_screen.controller.js',
        'public/angular/views/404_page/404_page.controller.js',
        'public/angular/views/settings/profile/profile.controller.js',
        'public/angular/views/settings/general/general.controller.js',
        'public/angular/views/managment/events/new_events/new_events.controller.js',
        'public/angular/views/managment/events/view_events/events.controller.js',
        'public/angular/views/managment/events/edit_events/edit_events.controller.js',
        'public/angular/views/booking_event/booking_event.controller.js',
        'public/angular/views/managment/categories/categories.controller.js',
        'public/angular/views/managment/locations/locations.controller.js',
        'public/angular/views/managment/order/list_order/list_order.controller.js',
        'public/angular/views/managment/order/edit_order/edit_order.controller.js',
        'public/angular/views/managment/order/edit_package_order/edit_package_order.controller.js',


    	//SERVICES
    	'public/angular/views/register/register.service.js',
    	'public/angular/views/login/login.service.js',
    	'public/angular/views/managment/packages/edit_package/edit_package.service.js',
    	'public/angular/views/managment/packages/new_package/new_package.service.js',
    	'public/angular/views/managment/packages/view_packages/packages.service.js',
    	'public/angular/views/dashboard/dashboard.service.js',
		'public/angular/views/managment/add_ons/view_add_ons/add_ons.service.js',
		'public/angular/views/managment/add_ons/edit_add_ons/edit_add_ons.service.js',
		'public/angular/views/managment/add_ons/new_add_ons/new_add_ons.service.js',
		'public/angular/views/managment/policy/view_policy/policy.service.js',
		'public/angular/views/managment/policy/edit_policy/edit_policy.service.js',
		'public/angular/views/managment/policy/new_policy/new_policy.service.js',
		'public/angular/views/managment/users/view_users/users.service.js',
		'public/angular/views/managment/users/edit_user/edit_user.service.js',
		'public/angular/views/managment/users/new_user/new_user.service.js',
		'public/angular/views/managment/scheduled_blocks/view_scheduled_blocks/scheduled_blocks.service.js',
		'public/angular/views/managment/scheduled_blocks/edit_scheduled_blocks/edit_scheduled_blocks.service.js',
		'public/angular/views/managment/scheduled_blocks/new_scheduled_blocks/new_scheduled_blocks.service.js',
        'public/angular/views/policy/policy.service.js',
        'public/angular/views/terms/terms.service.js',
        'public/angular/views/managment/packages/option/add_option/add_option.service.js',
        'public/angular/views/managment/packages/option/edit_option/edit_option.service.js',
        'public/angular/views/lock_screen/lock_screen.service.js',
        'public/angular/views/404_page/404_page.service.js',
        'public/angular/views/settings/profile/profile.service.js',
        'public/angular/views/settings/general/general.service.js',
        'public/angular/views/managment/events/view_events/events.service.js',
        'public/angular/views/managment/events/new_events/new_events.service.js',
        'public/angular/views/managment/events/edit_events/edit_events.service.js',
        'public/angular/views/booking_event/booking_event.service.js',
        'public/angular/views/managment/categories/categories.service.js',
        'public/angular/views/managment/locations/locations.service.js',
        'public/angular/views/managment/order/list_order/list_order.service.js',
        'public/angular/views/managment/order/edit_order/edit_order.service.js',
        'public/angular/views/managment/order/edit_package_order/edit_package_order.service.js',
        'public/angular/shared/search/search.service.js',
        'public/angular/shared/components/side_menu_component.service.js',


        //DIRECTIVES
        'public/angular/shared/directives/cs-select.js',
        'public/angular/shared/directives/pg-dropdown.js',
        'public/angular/shared/directives/pg-form-group.js',
        'public/angular/shared/directives/pg-horizontal-menu.js',
        'public/angular/shared/directives/pg-navigate.js',
        'public/angular/shared/directives/pg-notification.js',
        'public/angular/shared/directives/pg-portlet.js',
        'public/angular/shared/directives/pg-quickview.js',
        'public/angular/shared/directives/pg-search.js',
        'public/angular/shared/directives/pg-sidebar.js',
        'public/angular/shared/directives/pg-tab.js',
        'public/angular/shared/directives/pg-tab-dropdownfx.js',
        'public/angular/shared/directives/skycons.js',
        

	], 'public/angular/controllers.app.js')
	.sass('resources/assets/sass/app.scss', 'public/css');