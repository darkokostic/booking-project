<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" ng-app="booking" ng-controller="AppCtrl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>
            window.Laravel = { csrfToken: '{{ csrf_token() }}' };
        </script>

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- CSS -->
        <link href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/pages-icons.css" rel="stylesheet" type="text/css">
        <link class="main-stylesheet" href="/angular/shared/components/side_menu_component.css" rel="stylesheet" type="text/css">
        <link id="lazyload_placeholder">
        <link class="main-stylesheet" ng-href="@{{app.layout.theme}}" rel="stylesheet" type="text/css" />
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="animate.min.css">
    </head>
    <body ng-class="{'fixed-header':true ,'rtl':(app.layout.theme=='/css/themes/corporate.rtl.css' || app.layout.theme=='/css/pages.rtl.css' || app.layout.theme=='/css/themes/retro.rtl.css' || app.layout.theme=='/css/themes/unlax.rtl.css' || app.layout.theme=='/css/themes/vibes.rtl.css' || app.layout.theme=='/css/themes/abstract.rtl.css')}" >
        <ui-view></ui-view>

    </body>
    <!-- jQuery, Angular, Bootstrap -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('assets/plugins/angular-oc-lazyload/ocLazyLoad.min.js') }}"></script>
    <!-- angular-ui-router -->
    <script type="text/javascript" src="{{ asset('angular/booking.app.js') }}"></script>
    <!-- Lazy load -->
    <script src="{{ asset('assets/js/config.lazyload.js') }}"></script>
    <!-- controllers -->
    <script type="text/javascript" src="{{ asset('angular/controllers.app.js') }}"></script>
    <!-- javascript -->
    <script src="js/pages.min.js" type="text/javascript"></script>
    <script src="js/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    
</html>
