<?php

use App\Helpers\Constant;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', [
                Constant::ORDER_FAILED,
                Constant::ORDER_OPENED,
                Constant::ORDER_COMPLETED
            ]);
            $table->integer('guests_count');
            $table->integer('total');
            $table->dateTimeTz('event_date');
            $table->integer('time_block_id')->unsigned();
            $table->timestamp('order_date');
            $table->json('directary_restrictions')->nullable();
            $table->mediumText('notes')->nullable();
            $table->integer('package_option_id')->unsigned();
            $table->integer('location_id')->unsigned();

            $table->string('host_firstname');
            $table->string('host_lastname');
            $table->string('host_email');
            $table->string('host_phone');
            $table->string('host_ext')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();

            $table->string('birthday_firstname');
            $table->string('birthday_lastname');
            $table->dateTimeTz('birthday_date_of_birth');

            $table->string('guest_of_honor_firstname')->nullable();
            $table->string('guest_of_honor_lastname')->nullable();
            $table->dateTimeTz('guest_of_honor_date_of_birth')->nullable();

            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('package_option_id')->references('id')->on('package_options');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('time_block_id')->references('id')->on('time_blocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
