<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'schedules', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->json( 'days' );
			$table->dateTimeTz( 'from' );
			$table->dateTimeTz( 'to' );
			$table->string( 'start', 8 );
			$table->string( 'end', 8 );
			$table->integer( 'interval' )->unsigned();
			$table->integer( 'consecutive' )->unsigned();
			$table->timestamps();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'schedules' );
	}
}
