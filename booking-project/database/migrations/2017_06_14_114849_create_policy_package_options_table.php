<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyPackageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'policy_package_options', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'policy_id' )->unsigned();
            $table->integer( 'package_option_id' )->unsigned();
            $table->timestamps();
        } );

        Schema::table( 'policy_package_options', function( Blueprint $table ) {
            $table->foreign( 'policy_id' )->references( 'id' )->on( 'policies' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
            $table->foreign( 'package_option_id' )->references( 'id' )->on( 'package_options' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'policy_package_options' );
    }
}
