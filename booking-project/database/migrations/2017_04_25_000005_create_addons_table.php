<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'addons', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name', 60 );
			$table->integer( 'price' );
			$table->integer( 'quantity' );
			$table->longText( 'description' );
			$table->boolean( 'is_taxable' )->default( FALSE );
            $table->integer( 'location_id' )->unsigned();
            $table->integer('addon_category_id')->unsigned();
            $table->string('unit_of_measure');
			$table->timestamps();
		} );
		
		Schema::table( 'addons', function( Blueprint $table ) {
            $table->index( 'name' );
            $table->foreign('location_id')->references( 'id' )->on('locations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign( 'addon_category_id' )->references( 'id' )->on( 'addon_categories' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'addons' );
	}
}
