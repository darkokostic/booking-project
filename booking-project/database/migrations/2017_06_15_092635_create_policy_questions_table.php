<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'policy_questions', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer('policy_id')->unsigned();
            $table->longText('description');
            $table->timestamps();
        } );

        Schema::table( 'policy_questions', function( Blueprint $table ) {
            $table->foreign('policy_id')->references( 'id' )->on('policies')->onUpdate('cascade')->onDelete('cascade');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_questions');
    }
}
