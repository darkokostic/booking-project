<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'pricings', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->json( 'tier1' );
			$table->json( 'tier2' );
			$table->json( 'tier3' );
			$table->string( 'deposit_money' )->nullable();
            $table->string( 'deposit_percent' )->nullable();
            $table->boolean( 'is_birthday_persone_free' );
            $table->boolean( 'two_guest_of_honor');
            $table->integer( 'second_guest_of_honor_charge')->nullable();
			$table->enum( 'deposit_type', [
				'PERCENT',
				'FLAT',
			] );
            $table->enum( 'pricing_model', [
                'Pricing Model 1',
                'Pricing Model 2',
                'Pricing Model 3'
            ] );
			$table->string( 'balance_due' );
			$table->boolean( 'is_free' )->default( FALSE );
			$table->boolean( 'is_additional_guests' )->default( FALSE );
			$table->integer( 'per_additional_guest' )->unsigned();
			$table->timestamps();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'pricings' );
	}
}
