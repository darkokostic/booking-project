<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'policies', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'name' );
			$table->longText( 'description' );
			$table->boolean( 'is_global' )->default( FALSE );
			$table->integer( 'location_id' )->unsigned()->nullable();
			$table->timestamps();
		} );
		
		Schema::table( 'policies', function( Blueprint $table ) {
			$table->foreign( 'location_id' )->references( 'id' )->on( 'locations' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'policies' );
	}
}
