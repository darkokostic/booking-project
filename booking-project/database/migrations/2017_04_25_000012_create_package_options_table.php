<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageOptionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'package_options', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string('name');
			$table->integer('base_price');
            $table->integer('party_duration');
			$table->integer( 'pricing_id' )->unsigned();
            $table->integer( 'package_id' )->unsigned()->nullable();
			$table->integer( 'policy_id' )->unsigned()->nullable();
            $table->integer( 'location_id' )->unsigned()->nullable();
			$table->timestamps();
		} );
		
		Schema::table( 'package_options', function( Blueprint $table ) {
			$table->foreign( 'pricing_id' )->references( 'id' )->on( 'pricings' )->onUpdate('cascade')->onDelete('cascade');
            $table->foreign( 'package_id' )->references( 'id' )->on( 'packages' )->onUpdate('cascade')->onDelete('cascade');
			$table->foreign( 'policy_id' )->references( 'id' )->on( 'policies' )->onUpdate('cascade')->onDelete('cascade');
            $table->foreign( 'location_id' )->references( 'id' )->on( 'locations' )->onUpdate('cascade')->onDelete('cascade');
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'package_options' );
	}
}
