<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'questions', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer('event_id')->unsigned();
            $table->enum('type', ['text', 'checkbox', 'dropdown']);
            $table->longText( 'description' );
            $table->timestamps();
        } );

        Schema::table( 'questions', function( Blueprint $table ) {
            $table->foreign('event_id')->references( 'id' )->on('events')->onUpdate('cascade')->onDelete('cascade');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
