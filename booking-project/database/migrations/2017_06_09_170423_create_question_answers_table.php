<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'question_answers', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer('question_id')->unsigned();
            $table->string( 'answer' );
            $table->timestamps();
        } );

        Schema::table( 'question_answers', function( Blueprint $table ) {
            $table->foreign('question_id')->references( 'id' )->on('questions')->onUpdate('cascade')->onDelete('cascade');
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_answers');
    }
}
