<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeBlocksTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		
		Schema::create( 'time_blocks', function( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'from', 8 );
			$table->string( 'to', 8 ); //09:00 PM
			$table->boolean( 'is_booked' )->default( FALSE );
			$table->integer( 'schedule_id' )->unsigned();
			$table->timestamps();
		} );
		
		Schema::table( 'time_blocks', function( Blueprint $table ) {
			$table->foreign( 'schedule_id' )->references( 'id' )->on( 'schedules' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'time_blocks' );
	}
}
