<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulePackageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'schedule_package_options', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'schedule_id' )->unsigned();
            $table->integer( 'package_option_id' )->unsigned();
            $table->timestamps();
        } );

        Schema::table( 'schedule_package_options', function( Blueprint $table ) {
            $table->foreign( 'schedule_id' )->references( 'id' )->on( 'schedules' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
            $table->foreign( 'package_option_id' )->references( 'id' )->on( 'package_options' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'schedule_package_options' );
    }
}
