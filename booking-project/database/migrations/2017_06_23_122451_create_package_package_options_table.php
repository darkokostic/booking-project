<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagePackageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'package_package_options', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'package_id' )->unsigned();
            $table->integer( 'package_option_id' )->unsigned();
            $table->timestamps();
        } );

        Schema::table( 'package_package_options', function( Blueprint $table ) {
            $table->foreign( 'package_id' )->references( 'id' )->on( 'packages' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
            $table->foreign( 'package_option_id' )->references( 'id' )->on( 'package_options' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'package_package_options' );
    }
}
