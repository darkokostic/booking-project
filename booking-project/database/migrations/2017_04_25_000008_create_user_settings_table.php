<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSettingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'user_settings', function( Blueprint $table ) {
			$table->increments( 'id' );
            $table->enum( 'menu_position', ['left', 'right'] )->default('left');
			$table->string( 'template_src' )->default('/css/pages.css');
            $table->string( 'ribbon_color' )->default('#00cfbd');
            $table->string( 'ribbon_color_name')->default('Green');
            $table->string( 'ribbon_name' ,11)->default('RECOMMENDED');
			$table->integer( 'user_id' )->unsigned();
			$table->timestamps();
		} );
		
		Schema::table( 'user_settings', function( Blueprint $table ) {
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onUpdate( 'cascade' )->onDelete( 'cascade' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'user_settings' );
	}
}
