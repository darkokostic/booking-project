<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonPackageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'addon_package_options', function( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'addon_id' )->unsigned();
            $table->integer( 'package_option_id' )->unsigned();
            $table->timestamps();
        } );

        Schema::table( 'addon_package_options', function( Blueprint $table ) {
            $table->foreign( 'addon_id' )->references( 'id' )->on( 'addons' );
            $table->foreign( 'package_option_id' )->references( 'id' )->on( 'package_options' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'addon_package_options' );
    }
}
