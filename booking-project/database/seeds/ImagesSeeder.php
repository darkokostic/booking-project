<?php

use Illuminate\Database\Seeder;
use App\Helpers\Constant;
use Carbon\Carbon;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = ['/rb15.jpg', '/rb14.jpg', '/rb13.jpg'];
        //Users
        for($i = 0; $i <= 30; $i++) {
            $image = $images[rand(0, 2)];
            DB::table( 'images' )->insert( [
                'imageable_id'  => rand( 1, 10 ),
                'imageable_type' => Constant::USER_IDENTIFIER,
                'path'   => 'uploads/'.Constant::URL_USERS.$image,
                'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
            ] );
        }
        //package
        for($i = 0; $i <= 30; $i++) {
            $image = $images[rand(0, 2)];
            DB::table( 'images' )->insert( [
                'imageable_id'  => rand( 1, 10 ),
                'imageable_type' => Constant::PACKAGE_IDENTIFIER,
                'path'   => 'uploads/'.Constant::URL_PACKAGES.$image,
                'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
            ] );
        }
    }
}
