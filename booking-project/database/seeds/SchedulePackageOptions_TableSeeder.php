<?php

use Illuminate\Database\Seeder;

class SchedulePackageOptions_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 500; $i++) {
            DB::table( 'schedule_package_options' )->insert( [
                'schedule_id'          => rand( 1, 10 ),
                'package_option_id'    => rand( 1, 10 ),
            ] );
        }
    }
}
