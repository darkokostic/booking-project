<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        $this->call( Roles_TableSeeder::class );
        $this->call( Users_TableSeeder::class );
        $this->call( Permissions_TableSeeder::class );
		$this->call( AddonCategories_TableSeeder::class );
        $this->call( Locations_TableSeeder::class );
        $this->call( Addons_TableSeeder::class );
		$this->call( Packages_TableSeeder::class );
		$this->call( Policy_TableSeeder::class );
		$this->call( UserSettings_TableSeeder::class );
		$this->call( Chedules_TableSeeder::class );
		$this->call( TimeBlocks_TableSeeder::class );
		$this->call( Pricings_TableSeeder::class );
		$this->call( OAuthClients_TableSeeder::class );
		$this->call( ImagesSeeder::class );
		$this->call( PackageOptions_TableSeeder::class);
        $this->call( Events_TableSeeder::class );
        $this->call( Questions_TableSeeder::class );
        $this->call( QuestionAnswers_TableSeeder::class );
        $this->call( AddonPackageOptions_TableSeeder::class );
        $this->call( SchedulePackageOptions_TableSeeder::class );
        $this->call( PolicyPackageOptions_TableSeeder::class );
        $this->call( PolicyQuestions_TableSeeder::class );
        $this->call( GeneralSettings_TableSeeder::class );
        $this->call( PackagePackageOptions_TableSeeder::class );
	}
}
