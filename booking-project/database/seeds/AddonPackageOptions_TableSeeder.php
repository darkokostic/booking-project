<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddonPackageOptions_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 500; $i++) {
            DB::table( 'addon_package_options' )->insert( [
                'addon_id'             => rand( 1, 10 ),
                'package_option_id'    => rand( 1, 10 ),
            ] );
        }
    }
}
