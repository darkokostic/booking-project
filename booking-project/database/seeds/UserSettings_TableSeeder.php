<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSettings_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		for($i = 0; $i <= 10; $i++) {
			DB::table( 'user_settings' )->insert( [
				'template_src' => '/css/themes/abstract.css',
				'user_id'      => rand( 1, 9 ),
				'created_at'   => Carbon::now()->subMinutes( rand( 0, 60 ) ),
                'ribbon_color' => '#00cfbd',
                'ribbon_color_name' => 'Green',
                'ribbon_name'  => 'RECOMMENDED',
                'menu_position' => 'left'
			] );
		}
	}
}
