<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class Locations_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        DB::table( 'locations' )->insert( [
            'name'       => "Location Darko",
            'location'   => "Darkova Ulica 12",
            'user_id'    => 1,
            'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ]);

		$faker = Faker\Factory::create();
		for($i = 0; $i <= 205; $i++) {
			DB::table( 'locations' )->insert( [
				'name'       => $faker->unique()->name(),
				'location'   => $faker->unique()->address(),
				'user_id'    => rand( 1, 10 ),
				'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
