<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Pricings_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		for($i = 0; $i <= 10; $i++) {
			$tier1 = json_encode( [
				'guests_included'      => rand( 5, 50 ),
				'per_additional_guest' => rand( 5, 25 ),
				'min_guests'           => rand( 0, 15 ),
				'max_guests'           => rand( 15, 150 ),
				'label_singular'       => 'child',
				'label_plural'         => 'children',
				'ask_guest_count'      => (bool)rand( 0, 1 ),
			] );
			
			$tier2 = json_encode( [
				'guests_included'      => rand( 5, 50 ),
				'per_additional_guest' => rand( 5, 25 ),
				'min_guests'           => rand( 0, 15 ),
				'max_guests'           => rand( 15, 150 ),
				'label_singular'       => 'child',
				'label_plural'         => 'children',
				'ask_guest_count'      => (bool)rand( 0, 1 ),
			] );
			
			$tier3 = json_encode( [
				'guests_included'      => rand( 5, 50 ),
				'per_additional_guest' => rand( 5, 25 ),
				'min_guests'           => rand( 0, 15 ),
				'max_guests'           => rand( 15, 150 ),
				'label_singular'       => 'child',
				'label_plural'         => 'children',
				'ask_guest_count'      => (bool)rand( 0, 1 ),
			] );
			$pricing_models = ['Pricing Model 1', 'Pricing Model 2', 'Pricing Model 3'];
			DB::table( 'pricings' )->insert( [
				'tier1'                => $tier1,
				'tier2'                => $tier2,
				'tier3'                => $tier3,
				'deposit_money'              => rand( 50, 250 ),
                'deposit_percent'              => rand( 1, 5 ) * 10,
                'is_birthday_persone_free' => true,
                'two_guest_of_honor'    => false,
				'deposit_type'         => "FLAT",
				'balance_due'          => rand( 2, 20 ) . " days before",
				'is_free'              => (bool)rand( 0, 1 ),
				'is_additional_guests' => (bool)rand( 0, 1 ),
				'per_additional_guest' => rand( 5, 25 ),
                'pricing_model'        => $pricing_models[rand(0,2)],
                'created_at'           => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
