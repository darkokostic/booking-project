<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Chedules_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$days = [
			[
				[
					'Mon',
					'Tue',
					'Wed',
					'Thu',
					'Fri',
				],
			],
			[
				[
					'Sat',
					'Sun',
				],
			],
		];
		
		for($i = 0; $i <= 100; $i++) {
			$date = Carbon::create( 2017, 5, 28, rand( 0, 10 ), (rand( 0, 1 ) ? 0 : 30), 0 );
			
			DB::table( 'schedules' )->insert( [
				
				'days'        => json_encode( $days[rand( 0, 1 )] ),
				'from'        => Carbon::now( new DateTimeZone( 'Europe/London' ) ),
				'to'          => Carbon::now( new DateTimeZone( 'Europe/London' ) )->addHours( rand( 1, 4 ) ),
				'start'       => $date->format( 'H:i' ) . ' PM',
				'end'         => $date->addHours( rand( 1, 2 ) )->addMinutes( (rand( 0, 1 ) ? 0 : 30) )->format( 'H:i' ) . ' PM',
				'interval'    => rand( 1, 6 ),
				'consecutive' => rand( 1, 3 ),
				'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}

