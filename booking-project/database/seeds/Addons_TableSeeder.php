<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class Addons_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		for($i = 0; $i <= 800; $i++) {
			DB::table( 'addons' )->insert( [
				'name'              => $faker->unique()->text( $maxNbChars = 55 ),
				'price'             => rand( 5, 50 ),
				'quantity'          => rand( 0, 25 ),
				'description'       => $faker->unique()->text( $maxNbChars = 100 ),
				'is_taxable'        => (bool)rand( 0, 1 ),
				'addon_category_id' => rand( 1, 9 ),
                'location_id' => 1,
				'created_at'        => Carbon::now()->subMinutes( rand( 0, 60 ) ),
                'unit_of_measure'   => $faker->unique()->text( $maxNbChars = 15 ),
			] );
		}
	}
}
