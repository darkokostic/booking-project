<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class Permissions_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        /**
         * Manage Events
         */
		$permission               = new Permission;
		$permission->name         = 'manage-packages';
//		$permission->display_name = 'Manage packages';
//		$permission->description  = 'This is Permission for managing packages.';
		$permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'manage-options';
//        $permission->display_name = 'Manage options';
//        $permission->description  = 'This is Permission for managing options.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'manage-add-ons';
//        $permission->display_name = 'Manage add ons';
//        $permission->description  = 'This is Permission for managing add ons.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'manage-time-slots';
//        $permission->display_name = 'Manage time slots';
//        $permission->description  = 'This is Permission for managing time slots.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'make-packages-available';
//        $permission->display_name = 'Make Packages available';
//        $permission->description  = 'This is Permission for ability to make packages available for online booking.';
        $permission->save();

        /**
         *  Manage orders
         */
        $permission               = new Permission;
        $permission->name         = 'edit-orders';
//        $permission->display_name = 'Manage orders';
//        $permission->description  = 'This is Permission for edit orders.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'reschedule-orders';
//        $permission->display_name = 'Reschedule orders';
//        $permission->description  = 'This is Permission for reschedule orders.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'cancel-orders';
//        $permission->display_name = 'Cancel orders';
//        $permission->description  = 'This is Permission for cancel orders.';
        $permission->save();

        /**
         * Booking events
         */
        $permission               = new Permission;
        $permission->name         = 'book-event';
//        $permission->display_name = 'Book event';
//        $permission->description  = 'This is Permission for Booking a  event.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'place-event-date-time';
//        $permission->display_name = 'Place event date and time';
//        $permission->description  = 'This is Permission for placing event date and time on hold.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'time-slot-private';
//        $permission->display_name = 'Make time slot private';
//        $permission->description  = 'This is Permission for making time slot private.';
        $permission->save();
//
        $permission               = new Permission;
        $permission->name         = 'add-one-time-event';
//        $permission->display_name = 'Add one time event';
//        $permission->description  = 'This is Permission for adding one time event.';
        $permission->save();
//

	}
}
