<?php

use Illuminate\Database\Seeder;

class GeneralSettings_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table( 'general_settings' )->insert( [
            'created_at'   => \Carbon\Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ] );
    }
}
