<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Users_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		
		$user           = new User;
		$user->username = 'admin';
		$user->firstname= 'Admin';
		$user->lastname = 'Demo';
		$user->email    = 'admin@booking.dev';
		$user->password = 'admin';
        $user->phone    = '1234567890';
        $user->status   = true;
		if($user->save()) {
            $user->assignRole(\App\Helpers\Constant::ROLE_ADMIN);
        }
		
		$faker = Faker\Factory::create();
		for($i = 0; $i <= 100; $i++) {
			DB::table( 'users' )->insert( [
				'username'   => $faker->unique()->name(),
                'firstname'  => $faker->firstName(),
                'lastname'   => $faker->lastName(),
				'email'      => $faker->unique()->email(),
				'password'   => 'password',
				'phone'      => '1234567890',
                'status'     => true,
				'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
