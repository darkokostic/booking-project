<?php

use Illuminate\Database\Seeder;

class OAuthClients_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table( 'oauth_clients' )->insert( [
            'user_id'       => null,
            'name'   => 'Booking Personal Access Client',
            'secret'    => 'HwOim219G64btoj3bMz0PDF0DasEb8SZ6I202KRk',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
        ] );
        DB::table( 'oauth_clients' )->insert( [
            'user_id'       => null,
            'name'   => 'Booking Password Grant Client',
            'secret'    => 'dnPtWPGnQLcIAglu0hXQ3RfnHNIclpZYD7RwY914',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now()
        ] );
    }
}
