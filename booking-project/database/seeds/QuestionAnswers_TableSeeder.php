<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionAnswers_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 10; $i++) {

            DB::table( 'question_answers' )->insert( [
                'answer'          => $faker->text(25),
                'question_id'          => rand( 1, 10 ),
            ] );
        }
    }
}
