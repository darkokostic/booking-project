<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class Policy_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();

		for($i = 0; $i <= 500; $i++) {
			DB::table( 'policies' )->insert( [
				'name'        => $faker->unique()->name(),
				'description' => $faker->unique()->text( $maxNbChars = 500 ),
				'is_global'   => (bool)rand( 0, 1 ),
				'location_id'=> 1,
				'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
