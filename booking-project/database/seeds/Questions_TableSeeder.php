<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Questions_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 100; $i++) {
            DB::table( 'questions' )->insert( [
                'description'=> $faker->unique()->text( $maxNbChars = 500 ),
                'event_id' => rand( 1, 10 ),
                'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
                'type' => 'text'
            ] );
        }
    }
}
