<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddonCategories_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker\Factory::create();
		for($i = 0; $i <= 10; $i++) {
			DB::table( 'addon_categories' )->insert( [
				'name'       => $faker->unique()->word(),
				'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
