<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageOptions_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        DB::table( 'package_options' )->insert([
            'name'        => "Opcija 1",
            'pricing_id'  => rand( 1, 10 ),
            'package_id'  => 1,
            'policy_id'   => rand( 1, 10 ),
            'location_id' => 1,
            'base_price'  => rand(50, 1000),
            'party_duration' => rand( 50, 300 ),
            'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ]);
        DB::table( 'package_options' )->insert([
            'name'        => "Opcija 2",
            'pricing_id'  => rand( 1, 10 ),
            'package_id'  => 1,
            'policy_id'   => rand( 1, 10 ),
            'location_id' => 1,
            'base_price'  => rand(50, 1000),
            'party_duration' => rand( 50, 300 ),
            'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ]);
        DB::table( 'package_options' )->insert([
            'name'        => "Opcija 3",
            'pricing_id'  => rand( 1, 10 ),
            'package_id'  => 1,
            'policy_id'   => rand( 1, 10 ),
            'location_id' => 1,
            'base_price'  => rand(50, 1000),
            'party_duration' => rand( 50, 300 ),
            'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ]);
        $faker = Faker\Factory::create();
		for($i = 0; $i <= 1200; $i++) {
			DB::table( 'package_options' )->insert( [
			    'name'        => $faker->unique()->text( $maxNbChars = 55 ),
				'pricing_id'  => rand( 1, 10 ),
                'package_id'  => rand(1, 9),
				'policy_id'   => rand( 1, 10 ),
                'location_id' => rand( 1, 200 ),
                'base_price'  => rand(50, 1000),
                'party_duration' => rand( 50, 300 ),
				'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
		
	}
}
