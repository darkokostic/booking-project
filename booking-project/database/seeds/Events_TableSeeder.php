<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Events_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $steps = json_encode([1, 2, 3, 4, 5]);
        for ($i = 0; $i <= 800; $i++) {
            DB::table('events')->insert([
                'name' => $faker->unique()->name(),
                'description' => $faker->unique()->text($maxNbChars = 500),
                'location_id' => 1,
                'steps' => $steps,
                'package_option_id' => rand(1, 10),
                'created_at' => Carbon::now()->subMinutes(rand(0, 60)),
            ]);
        }
    }
}
