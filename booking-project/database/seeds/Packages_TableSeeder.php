<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Packages_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        $faker = Faker\Factory::create();
        DB::table( 'packages' )->insert([
            'name'        => "Darkov Paket",
            'description' => $faker->unique()->text( $maxNbChars = 500 ),
            'location_id' => 1,
            'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
        ]);

		for($i = 0; $i <= 9; $i++) {
			DB::table( 'packages' )->insert( [
				'name'        => $faker->unique()->name(),
				'description' => $faker->unique()->text( $maxNbChars = 500 ),
				'location_id' => 1,
				'created_at'  => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}