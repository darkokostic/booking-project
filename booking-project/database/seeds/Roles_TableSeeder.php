<?php

use App\Helpers\Constant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;


class Roles_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$role               = new Role;
		$role->name         = Constant::ROLE_ADMIN;
//		$role->display_name = "Administrator";
//		$role->description  = "This role have administrator of system.";
		$role->save();
//
		$role               = new Role;
		$role->name         = Constant::ROLE_MANAGER;
//		$role->display_name = "Manager";
//		$role->description  = "This role have manager of system.";
		$role->save();
//
		$role               = new Role;
		$role->name         = Constant::ROLE_CLIENT;
//		$role->display_name = "Client";
//		$role->description  = "This role have client of system.";
		$role->save();
	}
}
