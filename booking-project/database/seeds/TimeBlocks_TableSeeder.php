<?php

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Database\Seeder;


class TimeBlocks_TableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		for($i = 0; $i <= 10; $i++) {
			$date = Carbon::create( 2015, 5, 28, rand( 0, 10 ), (rand( 0, 1 ) ? 0 : 30), 0 );
			
			DB::table( 'time_blocks' )->insert( [
				'from'       => $date->format( 'H:i' ) . ' PM',
				'to'         => $date->addHours( rand( 1, 2 ) )->addMinutes( (rand( 0, 1 ) ? 0 : 30) )->format( 'H:i' ) . ' PM',
				'is_booked'  => boolval( rand( 0, 1 ) ),
				'schedule_id' => rand( 1, 10 ),
				'created_at' => Carbon::now()->subMinutes( rand( 0, 60 ) ),
			] );
		}
	}
}
