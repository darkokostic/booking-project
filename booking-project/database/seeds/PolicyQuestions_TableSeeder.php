<?php

use Illuminate\Database\Seeder;

class PolicyQuestions_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i <= 200; $i++) {
            DB::table( 'policy_questions' )->insert( [
                'policy_id' => rand( 1, 200 ),
                'description'  => $faker->text(25),
            ] );
        }
    }
}
