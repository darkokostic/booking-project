<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'Auth\RegisterController@customRegister');
Route::post('login', 'Auth\LoginController@customLogin');
Route::post('check-username', 'Auth\RegisterController@checkUsername');

Route::group(['middleware' => 'auth:api'], function () {

    Route::resource('locations', 'LocationController',
        ['only' => ['index', 'store', 'update', 'destroy', 'show']]);

    Route::resource('events', 'EventController',
        ['only' => ['index', 'store', 'update', 'destroy', 'show']]);
    Route::get('location/events/{id}', 'EventController@getEventsFromLocation');
    Route::post('search/events', 'EventController@searchEventsByName');

    Route::get('location/packages/{id}', 'PackageController@getPackagesFromLocation');
    Route::get('packages/{id}', 'PackageController@getPackageFromPackages');
    Route::post('packages', 'PackageController@store');
    Route::post('packages/{id}', 'PackageController@update');
    Route::resource('package/options', 'PackageOptionController',
        ['only' => ['store', 'show', 'update', 'destroy']]);
    Route::get('location/package/options/{id}', 'PackageOptionController@getPackageOptionsFromPackage');

    Route::post('search/packages', 'PackageController@searchPackagesByName');

    Route::resource('policies', 'PolicyController',
        ['only' => ['store', 'show', 'update', 'destroy']]);
    Route::get('location/policies/{id}', 'PolicyController@getPoliciesFromLocation');
    Route::post('search/policies', 'PolicyController@searchPolicyByName');


    Route::delete('packages/{id}', 'PackageController@destroy');

    Route::resource('addon-categories', 'AddonCategoryController',
        ['only' => ['index', 'store', 'update', 'destroy', 'show']]);

    Route::resource('schedules', 'ScheduleController',
        ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
    Route::post('search/schedules', 'ScheduleController@searchSchedulesByName');

    Route::get('location/addons/{id}', 'AddonController@getAddonsInLocation');
    Route::post('location/options/addon', 'AddonController@getWithAllOptions');


    Route::resource('addons', 'AddonController',
        ['only' => ['index','show', 'store', 'destroy']]);
    Route::post('addons/{id}', 'AddonController@update');
    Route::post('search/addons', 'AddonController@searchAddonsByName');

    Route::resource('users', 'UserController',
        ['only' => ['index', 'show', 'destroy', 'store']]);
    Route::post('search/users', 'UserController@searchUsersByName');
    Route::post('users/{id}', 'UserController@update');

    Route::resource('questions', 'QuestionController',
        ['only' => ['destroy', 'update', 'store']]);

    Route::get('events/questions/{id}', 'QuestionController@getQuestionsFromEvent');
    Route::get('questions/{id}', 'QuestionController@getQuestionFromQuestions');

    Route::get('questions/answers/{id}', 'QuestionAnswerController@getAnswersFromQuestion');
    Route::resource('answers', 'QuestionAnswerController',
        ['only' => ['destroy', 'update', 'store', 'show']]);

    Route::get('user/settings', 'UserSettingsController@getUserSettings');
    Route::post('user/settings', 'UserSettingsController@updateUserSettings');

    Route::post('global/search', 'SearchController@search_global');
    Route::get('general/settings', 'GeneralSettingsController@index');
    Route::put('general/settings', 'GeneralSettingsController@updateGeneralSettings');
});

Route::post('image/upload', 'ImageController@upload');

Route::resource('orders', 'OrderController',
    ['only' => ['store', 'index']]);


//Route::resource('question-answers', 'QuestionAnswerController',
//    ['only' => ['index']]);